create or replace view avaliados as 
          select pessoa.nome pessoa, pessoa.id as id_avaliado, papel.nome papel, p.nome processo, p.id id_processo 
            from sis_papel papel
      inner join sis_processo_papel figura on figura.id_papel = papel.id
      inner join sis_processo_pessoa pessoas on pessoas.id_pessoa = figura.id_pessoa and pessoas.id_processo = figura.id_processo
      inner join sis_processo p on p.id = pessoas.id_processo
      inner join sis_pessoa pessoa on pessoa.id = pessoas.id_pessoa
      where papel.id = 3;