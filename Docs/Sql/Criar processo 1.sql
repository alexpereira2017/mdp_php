
select * from sis_processo;
select * from sis_questao;

insert into sis_processo (nome, descricao, dt_inicio, dt_criacao, hr_criacao)
values ("Primeira e grande avalia��o", "Processo para usar como sendo o primeiro teste durante o desenvolvimento",curdate(),curdate(), curtime());


insert into sis_questao (pergunta, influencia, acesso, peso_padrao, tipo_resposta, periodicidade, dt_criacao, hr_criacao)
values("Bom comportamento extraclasse", "PO", "PU", 5, "ME", 15, curdate(), curtime());
insert into sis_questao (pergunta, influencia, acesso, peso_padrao, tipo_resposta, periodicidade, dt_criacao, hr_criacao)
values("Respeita hierarquia", "PO", "PU", 5, "ME", 15, curdate(), curtime());
insert into sis_questao (pergunta, influencia, acesso, peso_padrao, tipo_resposta, periodicidade, dt_criacao, hr_criacao)
values("Colabora com a equipe", "PO", "PU", 8, "ME", 15, curdate(), curtime());
insert into sis_questao (pergunta, influencia, acesso, peso_padrao, tipo_resposta, periodicidade, dt_criacao, hr_criacao)
values("Entrega no prazo", "PO", "PU", 3, "ME", 15, curdate(), curtime());
insert into sis_questao (pergunta, influencia, acesso, peso_padrao, tipo_resposta, periodicidade, dt_criacao, hr_criacao)
values("Possui empatia", "PO", "PU", 2, "ME", 15, curdate(), curtime());

select * from sis_questao_opcoes;
insert into sis_questao_opcoes (valor, ordem, id_questao, dt_criacao, hr_criacao) values ('A favor', 1, 1, curdate(), curtime());
insert into sis_questao_opcoes (valor, ordem, id_questao, dt_criacao, hr_criacao) values ('Neutro', 2, 1, curdate(), curtime());
insert into sis_questao_opcoes (valor, ordem, id_questao, dt_criacao, hr_criacao) values ('Contra', 3, 1, curdate(), curtime());
insert into sis_questao_opcoes (valor, ordem, id_questao, dt_criacao, hr_criacao) values ('A favor', 1, 2, curdate(), curtime());
insert into sis_questao_opcoes (valor, ordem, id_questao, dt_criacao, hr_criacao) values ('Neutro', 2, 2, curdate(), curtime());
insert into sis_questao_opcoes (valor, ordem, id_questao, dt_criacao, hr_criacao) values ('Contra', 3, 2, curdate(), curtime());
insert into sis_questao_opcoes (valor, ordem, id_questao, dt_criacao, hr_criacao) values ('A favor', 1, 3, curdate(), curtime());
insert into sis_questao_opcoes (valor, ordem, id_questao, dt_criacao, hr_criacao) values ('Neutro', 2, 3, curdate(), curtime());
insert into sis_questao_opcoes (valor, ordem, id_questao, dt_criacao, hr_criacao) values ('Contra', 3, 3, curdate(), curtime());
insert into sis_questao_opcoes (valor, ordem, id_questao, dt_criacao, hr_criacao) values ('A favor', 1, 4, curdate(), curtime());
insert into sis_questao_opcoes (valor, ordem, id_questao, dt_criacao, hr_criacao) values ('Neutro', 2, 4, curdate(), curtime());
insert into sis_questao_opcoes (valor, ordem, id_questao, dt_criacao, hr_criacao) values ('Contra', 3, 4, curdate(), curtime());
insert into sis_questao_opcoes (valor, ordem, id_questao, dt_criacao, hr_criacao) values ('A favor', 1, 5, curdate(), curtime());
insert into sis_questao_opcoes (valor, ordem, id_questao, dt_criacao, hr_criacao) values ('Neutro', 2, 5, curdate(), curtime());
insert into sis_questao_opcoes (valor, ordem, id_questao, dt_criacao, hr_criacao) values ('Contra', 3, 5, curdate(), curtime());

insert into sis_papel (nome, dt_criacao, hr_criacao) values ("Coordenador", curdate(), curtime());
insert into sis_papel (nome, dt_criacao, hr_criacao) values ("Avaliador", curdate(), curtime());
insert into sis_papel (nome, dt_criacao, hr_criacao) values ("Avaliado", curdate(), curtime());

select * from sis_pessoa;
insert into sis_pessoa (nome, cargo, dt_criacao, hr_criacao) values ("Alex", "Presidente", curdate(), curtime());
insert into sis_pessoa (nome, cargo, dt_criacao, hr_criacao) values ("Aloisio", "Comiss�o T�cnica", curdate(), curtime());
insert into sis_pessoa (nome, cargo, dt_criacao, hr_criacao) values ("Guiga", "Comiss�o T�cnica", curdate(), curtime());
insert into sis_pessoa (nome, cargo, dt_criacao, hr_criacao) values ("Vinicius", "Atleta", curdate(), curtime());
insert into sis_pessoa (nome, cargo, dt_criacao, hr_criacao) values ("Segu", "Atleta", curdate(), curtime());
insert into sis_pessoa (nome, cargo, dt_criacao, hr_criacao) values ("Marquinhos", "Atleta", curdate(), curtime());
insert into sis_pessoa (nome, cargo, dt_criacao, hr_criacao) values ("Marcos", "Atleta", curdate(), curtime());
insert into sis_pessoa (nome, cargo, dt_criacao, hr_criacao) values ("Machado", "Atleta", curdate(), curtime());
insert into sis_pessoa (nome, cargo, dt_criacao, hr_criacao) values ("Danilo", "Atleta", curdate(), curtime());
insert into sis_pessoa (nome, cargo, dt_criacao, hr_criacao) values ("Eduardo", "Atleta", curdate(), curtime());
insert into sis_pessoa (nome, cargo, dt_criacao, hr_criacao) values ("Andre", "Atleta", curdate(), curtime());
insert into sis_pessoa (nome, cargo, dt_criacao, hr_criacao) values ("Ramiro", "Atleta", curdate(), curtime());
insert into sis_pessoa (nome, cargo, dt_criacao, hr_criacao) values ("Juan", "Atleta", curdate(), curtime());

select * from sis_processo_pessoa;
insert into sis_processo_pessoa (id_pessoa, id_processo,dt_criacao, hr_criacao) values (1,1, curdate(), curtime());
insert into sis_processo_pessoa (id_pessoa, id_processo,dt_criacao, hr_criacao) values (2,1, curdate(), curtime());
insert into sis_processo_pessoa (id_pessoa, id_processo,dt_criacao, hr_criacao) values (3,1, curdate(), curtime());
insert into sis_processo_pessoa (id_pessoa, id_processo,dt_criacao, hr_criacao) values (4,1, curdate(), curtime());
insert into sis_processo_pessoa (id_pessoa, id_processo,dt_criacao, hr_criacao) values (5,1, curdate(), curtime());
insert into sis_processo_pessoa (id_pessoa, id_processo,dt_criacao, hr_criacao) values (6,1, curdate(), curtime());
insert into sis_processo_pessoa (id_pessoa, id_processo,dt_criacao, hr_criacao) values (7,1, curdate(), curtime());
insert into sis_processo_pessoa (id_pessoa, id_processo,dt_criacao, hr_criacao) values (8,1, curdate(), curtime());
insert into sis_processo_pessoa (id_pessoa, id_processo,dt_criacao, hr_criacao) values (9,1, curdate(), curtime());
insert into sis_processo_pessoa (id_pessoa, id_processo,dt_criacao, hr_criacao) values (10,1, curdate(), curtime());
insert into sis_processo_pessoa (id_pessoa, id_processo,dt_criacao, hr_criacao) values (11,1, curdate(), curtime());
insert into sis_processo_pessoa (id_pessoa, id_processo,dt_criacao, hr_criacao) values (12,1, curdate(), curtime());
insert into sis_processo_pessoa (id_pessoa, id_processo,dt_criacao, hr_criacao) values (13,1, curdate(), curtime());

select * from sis_papel;
select * from sis_processo_papel;
insert into sis_processo_papel (id_processo, id_pessoa, id_papel,dt_criacao, hr_criacao) values (1,1,1,curdate(),curtime());

insert into sis_processo_papel (id_processo, id_pessoa, id_papel,dt_criacao, hr_criacao) values (1,1,2,curdate(),curtime());
insert into sis_processo_papel (id_processo, id_pessoa, id_papel,dt_criacao, hr_criacao) values (1,2,2,curdate(),curtime());
insert into sis_processo_papel (id_processo, id_pessoa, id_papel,dt_criacao, hr_criacao) values (1,3,2,curdate(),curtime());

insert into sis_processo_papel (id_processo, id_pessoa, id_papel,dt_criacao, hr_criacao) values (1,1,3,curdate(),curtime());
insert into sis_processo_papel (id_processo, id_pessoa, id_papel,dt_criacao, hr_criacao) values (1,2,3,curdate(),curtime());
insert into sis_processo_papel (id_processo, id_pessoa, id_papel,dt_criacao, hr_criacao) values (1,3,3,curdate(),curtime());
insert into sis_processo_papel (id_processo, id_pessoa, id_papel,dt_criacao, hr_criacao) values (1,4,3,curdate(),curtime());
insert into sis_processo_papel (id_processo, id_pessoa, id_papel,dt_criacao, hr_criacao) values (1,5,3,curdate(),curtime());
insert into sis_processo_papel (id_processo, id_pessoa, id_papel,dt_criacao, hr_criacao) values (1,6,3,curdate(),curtime());
insert into sis_processo_papel (id_processo, id_pessoa, id_papel,dt_criacao, hr_criacao) values (1,7,3,curdate(),curtime());
insert into sis_processo_papel (id_processo, id_pessoa, id_papel,dt_criacao, hr_criacao) values (1,8,3,curdate(),curtime());
insert into sis_processo_papel (id_processo, id_pessoa, id_papel,dt_criacao, hr_criacao) values (1,9,3,curdate(),curtime());
insert into sis_processo_papel (id_processo, id_pessoa, id_papel,dt_criacao, hr_criacao) values (1,10,3,curdate(),curtime());
insert into sis_processo_papel (id_processo, id_pessoa, id_papel,dt_criacao, hr_criacao) values (1,11,3,curdate(),curtime());
insert into sis_processo_papel (id_processo, id_pessoa, id_papel,dt_criacao, hr_criacao) values (1,12,3,curdate(),curtime());
insert into sis_processo_papel (id_processo, id_pessoa, id_papel,dt_criacao, hr_criacao) values (1,13,3,curdate(),curtime());

insert into sis_processo_questao (peso_especifico, ordem, id_processo, id_questao, dt_criacao, hr_criacao) values (5, 1, 1, 3, curdate(), curtime());
insert into sis_processo_questao (peso_especifico, ordem, id_processo, id_questao, dt_criacao, hr_criacao) values (1, 2, 1, 2, curdate(), curtime());
insert into sis_processo_questao (peso_especifico, ordem, id_processo, id_questao, dt_criacao, hr_criacao) values (2, 3, 1, 1, curdate(), curtime());
insert into sis_processo_questao (peso_especifico, ordem, id_processo, id_questao, dt_criacao, hr_criacao) values (2, 4, 1, 4, curdate(), curtime());
insert into sis_processo_questao (peso_especifico, ordem, id_processo, id_questao, dt_criacao, hr_criacao) values (5, 5, 1, 5, curdate(), curtime());

insert into sis_resposta (id_processo, id_avaliador, id_avaliado, id_questao, id_opcao, descricao, dt_criacao, hr_criacao) values (1,1,9,1,2,'',curdate(), curtime());
insert into sis_resposta (id_processo, id_avaliador, id_avaliado, id_questao, id_opcao, descricao, dt_criacao, hr_criacao) values (1,1,9,2,4,'',curdate(), curtime());
insert into sis_resposta (id_processo, id_avaliador, id_avaliado, id_questao, id_opcao, descricao, dt_criacao, hr_criacao) values (1,1,9,3,7,'',curdate(), curtime());
insert into sis_resposta (id_processo, id_avaliador, id_avaliado, id_questao, id_opcao, descricao, dt_criacao, hr_criacao) values (1,1,9,4,11,'',curdate(), curtime());
insert into sis_resposta (id_processo, id_avaliador, id_avaliado, id_questao, id_opcao, descricao, dt_criacao, hr_criacao) values (1,1,9,5,15,'',curdate(), curtime());
insert into sis_resposta (id_processo, id_avaliador, id_avaliado, id_questao, id_opcao, descricao, dt_criacao, hr_criacao) values (1,2,9,1,2,'',curdate(), curtime());
insert into sis_resposta (id_processo, id_avaliador, id_avaliado, id_questao, id_opcao, descricao, dt_criacao, hr_criacao) values (1,2,9,2,4,'',curdate(), curtime());
insert into sis_resposta (id_processo, id_avaliador, id_avaliado, id_questao, id_opcao, descricao, dt_criacao, hr_criacao) values (1,2,9,3,7,'',curdate(), curtime());
insert into sis_resposta (id_processo, id_avaliador, id_avaliado, id_questao, id_opcao, descricao, dt_criacao, hr_criacao) values (1,2,9,4,11,'',curdate(), curtime());
insert into sis_resposta (id_processo, id_avaliador, id_avaliado, id_questao, id_opcao, descricao, dt_criacao, hr_criacao) values (1,2,8,1,1,'',curdate(), curtime());
insert into sis_resposta (id_processo, id_avaliador, id_avaliado, id_questao, id_opcao, descricao, dt_criacao, hr_criacao) values (1,2,8,2,5,'',curdate(), curtime());
insert into sis_resposta (id_processo, id_avaliador, id_avaliado, id_questao, id_opcao, descricao, dt_criacao, hr_criacao) values (1,2,8,3,8,'',curdate(), curtime());
insert into sis_resposta (id_processo, id_avaliador, id_avaliado, id_questao, id_opcao, descricao, dt_criacao, hr_criacao) values (1,2,8,4,10,'',curdate(), curtime());

