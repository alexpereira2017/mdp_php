<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/excecao/ExceptionValidador.php';
  class ValidadorSegUsuarios {

    public function validar ( ModeloSegUsuarios $oModelo ) {
      $aValidar = array ( 10 => array('Nome'                , $oModelo->Nome                , 'varchar', true, array(50)),
                          20 => array('Email'               , $oModelo->Email               , 'varchar', true, array(50)),
                          30 => array('Senha'               , $oModelo->Senha               , 'varchar', true, array(50)),
                          40 => array('DtCadastro'          , $oModelo->DtCadastro          , 'date', true),
                          50 => array('Status'              , $oModelo->Status              , 'varchar', true, array(2)),
                          60 => array('CdNivel'             , $oModelo->CdNivel             , 'int', true, array(2)),
                          70 => array('Token'               , $oModelo->Token               , 'varchar', true, array(255)),
                          80 => array('SenhaProvisoria'     , $oModelo->SenhaProvisoria     , 'varchar', true, array(50)),
                          90 => array('NuTentativasAcesso'  , $oModelo->NuTentativasAcesso  , 'int', true, array(10)),
                          100 => array('NuAcessos'           , $oModelo->NuAcessos           , 'int', true, array(10)),
                          110 => array('IdPessoa'            , $oModelo->IdPessoa            , 'int', true, array(10)),
                          120 => array('IdUsuario'           , $oModelo->IdUsuario           , 'int', true, array(8)),
                          130 => array('DtCriacao'           , $oModelo->DtCriacao           , 'date', true),
                          140 => array('HrCriacao'           , $oModelo->HrCriacao           , 'time', true),
                        );
      if ($this->oUtil->valida_Preenchimento($aValidar) !== true) {
        $this->aMsg = $this->oUtil->aMsg;
        throw new exception_validacao();
      }
    }
  }