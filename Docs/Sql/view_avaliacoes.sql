
 create or replace view avaliacoes as    
    select avaliadores.id_processo as id_processo, 
           avaliadores.avaliador as avaliador, 
           avaliadores.id_avaliador as id_avaliador, 
           avaliados.pessoa as avaliado, 
           avaliados.id_avaliado as id_avaliado,
           md5(concat(avaliador, avaliados.pessoa, avaliados.papel, avaliados.processo, avaliados.id_processo)) as hash,
           resumo.nr_questoes as total_questoes,
           count(respostas.id) as respondidas,
           case 
            when count(respostas.id) = 0 then 'N�o iniciada'
            when count(respostas.id) = resumo.nr_questoes then 'Conclu�da'
            else 'Em andamento'
           end status
     from avaliadores
     inner join avaliados on avaliados.id_processo = avaliadores.id_processo
     inner join processo_resumo resumo on resumo.id_processo = avaliadores.id_processo
     left join sis_resposta respostas on respostas.id_processo = avaliadores.id_processo
                                        and respostas.id_avaliador = avaliadores.id_avaliador
                                        and respostas.id_avaliado = avaliados.id_avaliado
       group by avaliadores.id_processo , 
           avaliadores.avaliador , 
           avaliadores.id_avaliador , 
           avaliados.pessoa , 
           avaliados.id_avaliado ,
           resumo.nr_questoes ;
