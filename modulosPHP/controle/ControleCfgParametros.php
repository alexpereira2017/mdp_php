<?php
include_once 'modulosPHP/validador/ValidatorFormularioCfgParametros.php';
class controle_CfgParametros extends Modelo {
  
  private $oNegocio;
  private $oModelo;
  private $oVisao;
  private $oValidador;


  public function __construct() {
    
    $this->oModelo = new ModeloCfgParametros();
    $this->oNegocio = new NegocioCfgParametros();
    
    if (isset($_POST)) {
      if (isset($_POST['sAcao'])) {
        if ($_POST['sAcao'] == 'salvar') {

          try {
            $this->oValidador = new ValidadorCfgParametros();
            $this->oModelo->carregarDadosAposPost($_POST);
            $this->oValidador->validar($this->oModelo);

            $this->oNegocio->salvar($this->oModelo);
            
            $this->oModelo = new ModeloCfgParametros();
            $this->oModelo->mAcaoResultado = 0;
            $this->oModelo->sAcaoMsg       = 'Novo cadastro foi realizado com sucesso!';
            
          } catch (exception_validacao $ex) {
            $this->oModelo->mAcaoResultado = 2;
            $this->oModelo->sAcaoMsg       = $this->oValidador->aMsg['sMsg'];
          }          
        }
      }
    }

    $this->oVisao  = new VisaoCfgParametros($this->oModelo);
  }
  
  public function getVisao() {
    return $this->oVisao;
  }
}

