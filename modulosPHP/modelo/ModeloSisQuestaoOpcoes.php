<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/


  class ModeloSisQuestaoOpcoes extends ModeloAdmin { 
    public $Id;
    public $Descricao;
    public $Valor;
    public $Ordem;
    public $IdQuestao;
    public $IdUsuario;
    public $DtCriacao;
    public $HrCriacao;
    public $sConteudo;
    public $aListaModelos;

    public function carregarDadosAposPost($aDados) { 
      $this->Id         = (isset($aDados['CMPSisQuestaoOpcoesId']))         ? $aDados['CMPSisQuestaoOpcoesId'] : '';
      $this->Descricao  = (isset($aDados['CMPSisQuestaoOpcoesDescricao']))  ? $aDados['CMPSisQuestaoOpcoesDescricao'] : '';
      $this->Valor      = (isset($aDados['CMPSisQuestaoOpcoesValor']))      ? $aDados['CMPSisQuestaoOpcoesValor'] : '';
      $this->Ordem      = (isset($aDados['CMPSisQuestaoOpcoesOrdem']))      ? $aDados['CMPSisQuestaoOpcoesOrdem'] : '';
      $this->IdQuestao  = (isset($aDados['CMPSisQuestaoOpcoesIdQuestao']))  ? $aDados['CMPSisQuestaoOpcoesIdQuestao'] : '';
      $this->IdUsuario  = (isset($aDados['CMPSisQuestaoOpcoesIdUsuario']))  ? $aDados['CMPSisQuestaoOpcoesIdUsuario'] : '';
      $this->DtCriacao  = (isset($aDados['CMPSisQuestaoOpcoesDtCriacao']))  ? $aDados['CMPSisQuestaoOpcoesDtCriacao'] : '';
      $this->HrCriacao  = (isset($aDados['CMPSisQuestaoOpcoesHrCriacao']))  ? $aDados['CMPSisQuestaoOpcoesHrCriacao'] : '';

    }
  }