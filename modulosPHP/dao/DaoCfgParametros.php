<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/modelo/ModeloCfgParametros.php';
  class DaoCfgParametros {
      
    public    $aMsg = array();
    protected $iCdMsg;
    protected $sMsg;
    protected $sErro;
    public    $sBackpage;
    protected $oUtil;
    protected $oBd;
    protected $oModelo;
    protected $aModelos;

    public function __construct() {
      include_once 'modulosPHP/ajudantes/ClassConexao.php';
      $this->oBd   = new Conexao();
      $this->oUtil = new wTools();
    }
    public function inserir ( ModeloCfgParametros $oModelo ) {
      $sSql = "INSERT INTO nm_parametro
                           cd_parametro
                           tx_explicativo
                           cd_tipo_uso
                           cd_tipo
                           nu_limite_cadastro
                           status
                           nu_ordem
                           nu_importancia
                           tx_mascara
                           vl_padrao
                           id_usuario
                           dt_criacao
                           hr_criacao
                   VALUES ('".$this->oModelo->NmParametro."',
                           '".$this->oModelo->CdParametro."',
                           '".$this->oModelo->TxExplicativo."',
                           '".$this->oModelo->CdTipoUso."',
                           '".$this->oModelo->CdTipo."',
                           '".$this->oModelo->NuLimiteCadastro."',
                           '".$this->oModelo->Status."',
                           '".$this->oModelo->NuOrdem."',
                           '".$this->oModelo->NuImportancia."',
                           '".$this->oModelo->TxMascara."',
                           '".$this->oModelo->VlPadrao."',
                           '".$this->oModelo->IdUsuario."',
                           '".$this->oModelo->DtCriacao."',
                           '".$this->oModelo->HrCriacao."')";
      return $this->executar( $sSql );
    }

    public function editar ( ModeloCfgParametros $oModelo, $sFiltro ) {
      $sSql = "UPDATE cfg_parametros
                  SET nm_parametro        = '".$oModelo->NmParametro."',
                      cd_parametro        = '".$oModelo->CdParametro."',
                      tx_explicativo      = '".$oModelo->TxExplicativo."',
                      cd_tipo_uso          = '".$oModelo->CdTipoUso."',
                      cd_tipo             = '".$oModelo->CdTipo."',
                      nu_limite_cadastro   = '".$oModelo->NuLimiteCadastro."',
                      status             = '".$oModelo->Status."',
                      nu_ordem            = '".$oModelo->NuOrdem."',
                      nu_importancia      = '".$oModelo->NuImportancia."',
                      tx_mascara          = '".$oModelo->TxMascara."',
                      vl_padrao           = '".$oModelo->VlPadrao."',
                      id_usuario          = '".$oModelo->IdUsuario."',
                      dt_criacao          = '".$oModelo->DtCriacao."',
                      hr_criacao          = '".$oModelo->HrCriacao."'
                ".$sFiltro;
      return $this->executar( $sSql );
    }

    public function remover ( $sFiltro ) {
      $sSql = "DELETE FROM cfg_parametros". $sFiltro;
      return $this->executar( $sSql );
    }

    public function listar ( $mOpcoes = '') {
      $sFiltro = $this->oUtil->tratarOpcoesDeFiltro($mOpcoes);
      $sSql = 'SELECT id
                        nm_parametro
                        cd_parametro
                        tx_explicativo
                        cd_tipo_uso
                        cd_tipo
                        nu_limite_cadastro
                        status
                        nu_ordem
                        nu_importancia
                        tx_mascara
                        vl_padrao
                        id_usuario
                        dt_criacao
                        hr_criacao
                   FROM cfg_parametros
       '.$sFiltro;

      $mResultado = $this->oBd->query($sSql);

      $this->iLinhas = $this->oBd->getNumeroLinhas();

      if (!is_array($mResultado)) {
        $this->aMsg = $this->oBd->getMsg();
        return false;
      }

      for ($i= 0; $i < $this->iLinhas; $i++) { 
        $oModelo = new ModeloCfgParametros();
        $oModelo->Id                 = $mResultado[$i]['id'];
        $oModelo->NmParametro        = $mResultado[$i]['nm_parametro'];
        $oModelo->CdParametro        = $mResultado[$i]['cd_parametro'];
        $oModelo->TxExplicativo      = $mResultado[$i]['tx_explicativo'];
        $oModelo->CdTipoUso          = $mResultado[$i]['cd_tipo_uso'];
        $oModelo->CdTipo             = $mResultado[$i]['cd_tipo'];
        $oModelo->NuLimiteCadastro   = $mResultado[$i]['nu_limite_cadastro'];
        $oModelo->Status             = $mResultado[$i]['status'];
        $oModelo->NuOrdem            = $mResultado[$i]['nu_ordem'];
        $oModelo->NuImportancia      = $mResultado[$i]['nu_importancia'];
        $oModelo->TxMascara          = $mResultado[$i]['tx_mascara'];
        $oModelo->VlPadrao           = $mResultado[$i]['vl_padrao'];
        $oModelo->IdUsuario          = $mResultado[$i]['id_usuario'];
        $oModelo->DtCriacao          = $mResultado[$i]['dt_criacao'];
        $oModelo->HrCriacao          = $mResultado[$i]['hr_criacao'];
        $this->aModelo[] = $oModelo;
      }

      return $this->aModelo;
    }

    public function executar ( $sSql ) {
      if (!$this->oBd->execute( $sSql )) {
        $this->iCdMsg = 1;
        $this->sMsg  = 'Ocorreu um erro ao salvar o registro.';
        $bSucesso = false;
      } else {
        $this->iCdMsg = 0;
        $this->sMsg  = 'O registro foi salvo com sucesso!';
        $bSucesso = true;
      }

      $this->aMsg = array('iCdMsg' => $this->iCdMsg,
                            'sMsg' => $this->sMsg);
      return $bSucesso;
    }
  }
