<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/excecao/ExceptionValidador.php';
  class ValidadorSisProcessoQuestao {

    public function validar ( ModeloSisProcessoQuestao $oModelo ) {
      $aValidar = array ( 10 => array('PesoEspecifico' , $oModelo->PesoEspecifico , 'int', true, array(2)),
                          20 => array('Ordem'          , $oModelo->Ordem          , 'int', true, array(2)),
                          30 => array('IdProcesso'     , $oModelo->IdProcesso     , 'int', true, array(8)),
                          40 => array('IdQuestao'      , $oModelo->IdQuestao      , 'int', true, array(8)),
                          50 => array('IdUsuario'      , $oModelo->IdUsuario      , 'int', true, array(8)),
                          60 => array('DtCriacao'      , $oModelo->DtCriacao      , 'date', true),
                          70 => array('HrCriacao'      , $oModelo->HrCriacao      , 'time', true),
                        );
      if ($this->oUtil->valida_Preenchimento($aValidar) !== true) {
        $this->aMsg = $this->oUtil->aMsg;
        throw new exception_validacao();
      }
    }
  }