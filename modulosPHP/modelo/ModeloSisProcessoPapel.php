<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/


  class ModeloSisProcessoPapel extends ModeloAdmin { 
    public $Id;
    public $IdProcesso;
    public $IdPessoa;
    public $IdPapel;
    public $IdUsuario;
    public $DtCriacao;
    public $HrCriacao;
    public $sConteudo;
    public $aListaModelos;

    public function carregarDadosAposPost($aDados) { 
      $this->Id          = (isset($aDados['CMPSisProcessoPapelId']))          ? $aDados['CMPSisProcessoPapelId'] : '';
      $this->IdProcesso  = (isset($aDados['CMPSisProcessoPapelIdProcesso']))  ? $aDados['CMPSisProcessoPapelIdProcesso'] : '';
      $this->IdPessoa    = (isset($aDados['CMPSisProcessoPapelIdPessoa']))    ? $aDados['CMPSisProcessoPapelIdPessoa'] : '';
      $this->IdPapel     = (isset($aDados['CMPSisProcessoPapelIdPapel']))     ? $aDados['CMPSisProcessoPapelIdPapel'] : '';
      $this->IdUsuario   = (isset($aDados['CMPSisProcessoPapelIdUsuario']))   ? $aDados['CMPSisProcessoPapelIdUsuario'] : '';
      $this->DtCriacao   = (isset($aDados['CMPSisProcessoPapelDtCriacao']))   ? $aDados['CMPSisProcessoPapelDtCriacao'] : '';
      $this->HrCriacao   = (isset($aDados['CMPSisProcessoPapelHrCriacao']))   ? $aDados['CMPSisProcessoPapelHrCriacao'] : '';

    }
  }