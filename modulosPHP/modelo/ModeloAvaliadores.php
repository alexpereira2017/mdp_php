<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/


  class ModeloAvaliadores extends ModeloAdmin { 
    public $Avaliador;
    public $IdAvaliador;
    public $Papel;
    public $Processo;
    public $IdProcesso;
    public $Hash;
    public $sConteudo;
    public $aListaModelos;

    public function carregarDadosAposPost($aDados) { 
      $this->Avaliador    = (isset($aDados['CMPAvaliadoresAvaliador']))    ? $aDados['CMPAvaliadoresAvaliador'] : '';
      $this->IdAvaliador  = (isset($aDados['CMPAvaliadoresIdAvaliador']))  ? $aDados['CMPAvaliadoresIdAvaliador'] : '';
      $this->Papel        = (isset($aDados['CMPAvaliadoresPapel']))        ? $aDados['CMPAvaliadoresPapel'] : '';
      $this->Processo     = (isset($aDados['CMPAvaliadoresProcesso']))     ? $aDados['CMPAvaliadoresProcesso'] : '';
      $this->IdProcesso   = (isset($aDados['CMPAvaliadoresIdProcesso']))   ? $aDados['CMPAvaliadoresIdProcesso'] : '';
      $this->Hash         = (isset($aDados['CMPAvaliadoresHash']))         ? $aDados['CMPAvaliadoresHash'] : '';

    }
  }