<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/excecao/ExceptionValidador.php';
  class ValidadorSisProcesso {

    public function validar ( ModeloSisProcesso $oModelo ) {
      $aValidar = array ( 10 => array('Nome'           , $oModelo->Nome           , 'varchar', true, array(255)),
                          20 => array('Descricao'      , $oModelo->Descricao      , 'text', true),
                          30 => array('DtInicio'       , $oModelo->DtInicio       , 'date', true),
                          40 => array('DtEncerramento' , $oModelo->DtEncerramento , 'date', true),
                          50 => array('IdUsuario'      , $oModelo->IdUsuario      , 'int', true, array(8)),
                          60 => array('DtCriacao'      , $oModelo->DtCriacao      , 'date', true),
                          70 => array('HrCriacao'      , $oModelo->HrCriacao      , 'time', true),
                        );
      if ($this->oUtil->valida_Preenchimento($aValidar) !== true) {
        $this->aMsg = $this->oUtil->aMsg;
        throw new exception_validacao();
      }
    }
  }