create or replace view processo_resumo as 
    select p.id as id_processo, count(pq.id) as nr_questoes , p.nome, dt_inicio, dt_encerramento, 
        case 
          when dt_inicio > curdate() then 'N�o come�ou'
          when dt_encerramento < curdate() then 'Encerrado'
          else 'Andamento' end as status
      from sis_processo p
      inner join sis_processo_questao pq on p.id = pq.id_processo