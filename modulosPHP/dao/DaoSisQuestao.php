<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/modelo/ModeloSisQuestao.php';
  class DaoSisQuestao {
      
    public    $aMsg = array();
    protected $iCdMsg;
    protected $sMsg;
    protected $sErro;
    public    $sBackpage;
    protected $oUtil;
    protected $oBd;
    protected $oModelo;
    protected $aModelos;

    public function __construct() {
      include_once 'modulosPHP/ajudantes/ClassConexao.php';
      $this->oBd   = new Conexao();
      $this->oUtil = new wTools();
    }
    public function inserir ( ModeloSisQuestao $oModelo ) {
      $sSql = "INSERT INTO pergunta
                           influencia
                           acesso
                           peso_padrao
                           tipo_resposta
                           periodicidade
                           id_usuario
                           dt_criacao
                           hr_criacao
                   VALUES ('".$this->oModelo->Pergunta."',
                           '".$this->oModelo->Influencia."',
                           '".$this->oModelo->Acesso."',
                           '".$this->oModelo->PesoPadrao."',
                           '".$this->oModelo->TipoResposta."',
                           '".$this->oModelo->Periodicidade."',
                           '".$this->oModelo->IdUsuario."',
                           '".$this->oModelo->DtCriacao."',
                           '".$this->oModelo->HrCriacao."')";
      return $this->executar( $sSql );
    }

    public function editar ( ModeloSisQuestao $oModelo, $sFiltro ) {
      $sSql = "UPDATE sis_questao
                  SET pergunta      = '".$oModelo->Pergunta."',
                      influencia    = '".$oModelo->Influencia."',
                      acesso        = '".$oModelo->Acesso."',
                      peso_padrao    = '".$oModelo->PesoPadrao."',
                      tipo_resposta  = '".$oModelo->TipoResposta."',
                      periodicidade = '".$oModelo->Periodicidade."',
                      id_usuario     = '".$oModelo->IdUsuario."',
                      dt_criacao     = '".$oModelo->DtCriacao."',
                      hr_criacao     = '".$oModelo->HrCriacao."'
                ".$sFiltro;
      return $this->executar( $sSql );
    }

    public function remover ( $sFiltro ) {
      $sSql = "DELETE FROM sis_questao". $sFiltro;
      return $this->executar( $sSql );
    }

    public function listar ( $mOpcoes = '') {
      $sFiltro = $this->oUtil->tratarOpcoesDeFiltro($mOpcoes);
      $sSql = 'SELECT id
                        pergunta
                        influencia
                        acesso
                        peso_padrao
                        tipo_resposta
                        periodicidade
                        id_usuario
                        dt_criacao
                        hr_criacao
                   FROM sis_questao
       '.$sFiltro;

      $mResultado = $this->oBd->query($sSql);

      $this->iLinhas = $this->oBd->getNumeroLinhas();

      if (!is_array($mResultado)) {
        $this->aMsg = $this->oBd->getMsg();
        return false;
      }

      for ($i= 0; $i < $this->iLinhas; $i++) { 
        $oModelo = new ModeloSisQuestao();
        $oModelo->Id            = $mResultado[$i]['id'];
        $oModelo->Pergunta      = $mResultado[$i]['pergunta'];
        $oModelo->Influencia    = $mResultado[$i]['influencia'];
        $oModelo->Acesso        = $mResultado[$i]['acesso'];
        $oModelo->PesoPadrao    = $mResultado[$i]['peso_padrao'];
        $oModelo->TipoResposta  = $mResultado[$i]['tipo_resposta'];
        $oModelo->Periodicidade = $mResultado[$i]['periodicidade'];
        $oModelo->IdUsuario     = $mResultado[$i]['id_usuario'];
        $oModelo->DtCriacao     = $mResultado[$i]['dt_criacao'];
        $oModelo->HrCriacao     = $mResultado[$i]['hr_criacao'];
        $this->aModelo[] = $oModelo;
      }

      return $this->aModelo;
    }

    public function executar ( $sSql ) {
      if (!$this->oBd->execute( $sSql )) {
        $this->iCdMsg = 1;
        $this->sMsg  = 'Ocorreu um erro ao salvar o registro.';
        $bSucesso = false;
      } else {
        $this->iCdMsg = 0;
        $this->sMsg  = 'O registro foi salvo com sucesso!';
        $bSucesso = true;
      }

      $this->aMsg = array('iCdMsg' => $this->iCdMsg,
                            'sMsg' => $this->sMsg);
      return $bSucesso;
    }
  }
