<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/


  class ModeloSisTag extends ModeloAdmin { 
    public $Id;
    public $Nome;
    public $PesoPadrao;
    public $IdUsuario;
    public $DtCriacao;
    public $HrCriacao;
    public $sConteudo;
    public $aListaModelos;

    public function carregarDadosAposPost($aDados) { 
      $this->Id          = (isset($aDados['CMPSisTagId']))          ? $aDados['CMPSisTagId'] : '';
      $this->Nome        = (isset($aDados['CMPSisTagNome']))        ? $aDados['CMPSisTagNome'] : '';
      $this->PesoPadrao  = (isset($aDados['CMPSisTagPesoPadrao']))  ? $aDados['CMPSisTagPesoPadrao'] : '';
      $this->IdUsuario   = (isset($aDados['CMPSisTagIdUsuario']))   ? $aDados['CMPSisTagIdUsuario'] : '';
      $this->DtCriacao   = (isset($aDados['CMPSisTagDtCriacao']))   ? $aDados['CMPSisTagDtCriacao'] : '';
      $this->HrCriacao   = (isset($aDados['CMPSisTagHrCriacao']))   ? $aDados['CMPSisTagHrCriacao'] : '';

    }
  }