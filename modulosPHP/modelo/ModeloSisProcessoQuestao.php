<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/


  class ModeloSisProcessoQuestao extends ModeloAdmin { 
    public $Id;
    public $PesoEspecifico;
    public $Ordem;
    public $IdProcesso;
    public $IdQuestao;
    public $IdUsuario;
    public $DtCriacao;
    public $HrCriacao;
    public $sConteudo;
    public $aListaModelos;

    public function carregarDadosAposPost($aDados) { 
      $this->Id              = (isset($aDados['CMPSisProcessoQuestaoId']))              ? $aDados['CMPSisProcessoQuestaoId'] : '';
      $this->PesoEspecifico  = (isset($aDados['CMPSisProcessoQuestaoPesoEspecifico']))  ? $aDados['CMPSisProcessoQuestaoPesoEspecifico'] : '';
      $this->Ordem           = (isset($aDados['CMPSisProcessoQuestaoOrdem']))           ? $aDados['CMPSisProcessoQuestaoOrdem'] : '';
      $this->IdProcesso      = (isset($aDados['CMPSisProcessoQuestaoIdProcesso']))      ? $aDados['CMPSisProcessoQuestaoIdProcesso'] : '';
      $this->IdQuestao       = (isset($aDados['CMPSisProcessoQuestaoIdQuestao']))       ? $aDados['CMPSisProcessoQuestaoIdQuestao'] : '';
      $this->IdUsuario       = (isset($aDados['CMPSisProcessoQuestaoIdUsuario']))       ? $aDados['CMPSisProcessoQuestaoIdUsuario'] : '';
      $this->DtCriacao       = (isset($aDados['CMPSisProcessoQuestaoDtCriacao']))       ? $aDados['CMPSisProcessoQuestaoDtCriacao'] : '';
      $this->HrCriacao       = (isset($aDados['CMPSisProcessoQuestaoHrCriacao']))       ? $aDados['CMPSisProcessoQuestaoHrCriacao'] : '';

    }
  }