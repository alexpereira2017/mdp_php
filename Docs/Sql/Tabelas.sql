drop view avaliacoes;
drop view avaliadores;
drop view avaliados;

drop table sis_resposta;
drop table sis_processo_papel;
drop table sis_processo_pessoa;
drop table cfg_parametros_valores;
drop table cfg_parametros;
drop table seg_loggeral;
drop table seg_usuarios;
drop table sis_papel;
drop table sis_pessoa;
drop table sis_processo_questao_tag;
drop table sis_processo_questao;
drop table sis_questao_opcoes;
drop table sis_questao;
drop table sis_processo;
drop table sis_tag;


-- create database mdp;

CREATE TABLE seg_loggeral (
  id int(8) NOT NULL AUTO_INCREMENT,
  nome varchar(255) DEFAULT NULL,
  descricao text,
  codigo varchar(40) DEFAULT NULL,
  acao varchar(2) DEFAULT NULL COMMENT 'I: Inserir, E: Editar, R: Remover, L: Listar',
  ip varchar(30) DEFAULT NULL,
  trace text,
  id_usuario int(8) DEFAULT NULL,
  dt_criacao date DEFAULT NULL,
  hr_criacao time DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE seg_usuarios (
  id int(10) NOT NULL AUTO_INCREMENT,
  nome varchar(50) DEFAULT NULL,
  email varchar(50) DEFAULT NULL,
  senha varchar(50) DEFAULT NULL,
  dt_cadastro date DEFAULT NULL,
  status varchar(2) DEFAULT NULL,
  cd_nivel int(2) DEFAULT NULL,
  token varchar(255) DEFAULT NULL,
  senha_provisoria varchar(50) DEFAULT NULL,
  nu_tentativas_acesso int(10) DEFAULT NULL,
  nu_acessos int(10) DEFAULT NULL,
  id_pessoa int(10) DEFAULT NULL,
  id_usuario int(8) DEFAULT NULL,
  dt_criacao date DEFAULT NULL,
  hr_criacao time DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE cfg_parametros (
  id int(8) NOT NULL AUTO_INCREMENT,
  nm_parametro varchar(55) DEFAULT NULL COMMENT 'Nome do registro',
  cd_parametro varchar(20) DEFAULT NULL COMMENT 'Codigo para identificar o parametro na hora de utilizacao do valor salvo',
  tx_explicativo varchar(255) DEFAULT NULL COMMENT 'Texto que apresentado ao usuario',
  cd_tipo_uso varchar(2) DEFAULT NULL COMMENT 'CF: Configuracao da plataforma de acesso ao adm master, PM: Parametro com acesso pelo usuario',
  cd_tipo varchar(2) DEFAULT NULL COMMENT 'M: Moeda, A: Textarea, H:Html, I:Numero, S:Texto, D:Data,O:Lista de opcoes (na tabela tbaslstcfg), M:Lista de opcoes multi-select, H:Hora, L:Logico,T:Tabela,P:Password',
  nu_limite_cadastro int(2) DEFAULT '1' COMMENT 'Quantidade de valores que podem ser adicionados',
  status char(2) DEFAULT 'AT',
  nu_ordem int(2) DEFAULT NULL,
  nu_importancia int(2) DEFAULT NULL,
  tx_mascara text COMMENT 'Validacao do tipo de dado que deve ser inserido',
  vl_padrao varchar(255) DEFAULT NULL,
  id_usuario int(8) DEFAULT NULL,
  dt_criacao date DEFAULT NULL,
  hr_criacao time DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE cfg_parametros_valores (
  id int(8) NOT NULL AUTO_INCREMENT,
  id_parametro int(8) DEFAULT NULL,
  tx_valor varchar(255) DEFAULT NULL,
  tx_func text,
  status varchar(2) DEFAULT 'AT',
  id_usuario int(8) DEFAULT NULL,
  dt_criacao date DEFAULT NULL,
  hr_criacao time DEFAULT NULL,
  PRIMARY KEY (id),
  KEY id_parametro (id_parametro),
  CONSTRAINT cfg_parametros_valores_ibfk_1 FOREIGN KEY (id_parametro) REFERENCES cfg_parametros (id)
);

create table sis_processo(
  id int(8) NOT NULL AUTO_INCREMENT,
  nome varchar(255),
  descricao text,
  dt_inicio date,
  dt_encerramento date,
  id_usuario int(8) DEFAULT NULL,
  dt_criacao date DEFAULT NULL,
  hr_criacao time DEFAULT NULL,
  PRIMARY KEY (id)
);

create table sis_questao(
  id int(8) NOT NULL AUTO_INCREMENT,
  pergunta varchar(400),
  influencia char(2) COMMENT 'PO: Positiva, NE: Negativa',
  acesso char(2) COMMENT 'PU: Publico, PR: Privado',
  peso_padrao int(2),
  tipo_resposta char(2) COMMENT 'ME:Multipla escolha, OB: Objetiva, DE: Descritiva',
  periodicidade int(3) COMMENT 'Em dias',
  id_usuario int(8) DEFAULT NULL,
  dt_criacao date DEFAULT NULL,
  hr_criacao time DEFAULT NULL,
  PRIMARY KEY (id)
);

create table sis_processo_questao(
  id int(8) NOT NULL AUTO_INCREMENT,
  peso_especifico int(2),
  ordem int(2),
  id_processo int(8),
  id_questao int(8),
  id_usuario int(8) DEFAULT NULL,
  dt_criacao date DEFAULT NULL,
  hr_criacao time DEFAULT NULL,
  CONSTRAINT sis_processo_questoes_ibfk_1 FOREIGN KEY (id_processo) REFERENCES sis_processo (id),
  CONSTRAINT sis_processo_questoes_ibfk_2 FOREIGN KEY (id_questao) REFERENCES sis_questao (id),
  PRIMARY KEY (id)
);

create table sis_tag(
  id int(8) NOT NULL AUTO_INCREMENT,
  nome varchar(100),
  peso_padrao int(2),
  id_usuario int(8) DEFAULT NULL,
  dt_criacao date DEFAULT NULL,
  hr_criacao time DEFAULT NULL,
  PRIMARY KEY (id)
);
  
create table sis_processo_questao_tag(
  id int(8) NOT NULL AUTO_INCREMENT,
  peso_especifico int(2),
  id_tag int(8),
  id_processo_questao int(8),
  id_usuario int(8) DEFAULT NULL,
  dt_criacao date DEFAULT NULL,
  hr_criacao time DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT sis_processo_questao_tag_ibfk_1 FOREIGN KEY (id_tag) REFERENCES sis_tag (id),
  CONSTRAINT sis_processo_questao_tag_ibfk_2 FOREIGN KEY (id_processo_questao) REFERENCES sis_processo_questao (id)
);
  
create table sis_questao_opcoes(
  id int(8) NOT NULL AUTO_INCREMENT,
  descricao varchar(400),
  valor varchar(400),
  ordem int(2),
  id_questao int(8) DEFAULT NULL,
  id_usuario int(8) DEFAULT NULL,
  dt_criacao date DEFAULT NULL,
  hr_criacao time DEFAULT NULL,
  CONSTRAINT sis_questao_opcoes_ibfk_1 FOREIGN KEY (id_questao) REFERENCES sis_questao (id),
  PRIMARY KEY (id)
);

create table sis_pessoa(
  id int(8) NOT NULL AUTO_INCREMENT,
  nome varchar(120),
  cargo varchar(80),
  email varchar(80),
  id_usuario int(8) DEFAULT NULL,
  dt_criacao date DEFAULT NULL,
  hr_criacao time DEFAULT NULL,
  PRIMARY KEY (id)
);



create table sis_processo_pessoa(
  id int(8) NOT NULL AUTO_INCREMENT,
  id_processo int(8),
  id_pessoa int(8),
  id_usuario int(8) DEFAULT NULL,
  dt_criacao date DEFAULT NULL,
  hr_criacao time DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY (id_processo, id_pessoa),
  CONSTRAINT sis_processo_pessoas_ibfk_1 FOREIGN KEY (id_processo) REFERENCES sis_processo (id),
  CONSTRAINT sis_processo_pessoas_ibfk_2 FOREIGN KEY (id_pessoa) REFERENCES sis_pessoa (id)

);

create table sis_papel(
  id int(8) NOT NULL AUTO_INCREMENT,
  nome varchar(50),
  id_usuario int(8) DEFAULT NULL,
  dt_criacao date DEFAULT NULL,
  hr_criacao time DEFAULT NULL,
  PRIMARY KEY (id)
);



create table sis_processo_papel(
  id int(8) NOT NULL AUTO_INCREMENT,
  id_processo int(8),
  id_pessoa int(8),
  id_papel int(8),
  id_usuario int(8) DEFAULT NULL,
  dt_criacao date DEFAULT NULL,
  hr_criacao time DEFAULT null,
  PRIMARY KEY (id),
  UNIQUE KEY (id_processo, id_pessoa, id_papel),
  CONSTRAINT sis_processo_papel_ibfk_1 FOREIGN KEY (id_processo) REFERENCES sis_processo_pessoa (id_processo),
  CONSTRAINT sis_processo_papel_ibfk_2 FOREIGN KEY (id_pessoa) REFERENCES sis_processo_pessoa (id_pessoa),
  CONSTRAINT sis_processo_papel_ibfk_3 FOREIGN KEY (id_papel) REFERENCES sis_papel (id)
);


create table sis_resposta(
  id int(8) NOT NULL AUTO_INCREMENT,
  id_processo int(8),
  id_avaliador int(8) COMMENT 'Quem realizou a avaliacao',
  id_avaliado int(8) COMMENT 'Usuario que recebeu a resposta',
  id_questao int(8) COMMENT 'Localiza a questao respondida',
  id_opcao int(8) COMMENT 'Caso seja multipla escolha',
  descricao text COMMENT 'Caso seja resposta dissertativa',
  id_usuario int(8) DEFAULT NULL,
  dt_criacao date DEFAULT NULL,
  hr_criacao time DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT sis_resposta_ibfk_1 FOREIGN KEY (id_processo) REFERENCES sis_processo (id),
  CONSTRAINT sis_resposta_ibfk_2 FOREIGN KEY (id_avaliador) REFERENCES sis_processo_papel (id),
  CONSTRAINT sis_resposta_ibfk_3 FOREIGN KEY (id_avaliado) REFERENCES sis_processo_papel (id),
  CONSTRAINT sis_resposta_ibfk_4 FOREIGN KEY (id_questao) REFERENCES sis_processo_papel (id),
  CONSTRAINT sis_resposta_ibfk_5 FOREIGN KEY (id_opcao) REFERENCES sis_questao_opcoes (id)
);


