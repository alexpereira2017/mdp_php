<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/modelo/ModeloAvaliados.php';
  class DaoAvaliados {
      
    public    $aMsg = array();
    protected $iCdMsg;
    protected $sMsg;
    protected $sErro;
    public    $sBackpage;
    protected $oUtil;
    protected $oBd;
    protected $oModelo;
    protected $aModelos;

    public function __construct() {
      include_once 'modulosPHP/ajudantes/ClassConexao.php';
      $this->oBd   = new Conexao();
      $this->oUtil = new wTools();
    }
    public function inserir ( ModeloAvaliados $oModelo ) {
      $sSql = "INSERT INTO id_avaliado
                           papel
                           processo
                           id_processo
                   VALUES ('".$this->oModelo->IdAvaliado."',
                           '".$this->oModelo->Papel."',
                           '".$this->oModelo->Processo."',
                           '".$this->oModelo->IdProcesso."')";
      return $this->executar( $sSql );
    }

    public function editar ( ModeloAvaliados $oModelo, $sFiltro ) {
      $sSql = "UPDATE avaliados
                  SET id_avaliado  = '".$oModelo->IdAvaliado."',
                      papel       = '".$oModelo->Papel."',
                      processo    = '".$oModelo->Processo."',
                      id_processo  = '".$oModelo->IdProcesso."'
                ".$sFiltro;
      return $this->executar( $sSql );
    }

    public function remover ( $sFiltro ) {
      $sSql = "DELETE FROM avaliados". $sFiltro;
      return $this->executar( $sSql );
    }

    public function listar ( $mOpcoes = '') {
      $sFiltro = $this->oUtil->tratarOpcoesDeFiltro($mOpcoes);
      $sSql = 'SELECT pessoa
                        id_avaliado
                        papel
                        processo
                        id_processo
                   FROM avaliados
       '.$sFiltro;

      $mResultado = $this->oBd->query($sSql);

      $this->iLinhas = $this->oBd->getNumeroLinhas();

      if (!is_array($mResultado)) {
        $this->aMsg = $this->oBd->getMsg();
        return false;
      }

      for ($i= 0; $i < $this->iLinhas; $i++) { 
        $oModelo = new ModeloAvaliados();
        $oModelo->Pessoa      = $mResultado[$i]['pessoa'];
        $oModelo->IdAvaliado  = $mResultado[$i]['id_avaliado'];
        $oModelo->Papel       = $mResultado[$i]['papel'];
        $oModelo->Processo    = $mResultado[$i]['processo'];
        $oModelo->IdProcesso  = $mResultado[$i]['id_processo'];
        $this->aModelo[] = $oModelo;
      }

      return $this->aModelo;
    }

    public function executar ( $sSql ) {
      if (!$this->oBd->execute( $sSql )) {
        $this->iCdMsg = 1;
        $this->sMsg  = 'Ocorreu um erro ao salvar o registro.';
        $bSucesso = false;
      } else {
        $this->iCdMsg = 0;
        $this->sMsg  = 'O registro foi salvo com sucesso!';
        $bSucesso = true;
      }

      $this->aMsg = array('iCdMsg' => $this->iCdMsg,
                            'sMsg' => $this->sMsg);
      return $bSucesso;
    }
  }
