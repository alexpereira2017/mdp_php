<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/


  class ModeloSegLoggeral extends ModeloAdmin { 
    public $Id;
    public $Nome;
    public $Descricao;
    public $Codigo;
    public $Acao;
    public $Ip;
    public $Trace;
    public $IdUsuario;
    public $DtCriacao;
    public $HrCriacao;
    public $sConteudo;
    public $aListaModelos;

    public function carregarDadosAposPost($aDados) { 
      $this->Id         = (isset($aDados['CMPSegLoggeralId']))         ? $aDados['CMPSegLoggeralId'] : '';
      $this->Nome       = (isset($aDados['CMPSegLoggeralNome']))       ? $aDados['CMPSegLoggeralNome'] : '';
      $this->Descricao  = (isset($aDados['CMPSegLoggeralDescricao']))  ? $aDados['CMPSegLoggeralDescricao'] : '';
      $this->Codigo     = (isset($aDados['CMPSegLoggeralCodigo']))     ? $aDados['CMPSegLoggeralCodigo'] : '';
      $this->Acao       = (isset($aDados['CMPSegLoggeralAcao']))       ? $aDados['CMPSegLoggeralAcao'] : '';
      $this->Ip         = (isset($aDados['CMPSegLoggeralIp']))         ? $aDados['CMPSegLoggeralIp'] : '';
      $this->Trace      = (isset($aDados['CMPSegLoggeralTrace']))      ? $aDados['CMPSegLoggeralTrace'] : '';
      $this->IdUsuario  = (isset($aDados['CMPSegLoggeralIdUsuario']))  ? $aDados['CMPSegLoggeralIdUsuario'] : '';
      $this->DtCriacao  = (isset($aDados['CMPSegLoggeralDtCriacao']))  ? $aDados['CMPSegLoggeralDtCriacao'] : '';
      $this->HrCriacao  = (isset($aDados['CMPSegLoggeralHrCriacao']))  ? $aDados['CMPSegLoggeralHrCriacao'] : '';

    }
  }