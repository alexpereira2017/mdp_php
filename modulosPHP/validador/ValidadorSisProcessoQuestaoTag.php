<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/excecao/ExceptionValidador.php';
  class ValidadorSisProcessoQuestaoTag {

    public function validar ( ModeloSisProcessoQuestaoTag $oModelo ) {
      $aValidar = array ( 10 => array('PesoEspecifico'     , $oModelo->PesoEspecifico     , 'int', true, array(2)),
                          20 => array('IdTag'              , $oModelo->IdTag              , 'int', true, array(8)),
                          30 => array('IdProcessoQuestao'  , $oModelo->IdProcessoQuestao  , 'int', true, array(8)),
                          40 => array('IdUsuario'          , $oModelo->IdUsuario          , 'int', true, array(8)),
                          50 => array('DtCriacao'          , $oModelo->DtCriacao          , 'date', true),
                          60 => array('HrCriacao'          , $oModelo->HrCriacao          , 'time', true),
                        );
      if ($this->oUtil->valida_Preenchimento($aValidar) !== true) {
        $this->aMsg = $this->oUtil->aMsg;
        throw new exception_validacao();
      }
    }
  }