<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/modelo/ModeloCfgParametrosValores.php';
  class DaoCfgParametrosValores {
      
    public    $aMsg = array();
    protected $iCdMsg;
    protected $sMsg;
    protected $sErro;
    public    $sBackpage;
    protected $oUtil;
    protected $oBd;
    protected $oModelo;
    protected $aModelos;

    public function __construct() {
      include_once 'modulosPHP/ajudantes/ClassConexao.php';
      $this->oBd   = new Conexao();
      $this->oUtil = new wTools();
    }
    public function inserir ( ModeloCfgParametrosValores $oModelo ) {
      $sSql = "INSERT INTO id_parametro
                           tx_valor
                           tx_func
                           status
                           id_usuario
                           dt_criacao
                           hr_criacao
                   VALUES ('".$this->oModelo->IdParametro."',
                           '".$this->oModelo->TxValor."',
                           '".$this->oModelo->TxFunc."',
                           '".$this->oModelo->Status."',
                           '".$this->oModelo->IdUsuario."',
                           '".$this->oModelo->DtCriacao."',
                           '".$this->oModelo->HrCriacao."')";
      return $this->executar( $sSql );
    }

    public function editar ( ModeloCfgParametrosValores $oModelo, $sFiltro ) {
      $sSql = "UPDATE cfg_parametros_valores
                  SET id_parametro  = '".$oModelo->IdParametro."',
                      tx_valor      = '".$oModelo->TxValor."',
                      tx_func       = '".$oModelo->TxFunc."',
                      status       = '".$oModelo->Status."',
                      id_usuario    = '".$oModelo->IdUsuario."',
                      dt_criacao    = '".$oModelo->DtCriacao."',
                      hr_criacao    = '".$oModelo->HrCriacao."'
                ".$sFiltro;
      return $this->executar( $sSql );
    }

    public function remover ( $sFiltro ) {
      $sSql = "DELETE FROM cfg_parametros_valores". $sFiltro;
      return $this->executar( $sSql );
    }

    public function listar ( $mOpcoes = '') {
      $sFiltro = $this->oUtil->tratarOpcoesDeFiltro($mOpcoes);
      $sSql = 'SELECT id
                        id_parametro
                        tx_valor
                        tx_func
                        status
                        id_usuario
                        dt_criacao
                        hr_criacao
                   FROM cfg_parametros_valores
       '.$sFiltro;

      $mResultado = $this->oBd->query($sSql);

      $this->iLinhas = $this->oBd->getNumeroLinhas();

      if (!is_array($mResultado)) {
        $this->aMsg = $this->oBd->getMsg();
        return false;
      }

      for ($i= 0; $i < $this->iLinhas; $i++) { 
        $oModelo = new ModeloCfgParametrosValores();
        $oModelo->Id           = $mResultado[$i]['id'];
        $oModelo->IdParametro  = $mResultado[$i]['id_parametro'];
        $oModelo->TxValor      = $mResultado[$i]['tx_valor'];
        $oModelo->TxFunc       = $mResultado[$i]['tx_func'];
        $oModelo->Status       = $mResultado[$i]['status'];
        $oModelo->IdUsuario    = $mResultado[$i]['id_usuario'];
        $oModelo->DtCriacao    = $mResultado[$i]['dt_criacao'];
        $oModelo->HrCriacao    = $mResultado[$i]['hr_criacao'];
        $this->aModelo[] = $oModelo;
      }

      return $this->aModelo;
    }

    public function executar ( $sSql ) {
      if (!$this->oBd->execute( $sSql )) {
        $this->iCdMsg = 1;
        $this->sMsg  = 'Ocorreu um erro ao salvar o registro.';
        $bSucesso = false;
      } else {
        $this->iCdMsg = 0;
        $this->sMsg  = 'O registro foi salvo com sucesso!';
        $bSucesso = true;
      }

      $this->aMsg = array('iCdMsg' => $this->iCdMsg,
                            'sMsg' => $this->sMsg);
      return $bSucesso;
    }
  }
