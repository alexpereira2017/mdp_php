<?php

/**
 /*
  email                          - Email
  senha-correta                  - Senha
  float                          - Float
  faixa                          - Faixa de n�meros, ex 0 ~ 100
  faixa-baixa                    - Faixa de n�meros > que X
  faixa-alta			 - Faixa de n�meros < que X
  dt-bd, dt-db, dt-br, data      - Data no formato dd/mm/yyyy
  date                           - Data no formato yyyy-mm-dd
  moeda                          - Moeda, reais
  digito			 - D�gitos
  caracteres			 - Caracteres
  cep                            - Cep
  texto, varchar		 - Texto, gen�rico
  telefone			 - Telefone (xx)xxxx-xxxx ou xxxx-xxxx
  abrangencia			 - Algum termo pre-definido em um array par�metro 4
  cnpj                           - CNPJ
  text                           - Testa se � um texto e limita at� 1000 caracteres
  enum			         - Permite somente as op��es permitidas
  senha
  logico			 - 
 *
 * @author Alex
 */
class ClassValidador {
  
  public $aValidar;
  public $aMsg;


  public function ValidarMaisItens(array $aMaisItens) {
      foreach ($aMaisItens as $chave => $valor) {
        $this->aValidar[$chave] = $valor;
      }
    }

  public function Validar () {
    if ($this->ValidarPreenchimento($this->aValidar) !== true) {
      throw new exception_validacao($this->aMsg['iCodigoMensagem'], $this->aMsg['sMensagem']);
    }
  }
    
  private function ValidarPreenchimento($aCampos) {
      ksort($aCampos);
      
      $aMsg = array();
      
      $REG_DATA = '/[[:digit:]]{2}\/[[:digit:]]{2}\/[[:digit:]]{4}/';
      $REG_DATE = '/([[:digit:]]{4})-([[:digit:]]{2})-([[:digit:]]{2})/';
      $REG_DIGIT_SIGNED = '/[-[:digit:]]+/';
      $REG_DIGIT_UNSIGNED = '/[[:digit:]]+/';
      $REG_PASSWORD = '/[[:alnum:][:punct:]]+/';
      $REG_CARACTERES = '/[[:alpha:]]+/';
      $REG_FLOAT = '/^[[:digit:]]{0,}[.,]{0,1}[[:digit:]]{0,}$/';
      $REG_MOEDA = '/[[:digit:]]+[,]{1}[[:digit:]]{2}/';
      $REG_EMAIL = '/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})/';
      $REG_CEP = '/[0-9]{5}-[0-9]{3}/';
      $REG_TEXTO = '/[[:graph:][:blank:]]+/';
      $REG_TELEFONE_DDD = '/^\([0-9]{2}\)[0-9]{4,5}-[0-9]{4}/';
      $REG_TELEFONE = '/[0-9]{4,5}-[0-9]{4}/';
      $REG_CNPJ = '/^[0-9]{2}.[0-9]{3}.[0-9]{3}\/[0-9]{4}-[0-9]{2}/';

      if (!is_array($aCampos)) {
        return false;
      }

      try {
        
        foreach ($aCampos as $aCampo) {

          $bObrigatorio = (isset($aCampo[3]) && $aCampo[3] === true);

          // Guarda a mensagem customizada de retorno ao usu�rio
          $sMsgCustomizada = '';
          if (isset($aCampo[5])) {
            $sMsgCustomizada = $aCampo[5];
          }

            if ($bObrigatorio && $aCampo[1] == '') {
              $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> deve ser preenchido corretamente!';
              continue;
            }

            if (isset($aCampo[2])) {
              switch ($aCampo[2]) {

                /* Email */
                case 'email':
                  if (!preg_match($REG_EMAIL, $aCampo[1])) {
                    if (!$bObrigatorio && $aCampo[1] == '')
                      break;
                    $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> deve ser um email v�lido!';
  //                  throw new Exception;
                  }
                  break;

                /* Float */
                case 'float':
                  if (!preg_match($REG_FLOAT, $aCampo[1])) {
                    if (!$bObrigatorio && ($aCampo[1] == ''))
                      break;
                    $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> deve ser um n�mero decimal!';
  //                  throw new Exception;
                  } elseif ($bObrigatorio && $aCampo[1] < 0.01) {
                    $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> deve obrigatoriamente ter um valor!';
  //                  throw new Exception;
                  }
                  break;

                /* Faixa de n�meros, ex 0 ~ 100 */
                case 'faixa':
                  $aFaixa = explode('~', $aCampo[4]);
                  if ($aCampo[1] <= $aFaixa[0] || $aCampo[1] > $aFaixa[1] || !preg_match($REG_FLOAT, $aCampo[1])) {
                    $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> deve ser menor que ' . $aFaixa[0] . ' e maior que ' . $aFaixa[1] . '!';
  //                  throw new Exception;
                  }
                  break;

                /* Faixa de n�meros > que X */
                case 'faixa-baixa':
                  if ($aCampo[1] <= $aCampo[4] || !preg_match($REG_FLOAT, $aCampo[1])) {
                    $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> deve ser maior que ' . $aCampo[4] . '!';
  //                  throw new Exception;
                  }
                  break;

                /* Faixa de n�meros < que X */
                case 'faixa-alta':
                  if ($aCampo[1] <= $aCampo[4] || !preg_match($REG_FLOAT, $aCampo[1])) {
                    $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> deve ser menor que ' . $aCampo[4] . '!';
  //                  throw new Exception;
                  }
                  break;


                /*  ********************************
                 *  Data no formato dd/mm/yyyy X 
                 * ********************************* */
                case 'dt-bd':
                case 'dt-db':
                case 'dt-br':
                case 'data':
                  if (!preg_match($REG_DATA, $aCampo[1])) {
                    if (!$bObrigatorio && $aCampo[1] == '')
                      break;
                    $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> deve ser uma data!';
  //                  throw new Exception;
                  }
                  break;

                /* ********************************
                 *  Data no formato yyyy/mm/dd X 
                 * ********************************* */
                case 'date':
                  if (!preg_match($REG_DATE, $aCampo[1])) {
                    if (!$bObrigatorio && $aCampo[1] == '')
                      break;
                    $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> deve ser uma data!';
  //                  throw new Exception;
                  }
                  break;

                /*********************************
                 *  Moeda, reais 
                 * ******************************** */
                case 'moeda':
                  if (!preg_match($REG_MOEDA, $aCampo[1])) {
                    if (!$bObrigatorio && $aCampo[1] == '')
                      break;
                    $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> deve ser um valor em Reais!';
  //                  throw new Exception;
                  }
                  break;

                /*  ********************************
                 *  D�gitos 
                 * ********************************* */
                case 'digito':
                  if (!preg_match($REG_DIGIT_UNSIGNED, $aCampo[1])) {
                    if (!$bObrigatorio && $aCampo[1] == '')
                      break;
                    $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> deve ser um valor num�rico!';
  //                  throw new Exception;
                  }
                  break;

                /*  ********************************
                 *  Caracteres 
                 * ********************************* */
                case 'caracteres':
                  if (!preg_match($REG_CARACTERES, $aCampo[1])) {
                    if (!$bObrigatorio && $aCampo[1] == '')
                      break;
                    $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> deve conter apenas letras!';
  //                  throw new Exception;
                  }
                  break;

                /*  ********************************
                 *  Cep
                 * ********************************* */
                case 'cep':
                  if (!preg_match($REG_CEP, $aCampo[1])) {
                    if (!$bObrigatorio && $aCampo[1] == '')
                      break;
                    $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> deve conter um n�mero de CEP v�lido!';
  //                  throw new Exception;
                  }
                  break;

                /*  ********************************
                 *  Senha
                 * ********************************* */
                case 'senha':
                  if (!preg_match($REG_PASSWORD, $aCampo[1])) {
                    if (!$bObrigatorio && $aCampo[1] == '')
                      break;
                    $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> deve ser preenchido corretamente!';
  //                  throw new Exception;
                  }
                  break;

                /* Validar confirma��o de senha */
                case 'senha-correta':
                  if ($aCampo[1] != $aCampo[4]) {
                    $aMsg[] = 'Voc� est� tentando atualizar a senha, para isto, voc� deve repetir corretamente a senha no campo "<b>' . $aCampo[0] . '</b>"!';
  //                  throw new Exception;
                  }
                  break;

                /*********************************
                 * Date-maior
                 * valores devem chegar no formado yyyy-mm-dd
                 * Validar que uma data seja maior que uma outra data
                 * ********************************* */
                case 'date-maior':
                     $odtDataInicio = new DateTime( $aCampo[1] );
                     $odtDataFinal = new DateTime( $aCampo[4] );
                     $oDadosIntervalo = $odtDataInicio->diff( $odtDataFinal );

                     if ($oDadosIntervalo->invert > 0) {
                       $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> n�o pode ter data inferior � '.$this->parseValue($aCampo[1], 'db-dt').'!';
  //                     throw new Exception;

                     }
                  break;

                /*********************************
                 * Data-maior
                 * Validar que uma data seja maior que uma outra data
                 * valores devem chegar no formado dd/mm/yyyy
                 * ********************************* */
                case 'data-maior':
                     $odtDataInicio = new DateTime( $this->parseValue($aCampo[1], 'db-dt') );
                     $odtDataFinal = new DateTime( $this->parseValue($aCampo[4], 'db-dt') );
                     $oDadosIntervalo = $odtDataInicio->diff( $odtDataFinal );

                     if ($oDadosIntervalo->invert > 0) {
                       $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> n�o pode ter data inferior � '.$aCampo[1].'!';
  //                     throw new Exception;

                     }
                  break;

                case 'senha':
                  if (!preg_match($REG_PASSWORD, $aCampo[1])) {
                    $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> deve ser preenchido corretamente!';
  //                  throw new Exception;
                  }
                  break;

                /* ********************************
                 *  Texto
                 * ********************************* */
                case 'varchar':
                  if (strlen($aCampo[1]) > $aCampo[4][0]) {
                    $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> deve possuir no maximo <b>' . $aCampo[4][0] . '</b> caracteres!';
  //                  throw new Exception;
                  }

                case 'texto':
                  if (!preg_match($REG_TEXTO, $aCampo[1])) {
                    if (!$bObrigatorio && $aCampo[1] == '')
                      break;
                    $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> deve possuir apenas texto!';
  //                  throw new Exception;
                  }
                  break;

                /**********************************
                 *  Text
                 * ********************************* */
                case 'text':
                  $LIMITE_CARACTERES_TEXT = 200000;

//                  if (strlen($aCampo[1]) > $LIMITE_CARACTERES_TEXT) {
//                    $aMsg[] = 'A quantidade m�xima de caracteres do campo <b>' . $aCampo[0] . '</b> n�o deve ser superior a ' . $LIMITE_CARACTERES_TEXT . '!';
//  //                  throw new Exception;
//                  }
                  if (!preg_match($REG_TEXTO, $aCampo[1])) {
                    if (!$bObrigatorio && $aCampo[1] == '')
                      break;
                    $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> deve possuir apenas texto!';
  //                  throw new Exception;
                  }
                  break;

                /*  ********************************
                 *  Telefone
                 * ********************************* */
                case 'telefone':
                  if (!preg_match($REG_TELEFONE, $aCampo[1]) && !preg_match($REG_TELEFONE_DDD, $aCampo[1])) {
                    if (!$bObrigatorio && $aCampo[1] == '')
                      break;
                    $aMsg[] = 'O campo <b>' . $aCampo[0] . '</b> deve ser um n�mero de telefone!';
  //                  throw new Exception;
                  }
                  break;

                /*  ********************************
                 *  Abrang�ncia
                 * ********************************* */
                case 'abrangencia':
                  if (!in_array($aCampo[1], $aCampo[4])) {
                    if (!$bObrigatorio && $aCampo[1] == '')
                      break;
                    $aMsg[] = 'O valor selecionado no campo <b>' . $aCampo[0] . '</b> n�o existe!';
  //                  throw new Exception;
                  }
                  break;

                /*  ********************************
                 *  CNPJ
                 * ********************************* */
                case 'cnpj':
                  if (!preg_match($REG_CNPJ, $aCampo[1])) {
                    $aMsg[] = 'O valor selecionado no campo <b>' . $aCampo[0] . '</b> deve ser um CNPJ!';
  //                  throw new Exception;
                  }
                  break;

                /*  ********************************
                 *  Enum
                 * ********************************* */
                case 'enum':
                  if (!is_array($aCampo[4])) {
                    $aMsg[] = 'Par�metro 4 deve receber um array!';
  //                  throw new Exception;
                  }
                  if (!in_array($aCampo[1], $aCampo[4])) {
                    if (!$bObrigatorio && $aCampo[1] == '')
                      break;
                    $aMsg[] = 'Valor inv�lido para o campo <b>' . $aCampo[0] . '</b>!';
  //                  throw new Exception;
                  }
                  break;

                default:
                  break;
              }
            }
        }
        if (count($aMsg) > 0) {
          throw new Exception;
        }
      } catch (Exception $e) {

        $this->aMsg = array(
            'iCodigoMensagem' => 2,
            'sMensagem' => $sMsgCustomizada == '' ? implode('<br />', $aMsg) : $sMsgCustomizada,
            'sResultado' => '');
        return $aCampo[0];
      }
      return true;
    }
}
