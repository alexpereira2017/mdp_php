<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/excecao/ExceptionValidador.php';
  class ValidadorCfgParametros {

    public function validar ( ModeloCfgParametros $oModelo ) {
      $aValidar = array ( 10 => array('NmParametro'       , $oModelo->NmParametro       , 'varchar', true, array(55)),
                          20 => array('CdParametro'       , $oModelo->CdParametro       , 'varchar', true, array(20)),
                          30 => array('TxExplicativo'     , $oModelo->TxExplicativo     , 'varchar', true, array(255)),
                          40 => array('CdTipoUso'         , $oModelo->CdTipoUso         , 'varchar', true, array(2)),
                          50 => array('CdTipo'            , $oModelo->CdTipo            , 'varchar', true, array(2)),
                          60 => array('NuLimiteCadastro'  , $oModelo->NuLimiteCadastro  , 'int', true, array(2)),
                          70 => array('Status'            , $oModelo->Status            , 'char', true, array(2)),
                          80 => array('NuOrdem'           , $oModelo->NuOrdem           , 'int', true, array(2)),
                          90 => array('NuImportancia'     , $oModelo->NuImportancia     , 'int', true, array(2)),
                          100 => array('TxMascara'         , $oModelo->TxMascara         , 'text', true),
                          110 => array('VlPadrao'          , $oModelo->VlPadrao          , 'varchar', true, array(255)),
                          120 => array('IdUsuario'         , $oModelo->IdUsuario         , 'int', true, array(8)),
                          130 => array('DtCriacao'         , $oModelo->DtCriacao         , 'date', true),
                          140 => array('HrCriacao'         , $oModelo->HrCriacao         , 'time', true),
                        );
      if ($this->oUtil->valida_Preenchimento($aValidar) !== true) {
        $this->aMsg = $this->oUtil->aMsg;
        throw new exception_validacao();
      }
    }
  }