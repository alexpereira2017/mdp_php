<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/


  class ModeloSegUsuarios extends ModeloAdmin { 
    public $Id;
    public $Nome;
    public $Email;
    public $Senha;
    public $DtCadastro;
    public $Status;
    public $CdNivel;
    public $Token;
    public $SenhaProvisoria;
    public $NuTentativasAcesso;
    public $NuAcessos;
    public $IdPessoa;
    public $IdUsuario;
    public $DtCriacao;
    public $HrCriacao;
    public $sConteudo;
    public $aListaModelos;

    public function carregarDadosAposPost($aDados) { 
      $this->Id                   = (isset($aDados['CMPSegUsuariosId']))                   ? $aDados['CMPSegUsuariosId'] : '';
      $this->Nome                 = (isset($aDados['CMPSegUsuariosNome']))                 ? $aDados['CMPSegUsuariosNome'] : '';
      $this->Email                = (isset($aDados['CMPSegUsuariosEmail']))                ? $aDados['CMPSegUsuariosEmail'] : '';
      $this->Senha                = (isset($aDados['CMPSegUsuariosSenha']))                ? $aDados['CMPSegUsuariosSenha'] : '';
      $this->DtCadastro           = (isset($aDados['CMPSegUsuariosDtCadastro']))           ? $aDados['CMPSegUsuariosDtCadastro'] : '';
      $this->Status               = (isset($aDados['CMPSegUsuariosStatus']))               ? $aDados['CMPSegUsuariosStatus'] : '';
      $this->CdNivel              = (isset($aDados['CMPSegUsuariosCdNivel']))              ? $aDados['CMPSegUsuariosCdNivel'] : '';
      $this->Token                = (isset($aDados['CMPSegUsuariosToken']))                ? $aDados['CMPSegUsuariosToken'] : '';
      $this->SenhaProvisoria      = (isset($aDados['CMPSegUsuariosSenhaProvisoria']))      ? $aDados['CMPSegUsuariosSenhaProvisoria'] : '';
      $this->NuTentativasAcesso   = (isset($aDados['CMPSegUsuariosNuTentativasAcesso']))   ? $aDados['CMPSegUsuariosNuTentativasAcesso'] : '';
      $this->NuAcessos            = (isset($aDados['CMPSegUsuariosNuAcessos']))            ? $aDados['CMPSegUsuariosNuAcessos'] : '';
      $this->IdPessoa             = (isset($aDados['CMPSegUsuariosIdPessoa']))             ? $aDados['CMPSegUsuariosIdPessoa'] : '';
      $this->IdUsuario            = (isset($aDados['CMPSegUsuariosIdUsuario']))            ? $aDados['CMPSegUsuariosIdUsuario'] : '';
      $this->DtCriacao            = (isset($aDados['CMPSegUsuariosDtCriacao']))            ? $aDados['CMPSegUsuariosDtCriacao'] : '';
      $this->HrCriacao            = (isset($aDados['CMPSegUsuariosHrCriacao']))            ? $aDados['CMPSegUsuariosHrCriacao'] : '';

    }
  }