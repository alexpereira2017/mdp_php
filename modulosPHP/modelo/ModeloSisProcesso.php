<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/


  class ModeloSisProcesso extends ModeloAdmin { 
    public $Id;
    public $Nome;
    public $Descricao;
    public $DtInicio;
    public $DtEncerramento;
    public $IdUsuario;
    public $DtCriacao;
    public $HrCriacao;
    public $sConteudo;
    public $aListaModelos;

    public function carregarDadosAposPost($aDados) { 
      $this->Id              = (isset($aDados['CMPSisProcessoId']))              ? $aDados['CMPSisProcessoId'] : '';
      $this->Nome            = (isset($aDados['CMPSisProcessoNome']))            ? $aDados['CMPSisProcessoNome'] : '';
      $this->Descricao       = (isset($aDados['CMPSisProcessoDescricao']))       ? $aDados['CMPSisProcessoDescricao'] : '';
      $this->DtInicio        = (isset($aDados['CMPSisProcessoDtInicio']))        ? $aDados['CMPSisProcessoDtInicio'] : '';
      $this->DtEncerramento  = (isset($aDados['CMPSisProcessoDtEncerramento']))  ? $aDados['CMPSisProcessoDtEncerramento'] : '';
      $this->IdUsuario       = (isset($aDados['CMPSisProcessoIdUsuario']))       ? $aDados['CMPSisProcessoIdUsuario'] : '';
      $this->DtCriacao       = (isset($aDados['CMPSisProcessoDtCriacao']))       ? $aDados['CMPSisProcessoDtCriacao'] : '';
      $this->HrCriacao       = (isset($aDados['CMPSisProcessoHrCriacao']))       ? $aDados['CMPSisProcessoHrCriacao'] : '';

    }
  }