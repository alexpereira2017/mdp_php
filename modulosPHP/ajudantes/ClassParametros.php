<?php
/**
 * Busca os parametros configurados no banco de dados
 *
 * @author Alex
 */
class ClassParametros {
  
  private $aParametros;
  
  public function __construct() {
    include_once 'modulosPHP/ajudantes/ClassConexao.php';
    $this->oBd   = new Conexao();
    $this->oUtil = new wTools();
  }
  
  public function getParametros($indice = '') {
    $arr = $this->aParametros;

    if ($indice != '' && isset($this->aParametros[$indice])) {
      $arr = $this->aParametros[$indice];
    }
    return $arr;
  }

  /* Parametros::Buscar
   * 
   * Substitui o m�todo wTools::buscarParametro criado em 19/11/2011
   *
   * Busca valores salvos na tabela de par�metros
   * Exemplo de utiliza��o:
   *  $aParam = $oUtil->buscarParametro(array('MAIL_MASTER'));
   *  echo $aParam['MAIL_MASTER'][0].' - ' .$aParam['MAIL_MASTER'][1]
   *
   * Deve ser usado somente uma vez por p�gina para evitar chamadas desnecess�rias
   * @date 10/06/2018
   * @param  string $mParametros - Array ou String contendo os par�metros solicitados
   * @return array               - Dados montados
   */

  public function Buscar($mParametros) {

    $sParametros = $this->oUtil->montarIN($mParametros);

    $sQuery = "SELECT IFNULL(cfg_parametros_valores.tx_valor,cfg_parametros.vl_padrao) AS TX_VALOR,
                      cfg_parametros.cd_parametro AS CD_PARAMETRO
                 FROM cfg_parametros
           LEFT JOIN cfg_parametros_valores ON cfg_parametros_valores.id_parametro = cfg_parametros.id
                WHERE cfg_parametros.cd_status = 'AT'                
                  AND cfg_parametros.cd_parametro IN (" . $sParametros . ")";

    $aResultado = $this->oBd->query($sQuery);

    if (empty($aResultado)) {
      return false;
    }

    foreach ($aResultado as $i => $aValor) {
      $this->aParametros[$aValor['CD_PARAMETRO']][] = $aValor['TX_VALOR'];
    }

    return $this->aParametros;
  }
}
