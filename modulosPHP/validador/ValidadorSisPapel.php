<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/excecao/ExceptionValidador.php';
  class ValidadorSisPapel {

    public function validar ( ModeloSisPapel $oModelo ) {
      $aValidar = array ( 10 => array('Nome'      , $oModelo->Nome      , 'varchar', true, array(50)),
                          20 => array('IdUsuario' , $oModelo->IdUsuario , 'int', true, array(8)),
                          30 => array('DtCriacao' , $oModelo->DtCriacao , 'date', true),
                          40 => array('HrCriacao' , $oModelo->HrCriacao , 'time', true),
                        );
      if ($this->oUtil->valida_Preenchimento($aValidar) !== true) {
        $this->aMsg = $this->oUtil->aMsg;
        throw new exception_validacao();
      }
    }
  }