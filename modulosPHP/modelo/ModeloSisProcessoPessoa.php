<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/


  class ModeloSisProcessoPessoa extends ModeloAdmin { 
    public $Id;
    public $IdProcesso;
    public $IdPessoa;
    public $IdUsuario;
    public $DtCriacao;
    public $HrCriacao;
    public $sConteudo;
    public $aListaModelos;

    public function carregarDadosAposPost($aDados) { 
      $this->Id          = (isset($aDados['CMPSisProcessoPessoaId']))          ? $aDados['CMPSisProcessoPessoaId'] : '';
      $this->IdProcesso  = (isset($aDados['CMPSisProcessoPessoaIdProcesso']))  ? $aDados['CMPSisProcessoPessoaIdProcesso'] : '';
      $this->IdPessoa    = (isset($aDados['CMPSisProcessoPessoaIdPessoa']))    ? $aDados['CMPSisProcessoPessoaIdPessoa'] : '';
      $this->IdUsuario   = (isset($aDados['CMPSisProcessoPessoaIdUsuario']))   ? $aDados['CMPSisProcessoPessoaIdUsuario'] : '';
      $this->DtCriacao   = (isset($aDados['CMPSisProcessoPessoaDtCriacao']))   ? $aDados['CMPSisProcessoPessoaDtCriacao'] : '';
      $this->HrCriacao   = (isset($aDados['CMPSisProcessoPessoaHrCriacao']))   ? $aDados['CMPSisProcessoPessoaHrCriacao'] : '';

    }
  }