<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/excecao/ExceptionValidador.php';
  class ValidadorSisQuestao {

    public function validar ( ModeloSisQuestao $oModelo ) {
      $aValidar = array ( 10 => array('Pergunta'     , $oModelo->Pergunta     , 'varchar', true, array(400)),
                          20 => array('Influencia'   , $oModelo->Influencia   , 'char', true, array(2)),
                          30 => array('Acesso'       , $oModelo->Acesso       , 'char', true, array(2)),
                          40 => array('PesoPadrao'   , $oModelo->PesoPadrao   , 'int', true, array(2)),
                          50 => array('TipoResposta' , $oModelo->TipoResposta , 'char', true, array(2)),
                          60 => array('Periodicidade', $oModelo->Periodicidade, 'int', true, array(3)),
                          70 => array('IdUsuario'    , $oModelo->IdUsuario    , 'int', true, array(8)),
                          80 => array('DtCriacao'    , $oModelo->DtCriacao    , 'date', true),
                          90 => array('HrCriacao'    , $oModelo->HrCriacao    , 'time', true),
                        );
      if ($this->oUtil->valida_Preenchimento($aValidar) !== true) {
        $this->aMsg = $this->oUtil->aMsg;
        throw new exception_validacao();
      }
    }
  }