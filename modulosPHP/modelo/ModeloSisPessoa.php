<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/


  class ModeloSisPessoa extends ModeloAdmin { 
    public $Id;
    public $Nome;
    public $Cargo;
    public $Email;
    public $IdUsuario;
    public $DtCriacao;
    public $HrCriacao;
    public $sConteudo;
    public $aListaModelos;

    public function carregarDadosAposPost($aDados) { 
      $this->Id         = (isset($aDados['CMPSisPessoaId']))         ? $aDados['CMPSisPessoaId'] : '';
      $this->Nome       = (isset($aDados['CMPSisPessoaNome']))       ? $aDados['CMPSisPessoaNome'] : '';
      $this->Cargo      = (isset($aDados['CMPSisPessoaCargo']))      ? $aDados['CMPSisPessoaCargo'] : '';
      $this->Email      = (isset($aDados['CMPSisPessoaEmail']))      ? $aDados['CMPSisPessoaEmail'] : '';
      $this->IdUsuario  = (isset($aDados['CMPSisPessoaIdUsuario']))  ? $aDados['CMPSisPessoaIdUsuario'] : '';
      $this->DtCriacao  = (isset($aDados['CMPSisPessoaDtCriacao']))  ? $aDados['CMPSisPessoaDtCriacao'] : '';
      $this->HrCriacao  = (isset($aDados['CMPSisPessoaHrCriacao']))  ? $aDados['CMPSisPessoaHrCriacao'] : '';

    }
  }