<?php
include_once 'ClassWebTools.php';
/**
 * Trata sess�o do servidor
 *
 * @author Alex
 */
class ClassSession {

  protected $sessionID;
  protected $loginPage;
  protected $logoutPage;

  public function __construct(){
    $objUtil = new wTools();
    
    $this->loginPage = $objUtil->sUrlBase.'/login';
    $this->logoutPage = $objUtil->sUrlBase.'/logout.php';
    
    if( !isset($_SESSION) ){
      $this->init();
    }
  }

  public function Init() {
    session_start();
  }

  public function Set_session_id(){
    $this->sessionID = session_id();
  }

  public function Get_session_id(){
      return $this->sessionID;
  }

  public function Session_exist( $session_name ){
      if( isset($_SESSION[$session_name]) ){
          return true;
      }
      else{
          return false;
      }
  }


  public function Create_session( $session_name , $is_array = false , $force = false){
      if( !isset($_SESSION[$session_name]) || $force  ){
          if( $is_array == true ){
              $_SESSION[$session_name] = array();
          }
          else{
              $_SESSION[$session_name] = '';
          }
      }
  }

  public function Push( $session_name , array $data ){

    if( is_array($_SESSION[$session_name]) ){
      array_push( $_SESSION[$session_name], $data );
    }
  }

  public function Display_session( $session_name ){
    echo '<pre>';print_r($_SESSION[$session_name]);echo '</pre>';
  }

  public function Remove_session( $session_name = '' ){
    if( !empty($session_name) ){
      unset( $_SESSION[$session_name] );
    } else {
      session_destroy();
    }
  }

  public function Get_session_data( $session_name ){
    return $_SESSION[$session_name];
  }

  public function Set_session_data( $session_name , $data ){
    $_SESSION[$session_name] = $data;
  }

  public function Redirect() {
    header('location: '.$this->loginPage);
  }
}
