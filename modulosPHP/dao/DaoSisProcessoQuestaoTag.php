<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/modelo/ModeloSisProcessoQuestaoTag.php';
  class DaoSisProcessoQuestaoTag {
      
    public    $aMsg = array();
    protected $iCdMsg;
    protected $sMsg;
    protected $sErro;
    public    $sBackpage;
    protected $oUtil;
    protected $oBd;
    protected $oModelo;
    protected $aModelos;

    public function __construct() {
      include_once 'modulosPHP/ajudantes/ClassConexao.php';
      $this->oBd   = new Conexao();
      $this->oUtil = new wTools();
    }
    public function inserir ( ModeloSisProcessoQuestaoTag $oModelo ) {
      $sSql = "INSERT INTO peso_especifico
                           id_tag
                           id_processo_questao
                           id_usuario
                           dt_criacao
                           hr_criacao
                   VALUES ('".$this->oModelo->PesoEspecifico."',
                           '".$this->oModelo->IdTag."',
                           '".$this->oModelo->IdProcessoQuestao."',
                           '".$this->oModelo->IdUsuario."',
                           '".$this->oModelo->DtCriacao."',
                           '".$this->oModelo->HrCriacao."')";
      return $this->executar( $sSql );
    }

    public function editar ( ModeloSisProcessoQuestaoTag $oModelo, $sFiltro ) {
      $sSql = "UPDATE sis_processo_questao_tag
                  SET peso_especifico      = '".$oModelo->PesoEspecifico."',
                      id_tag               = '".$oModelo->IdTag."',
                      id_processo_questao   = '".$oModelo->IdProcessoQuestao."',
                      id_usuario           = '".$oModelo->IdUsuario."',
                      dt_criacao           = '".$oModelo->DtCriacao."',
                      hr_criacao           = '".$oModelo->HrCriacao."'
                ".$sFiltro;
      return $this->executar( $sSql );
    }

    public function remover ( $sFiltro ) {
      $sSql = "DELETE FROM sis_processo_questao_tag". $sFiltro;
      return $this->executar( $sSql );
    }

    public function listar ( $mOpcoes = '') {
      $sFiltro = $this->oUtil->tratarOpcoesDeFiltro($mOpcoes);
      $sSql = 'SELECT id
                        peso_especifico
                        id_tag
                        id_processo_questao
                        id_usuario
                        dt_criacao
                        hr_criacao
                   FROM sis_processo_questao_tag
       '.$sFiltro;

      $mResultado = $this->oBd->query($sSql);

      $this->iLinhas = $this->oBd->getNumeroLinhas();

      if (!is_array($mResultado)) {
        $this->aMsg = $this->oBd->getMsg();
        return false;
      }

      for ($i= 0; $i < $this->iLinhas; $i++) { 
        $oModelo = new ModeloSisProcessoQuestaoTag();
        $oModelo->Id                  = $mResultado[$i]['id'];
        $oModelo->PesoEspecifico      = $mResultado[$i]['peso_especifico'];
        $oModelo->IdTag               = $mResultado[$i]['id_tag'];
        $oModelo->IdProcessoQuestao   = $mResultado[$i]['id_processo_questao'];
        $oModelo->IdUsuario           = $mResultado[$i]['id_usuario'];
        $oModelo->DtCriacao           = $mResultado[$i]['dt_criacao'];
        $oModelo->HrCriacao           = $mResultado[$i]['hr_criacao'];
        $this->aModelo[] = $oModelo;
      }

      return $this->aModelo;
    }

    public function executar ( $sSql ) {
      if (!$this->oBd->execute( $sSql )) {
        $this->iCdMsg = 1;
        $this->sMsg  = 'Ocorreu um erro ao salvar o registro.';
        $bSucesso = false;
      } else {
        $this->iCdMsg = 0;
        $this->sMsg  = 'O registro foi salvo com sucesso!';
        $bSucesso = true;
      }

      $this->aMsg = array('iCdMsg' => $this->iCdMsg,
                            'sMsg' => $this->sMsg);
      return $bSucesso;
    }
  }
