<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/modelo/ModeloAvaliacoes.php';
  class DaoAvaliacoes {
      
    public    $aMsg = array();
    protected $iCdMsg;
    protected $sMsg;
    protected $sErro;
    public    $sBackpage;
    protected $oUtil;
    protected $oBd;
    protected $oModelo;
    protected $aModelos;

    public function __construct() {
      include_once 'modulosPHP/ajudantes/ClassConexao.php';
      $this->oBd   = new Conexao();
      $this->oUtil = new wTools();
    }
    public function inserir ( ModeloAvaliacoes $oModelo ) {
      $sSql = "INSERT INTO avaliador
                           id_avaliador
                           avaliado
                           id_avaliado
                           hash
                           total_questoes
                           respondidas
                           status
                   VALUES ('".$this->oModelo->Avaliador."',
                           '".$this->oModelo->IdAvaliador."',
                           '".$this->oModelo->Avaliado."',
                           '".$this->oModelo->IdAvaliado."',
                           '".$this->oModelo->Hash."',
                           '".$this->oModelo->TotalQuestoes."',
                           '".$this->oModelo->Respondidas."',
                           '".$this->oModelo->Status."')";
      return $this->executar( $sSql );
    }

    public function editar ( ModeloAvaliacoes $oModelo, $sFiltro ) {
      $sSql = "UPDATE avaliacoes
                  SET avaliador      = '".$oModelo->Avaliador."',
                      id_avaliador    = '".$oModelo->IdAvaliador."',
                      avaliado       = '".$oModelo->Avaliado."',
                      id_avaliado     = '".$oModelo->IdAvaliado."',
                      hash           = '".$oModelo->Hash."',
                      total_questoes  = '".$oModelo->TotalQuestoes."',
                      respondidas    = '".$oModelo->Respondidas."',
                      status         = '".$oModelo->Status."'
                ".$sFiltro;
      return $this->executar( $sSql );
    }

    public function remover ( $sFiltro ) {
      $sSql = "DELETE FROM avaliacoes". $sFiltro;
      return $this->executar( $sSql );
    }

    public function listar ( $mOpcoes = '') {
      $sFiltro = $this->oUtil->tratarOpcoesDeFiltro($mOpcoes);
      $sSql = 'SELECT id_processo
                        avaliador
                        id_avaliador
                        avaliado
                        id_avaliado
                        hash
                        total_questoes
                        respondidas
                        status
                   FROM avaliacoes
       '.$sFiltro;

      $mResultado = $this->oBd->query($sSql);

      $this->iLinhas = $this->oBd->getNumeroLinhas();

      if (!is_array($mResultado)) {
        $this->aMsg = $this->oBd->getMsg();
        return false;
      }

      for ($i= 0; $i < $this->iLinhas; $i++) { 
        $oModelo = new ModeloAvaliacoes();
        $oModelo->IdProcesso     = $mResultado[$i]['id_processo'];
        $oModelo->Avaliador      = $mResultado[$i]['avaliador'];
        $oModelo->IdAvaliador    = $mResultado[$i]['id_avaliador'];
        $oModelo->Avaliado       = $mResultado[$i]['avaliado'];
        $oModelo->IdAvaliado     = $mResultado[$i]['id_avaliado'];
        $oModelo->Hash           = $mResultado[$i]['hash'];
        $oModelo->TotalQuestoes  = $mResultado[$i]['total_questoes'];
        $oModelo->Respondidas    = $mResultado[$i]['respondidas'];
        $oModelo->Status         = $mResultado[$i]['status'];
        $this->aModelo[] = $oModelo;
      }

      return $this->aModelo;
    }

    public function executar ( $sSql ) {
      if (!$this->oBd->execute( $sSql )) {
        $this->iCdMsg = 1;
        $this->sMsg  = 'Ocorreu um erro ao salvar o registro.';
        $bSucesso = false;
      } else {
        $this->iCdMsg = 0;
        $this->sMsg  = 'O registro foi salvo com sucesso!';
        $bSucesso = true;
      }

      $this->aMsg = array('iCdMsg' => $this->iCdMsg,
                            'sMsg' => $this->sMsg);
      return $bSucesso;
    }
  }
