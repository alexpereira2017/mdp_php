<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/excecao/ExceptionValidador.php';
  class ValidadorAvaliados {

    public function validar ( ModeloAvaliados $oModelo ) {
      $aValidar = array ( 10 => array('IdAvaliado' , $oModelo->IdAvaliado , 'int', true, array(8)),
                          20 => array('Papel'      , $oModelo->Papel      , 'varchar', true, array(50)),
                          30 => array('Processo'   , $oModelo->Processo   , 'varchar', true, array(255)),
                          40 => array('IdProcesso' , $oModelo->IdProcesso , 'int', true, array(8)),
                        );
      if ($this->oUtil->valida_Preenchimento($aValidar) !== true) {
        $this->aMsg = $this->oUtil->aMsg;
        throw new exception_validacao();
      }
    }
  }