<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once 'ClassDatabase.php';
include_once 'ClassBase64.php';
include_once 'ClassBase64Url.php';
include_once 'ClassSemCodificacao.php';

/**
 * 
 *
 * @author Alex Lunardelli
 */
define('CFG_USUARIO_NIVEL'              , 'aNivelUsuario');
define('CFG_USUARIO_STATUS'             , 'aStatus');
define('CFG_STATUS'                     , 'aStatus');
define('CFG_IMG_CAMINHO'                , 'sCaminhoImagens');
define('CFG_IMG_DIR_VITRINE'            , 'sDirImgVitrine');
define('CFG_IMG_DIR_USUARIO_UPLOAD'     , 'sCaminhoUsuarioUpload');
define('CFG_IMG_DIR_GALERIA'            , 'sCaminhoGaleria');
define('CFG_IMG_VITRINE_UPLOAD'         , 'oConfigUploadVitrine');
define('CFG_IMG_UPLOAD'                 , 'sCaminhoImagensUpload');
define('CFG_HTML_GERAL_SECAO'           , 'sHtmlGeralSecao');
define('CFG_HTML_GERAL_LOCAL_ESTILOS'   , 'sHtmlGeralLocalEstilos');
define('CFG_CAMPOS_FORM_TIPO'           , 'aCamposFormTipo');
define('CFG_NOME_MES'                   , 'arrMeses');



class Config {
  
  public $sPgAtual;
  public $aMsg;
  public $aParametros;
  private $oDadosConexaoBanco;
  private $sEnderecoServidor;
  private $sEnderecoRelativo;
  private $aStatus;
  private $sHtmlGeralSecao;
  private $aNivelUsuario;
  private $sCaminhoImagens;
  private $sCaminhoGaleria;
  private $sCaminhoUsuarioUpload;
  private $sHtmlGeralLocalEstilos;
  private $sDirImgVitrine;
  private $sCaminhoImagensUpload;
  private $oConfigUploadVitrine;
  private $aCamposFormTipo;
  private $arrMeses;
  private $arrStatus;
  private $sUrlHomeAdmin;
  
  public static $SALT = 'baitaca2018';
  

  public function __construct($sPgAtual = '0') {
    $this->aMsg    = array('iCdMsg' => '', 'sMsg' => '', 'sMsgErro' => '');
    $this->sPgAtual = $sPgAtual;
    $this->definirParametros();
    $this->ConfiguracoesAmbiente();
  }
  
  public static function buscarAmbiente() {
      $aRet = explode('/', $_SERVER['PHP_SELF']);

      $aRet[1] = ($_SERVER['SERVER_NAME'] == 'localhost') ? $aRet[1] : 'producao';
   
      switch ($aRet[1]) {
	case 'mim':
	  $sAmbiente = 'mim_v1';
	  break;
        default:
	  $sAmbiente = 'producao';
	  break;
      }
      return $sAmbiente;
  }

  /* config::incluirCss
   *
   * @date 23/02/2015
   * @param  
   * @return bool
   */
  public function incluirCss() {?>
    <link rel="stylesheet" type="text/css" href="comum/estilos.css" />
    <link rel="stylesheet" type="text/css" href="comum/sprites.css" />
    <link href='http://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
  <?php  
  }

  /* config::incluirJs
   *
   * @date 23/02/2015
   * @param  
   * @return bool
   */
  public function incluirJs() { ?>
    <script src="modulosJS/jquery-2.1.3.min.js"	   type="text/javascript"></script>
    <script src="modulosJS/slider/js/jssor.slider.mini.js"></script>
    <?php
    return true;
  }



  /* config::buscar
   *
   * Cria um resumo de um texto passado como par�metro
   * @date 23/02/2015
   * @param  string $sParam - Par�metro a ser usado
   * @return mixed	    - false caso a configura��o n�o exista, sen�o, o valor
   *			      do campo.
   */
  public function Buscar($sParam) {
    return isset($this->$sParam) ? $this->$sParam : false;
  }


  private function DefinirParametros() {

//    include_once 'class.param_imagem_upload.php';
//    $oImagensVitrine = new param_imagem_upload();

    $teste = false;
    $var = 'secao=noticias';
    
    $this->oCodificador = new Base64Url();
    echo ($teste) ? $this->oCodificador->Codificar($var)."<br />" : '';
    
    $this->oCodificador = new Base64();
    echo ($teste) ? $this->oCodificador->Codificar($var)."<br />" : '';
    
    $this->oCodificador = new SemCodificacao();
    echo ($teste) ? $this->oCodificador->Codificar($var)."<br />" : '';

    $this->aStatus = array('AT' => 'Ativo', 
                           'IN' => 'Inativo');
    $this->sHtmlGeralSecao = array('geral' => 'Geral');

    $this->aNivelUsuario = array( 5  => 'Administrador',
                                         7  => 'Comiss�o T�cnica',
                                         10 => 'Atleta');

    $this->sCaminhoImagens        = 'comum/imagens/';
    $this->sCaminhoGaleria        = 'comum/imagens/galeria/imagem-usuario/';
    $this->sCaminhoUsuarioUpload  = 'comum/imagens/galeria/imagem-usuario/upload/';
    $this->sHtmlGeralLocalEstilos = 'comum/estilos-html-geral/';
    $this->sDirImgVitrine         = 'vitrine';
    $this->sCaminhoImagensUpload  = 'comum/imagens/galeria/imagem-usuario/upload/';
//    $this->oConfigUploadVitrine   = $oImagensVitrine;

    $this->aCamposFormTipo = array( 'M' => 'moeda',
                                    'A' => 'text',
                                    'H' => 'text',
                                    'I' => 'digito',
                                    'S' => 'text',
                                    'D' => 'dt-br',
                                    'O' => 'text',
                                    'M' => 'text',
                                    'H' => 'hora',
                                    'L' => 'bool',
                                    'T' => 'text',
                                    'P' => 'senha');
    
    $this->arrMeses = array( 1 => 'Janeiro',  
                             2 => 'Fevereiro',
                             3 => 'Mar�o',
                             4 => 'Abril',
                             5 => 'Maio',
                             6 => 'Junho',
                             7 => 'Julho',
                             8 => 'Agosto',
                             9 => 'Setembro',
                             10 => 'Outubro',
                             11 => 'Novembro',
                             12 => 'Dezembro',
              );
    
    $this->arrStatus = array('A' => 'Ativo',
                            'I' => 'Inativo');
    
    
    $this->aParametros = array(
      
	// Par�metros espec�ficos por p�ginas 
       '0' => array('titulo'   => 'Par�metros de configura��es n�o definidos',
                    'backPage' => 'index.php' ),
       'index' => array('titulo'   => 'Bravo FC',
                        'backPage' => 'index.php' ),
     'contato' => array('titulo'   => 'Bravo FC',
                        'backPage' => 'contato.php' ),
        'lancamento-contratos' => array('titulo'   => 'Lan�ar Contratos',
	 			        'backPage' => 'lancar-contratos.php' ),
        // Usu�rios
              'admin-usuarios' => array('titulo'   => 'Usu�rios',
				        'backPage' => 'admin-usuarios.php' ),
       'admin-usuarios-editar' => array('titulo'   => 'Inserir Usu�rio',
				        'backPage' => 'admin-usuarios.php' ),
       'admin-usuarios-editar' => array('titulo'   => 'Editar Usu�rio',
				        'backPage' => 'admin-usuarios.php' ),
        
        // Vitrine
              'admin-vitrine' => array('titulo'   => 'Vitrine',
				        'backPage' => 'admin-vitrine.php' ),
       'admin-vitrine-editar' => array('titulo'   => 'Inserir Vitrine',
				        'backPage' => 'admin-vitrine.php' ),
       'admin-vitrine-editar' => array('titulo'   => 'Editar vitrine',
				        'backPage' => 'admin-vitrine.php' ),
        
        
    );
  }
  
  public function buscarTituloHtml() {
    if (isset($this->aParametros[$this->sPgAtual])) {
      return $this->aParametros[$this->sPgAtual]['titulo'];
    }
    return $this->aParametros[0]['titulo'];
  }

  public function ConfiguracoesAmbiente() {
    $aDados['mim_v1']    = array('sServidor' => 'localhost',
                                  'sUsuario' => 'root',
                                    'sSenha' => 'mylunacom',
                                    'sBanco' => 'mim_v1',
                                       'sBd' => 'mysqli',
                         'sEnderecoServidor' => 'http://localhost/mim/trunk',
                         'sEnderecoRelativo' => '',
        );
     $aDados['producao'] = array('sServidor' => 'localhost',
                                  'sUsuario' => 'lunac207_movi',
                                    'sSenha' => '+4WbI2-=$Jck',
                                    'sBanco' => 'lunac207_mim',
                                       'sBd' => 'mysqli',
                         'sEnderecoServidor' => 'http://mim.lunacom.com.br',
                         'sEnderecoRelativo' => '',
         );

    include_once 'ClassDatabase.php';
    $this->oDadosConexaoBanco = new Database($aDados[self::buscarAmbiente()]);

    $this->sEnderecoServidor = $aDados[self::buscarAmbiente()]['sEnderecoServidor'];
    $this->sEnderecoRelativo = $aDados[self::buscarAmbiente()]['sEnderecoRelativo'];
    
    $this->sUrlHomeAdmin = $this->sEnderecoServidor.'/painel/?secao=noticias';

  }


  public function getInfoBd() {
    return $this->oDadosConexaoBanco;
  }
}
