<?php
include_once 'modulosPHP/validador/ValidatorFormularioProcessoResumo.php';
class controle_ProcessoResumo extends Modelo {
  
  private $oNegocio;
  private $oModelo;
  private $oVisao;
  private $oValidador;


  public function __construct() {
    
    $this->oModelo = new ModeloProcessoResumo();
    $this->oNegocio = new NegocioProcessoResumo();
    
    if (isset($_POST)) {
      if (isset($_POST['sAcao'])) {
        if ($_POST['sAcao'] == 'salvar') {

          try {
            $this->oValidador = new ValidadorProcessoResumo();
            $this->oModelo->carregarDadosAposPost($_POST);
            $this->oValidador->validar($this->oModelo);

            $this->oNegocio->salvar($this->oModelo);
            
            $this->oModelo = new ModeloProcessoResumo();
            $this->oModelo->mAcaoResultado = 0;
            $this->oModelo->sAcaoMsg       = 'Novo cadastro foi realizado com sucesso!';
            
          } catch (exception_validacao $ex) {
            $this->oModelo->mAcaoResultado = 2;
            $this->oModelo->sAcaoMsg       = $this->oValidador->aMsg['sMsg'];
          }          
        }
      }
    }

    $this->oVisao  = new VisaoProcessoResumo($this->oModelo);
  }
  
  public function getVisao() {
    return $this->oVisao;
  }
}

