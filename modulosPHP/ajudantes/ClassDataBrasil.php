<?php
/**
 * Calsse para trabalhar com formata��o de datas
 *
 * @author Alex
 */
class ClassDataBrasil {

  /* wTools::nomeMes
   *
   * Traz o nome do m�s em Portugues
   * @date 31/10/2010
   * @param  integer $iMes    - Numero do mes a ser testado
   * @return string $sNomeMes - Nome do mes em portugues
   */
  public function nomeMes($iMes) {

    switch ($iMes) {
      case 01: $sNomeMes = 'Janeiro';
        break;
      case 02: $sNomeMes = 'Fevereiro';
        break;
      case 03: $sNomeMes = 'Mar&ccedil;o';
        break;
      case 04: $sNomeMes = 'Abril';
        break;
      case 05: $sNomeMes = 'Maio';
        break;
      case 06: $sNomeMes = 'Junho';
        break;
      case 07: $sNomeMes = 'Julho';
        break;
      case 08: $sNomeMes = 'Agosto';
        break;
      case 09: $sNomeMes = 'Setembro';
        break;
      case 10: $sNomeMes = 'Outubro';
        break;
      case 11: $sNomeMes = 'Novembro';
        break;
      case 12: $sNomeMes = 'Dezembro';
        break;
      default: $sNomeMes = '';
    }
    return $sNomeMes;
  }

  /* transformarDataDoBancoDeDadosParaFormatoBrasil
   *
   * 
   * @date 04/06/2018
   * @param  string $sData - No formato 2018-12-01
   * @return string data no formato BR
   */
  public function transformarDataDoBancoDeDadosParaFormatoBrasil() {
    //@TODO
  }

  /* transformarDataDoBancoDeDadosParaFormatoBrasil
   *
   * 
   * @date 04/06/2018
   * @param  string $sData - No formato 2018-12-01
   * @return string Data por extenso por ex 01 de Novembro de 2016
   */
  public function transformarDataDoBancoDeDadosParaFormatoBrasilPorExtenso($sData) {
    $arr = explode('-', $sData);

    return $arr[2] .' de '.$this->nomeMes($arr[1]).' de '.$arr[0];
  }
}
