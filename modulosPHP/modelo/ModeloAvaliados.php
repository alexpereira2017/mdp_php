<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/


  class ModeloAvaliados extends ModeloAdmin { 
    public $Pessoa;
    public $IdAvaliado;
    public $Papel;
    public $Processo;
    public $IdProcesso;
    public $sConteudo;
    public $aListaModelos;

    public function carregarDadosAposPost($aDados) { 
      $this->Pessoa      = (isset($aDados['CMPAvaliadosPessoa']))      ? $aDados['CMPAvaliadosPessoa'] : '';
      $this->IdAvaliado  = (isset($aDados['CMPAvaliadosIdAvaliado']))  ? $aDados['CMPAvaliadosIdAvaliado'] : '';
      $this->Papel       = (isset($aDados['CMPAvaliadosPapel']))       ? $aDados['CMPAvaliadosPapel'] : '';
      $this->Processo    = (isset($aDados['CMPAvaliadosProcesso']))    ? $aDados['CMPAvaliadosProcesso'] : '';
      $this->IdProcesso  = (isset($aDados['CMPAvaliadosIdProcesso']))  ? $aDados['CMPAvaliadosIdProcesso'] : '';

    }
  }