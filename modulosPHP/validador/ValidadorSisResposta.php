<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/excecao/ExceptionValidador.php';
  class ValidadorSisResposta {

    public function validar ( ModeloSisResposta $oModelo ) {
      $aValidar = array ( 10 => array('IdProcesso'  , $oModelo->IdProcesso  , 'int', true, array(8)),
                          20 => array('IdAvaliador' , $oModelo->IdAvaliador , 'int', true, array(8)),
                          30 => array('IdAvaliado'  , $oModelo->IdAvaliado  , 'int', true, array(8)),
                          40 => array('IdQuestao'   , $oModelo->IdQuestao   , 'int', true, array(8)),
                          50 => array('IdOpcao'     , $oModelo->IdOpcao     , 'int', true, array(8)),
                          60 => array('Descricao'   , $oModelo->Descricao   , 'text', true),
                          70 => array('IdUsuario'   , $oModelo->IdUsuario   , 'int', true, array(8)),
                          80 => array('DtCriacao'   , $oModelo->DtCriacao   , 'date', true),
                          90 => array('HrCriacao'   , $oModelo->HrCriacao   , 'time', true),
                        );
      if ($this->oUtil->valida_Preenchimento($aValidar) !== true) {
        $this->aMsg = $this->oUtil->aMsg;
        throw new exception_validacao();
      }
    }
  }