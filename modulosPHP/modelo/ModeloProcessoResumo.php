<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/


  class ModeloProcessoResumo extends ModeloAdmin { 
    public $IdProcesso;
    public $NrQuestoes;
    public $Nome;
    public $DtInicio;
    public $DtEncerramento;
    public $Status;
    public $sConteudo;
    public $aListaModelos;

    public function carregarDadosAposPost($aDados) { 
      $this->IdProcesso      = (isset($aDados['CMPProcessoResumoIdProcesso']))      ? $aDados['CMPProcessoResumoIdProcesso'] : '';
      $this->NrQuestoes      = (isset($aDados['CMPProcessoResumoNrQuestoes']))      ? $aDados['CMPProcessoResumoNrQuestoes'] : '';
      $this->Nome            = (isset($aDados['CMPProcessoResumoNome']))            ? $aDados['CMPProcessoResumoNome'] : '';
      $this->DtInicio        = (isset($aDados['CMPProcessoResumoDtInicio']))        ? $aDados['CMPProcessoResumoDtInicio'] : '';
      $this->DtEncerramento  = (isset($aDados['CMPProcessoResumoDtEncerramento']))  ? $aDados['CMPProcessoResumoDtEncerramento'] : '';
      $this->Status          = (isset($aDados['CMPProcessoResumoStatus']))          ? $aDados['CMPProcessoResumoStatus'] : '';

    }
  }