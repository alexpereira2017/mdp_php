<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/excecao/ExceptionValidador.php';
  class ValidadorSisPessoa {

    public function validar ( ModeloSisPessoa $oModelo ) {
      $aValidar = array ( 10 => array('Nome'      , $oModelo->Nome      , 'varchar', true, array(120)),
                          20 => array('Cargo'     , $oModelo->Cargo     , 'varchar', true, array(80)),
                          30 => array('Email'     , $oModelo->Email     , 'varchar', true, array(80)),
                          40 => array('IdUsuario' , $oModelo->IdUsuario , 'int', true, array(8)),
                          50 => array('DtCriacao' , $oModelo->DtCriacao , 'date', true),
                          60 => array('HrCriacao' , $oModelo->HrCriacao , 'time', true),
                        );
      if ($this->oUtil->valida_Preenchimento($aValidar) !== true) {
        $this->aMsg = $this->oUtil->aMsg;
        throw new exception_validacao();
      }
    }
  }