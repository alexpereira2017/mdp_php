<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/modelo/ModeloProcessoResumo.php';
  class DaoProcessoResumo {
      
    public    $aMsg = array();
    protected $iCdMsg;
    protected $sMsg;
    protected $sErro;
    public    $sBackpage;
    protected $oUtil;
    protected $oBd;
    protected $oModelo;
    protected $aModelos;

    public function __construct() {
      include_once 'modulosPHP/ajudantes/ClassConexao.php';
      $this->oBd   = new Conexao();
      $this->oUtil = new wTools();
    }
    public function inserir ( ModeloProcessoResumo $oModelo ) {
      $sSql = "INSERT INTO nr_questoes
                           nome
                           dt_inicio
                           dt_encerramento
                           status
                   VALUES ('".$this->oModelo->NrQuestoes."',
                           '".$this->oModelo->Nome."',
                           '".$this->oModelo->DtInicio."',
                           '".$this->oModelo->DtEncerramento."',
                           '".$this->oModelo->Status."')";
      return $this->executar( $sSql );
    }

    public function editar ( ModeloProcessoResumo $oModelo, $sFiltro ) {
      $sSql = "UPDATE processo_resumo
                  SET nr_questoes      = '".$oModelo->NrQuestoes."',
                      nome            = '".$oModelo->Nome."',
                      dt_inicio        = '".$oModelo->DtInicio."',
                      dt_encerramento  = '".$oModelo->DtEncerramento."',
                      status          = '".$oModelo->Status."'
                ".$sFiltro;
      return $this->executar( $sSql );
    }

    public function remover ( $sFiltro ) {
      $sSql = "DELETE FROM processo_resumo". $sFiltro;
      return $this->executar( $sSql );
    }

    public function listar ( $mOpcoes = '') {
      $sFiltro = $this->oUtil->tratarOpcoesDeFiltro($mOpcoes);
      $sSql = 'SELECT id_processo
                        nr_questoes
                        nome
                        dt_inicio
                        dt_encerramento
                        status
                   FROM processo_resumo
       '.$sFiltro;

      $mResultado = $this->oBd->query($sSql);

      $this->iLinhas = $this->oBd->getNumeroLinhas();

      if (!is_array($mResultado)) {
        $this->aMsg = $this->oBd->getMsg();
        return false;
      }

      for ($i= 0; $i < $this->iLinhas; $i++) { 
        $oModelo = new ModeloProcessoResumo();
        $oModelo->IdProcesso      = $mResultado[$i]['id_processo'];
        $oModelo->NrQuestoes      = $mResultado[$i]['nr_questoes'];
        $oModelo->Nome            = $mResultado[$i]['nome'];
        $oModelo->DtInicio        = $mResultado[$i]['dt_inicio'];
        $oModelo->DtEncerramento  = $mResultado[$i]['dt_encerramento'];
        $oModelo->Status          = $mResultado[$i]['status'];
        $this->aModelo[] = $oModelo;
      }

      return $this->aModelo;
    }

    public function executar ( $sSql ) {
      if (!$this->oBd->execute( $sSql )) {
        $this->iCdMsg = 1;
        $this->sMsg  = 'Ocorreu um erro ao salvar o registro.';
        $bSucesso = false;
      } else {
        $this->iCdMsg = 0;
        $this->sMsg  = 'O registro foi salvo com sucesso!';
        $bSucesso = true;
      }

      $this->aMsg = array('iCdMsg' => $this->iCdMsg,
                            'sMsg' => $this->sMsg);
      return $bSucesso;
    }
  }
