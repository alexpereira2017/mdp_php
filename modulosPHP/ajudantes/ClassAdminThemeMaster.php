<?php
  class AdminThemeMaster 
  {
    private $sUrlBase;
    public function __construct($sUrlBase = '') {
      $this->sUrlBase = $sUrlBase;
    }

    public function Botao($sLabel) {
      return '<button class="btn">'.$sLabel.'</button>';
    }


    public function InputText(ModeloHtml $modelo) { ?>
      <div class="control-group">
        <label class="control-label" for="<?php echo $modelo->sId; ?>"><?php echo $modelo->sRotulo; ?></label>
        <div class="controls">
          <input id="<?php echo $modelo->sId; ?>" name="<?php echo $this->DefinirNomeDoCampo($modelo); ?>" class="input-xlarge focused" type="text" value="<?php echo $modelo->mValor; ?>">
        </div>
      </div> <?php
    }

    public function InputPassword(ModeloHtml $modelo) { ?>
      <div class="control-group">
        <label class="control-label" for="<?php echo $modelo->sId; ?>"><?php echo $modelo->sRotulo; ?></label>
        <div class="controls">
          <input id="<?php echo $modelo->sId; ?>" name="<?php echo $this->DefinirNomeDoCampo($modelo); ?>" class="input-xlarge focused" type="password" value="<?php echo $modelo->mValor; ?>">
        </div>
      </div> <?php
    }
    
    public function SalvarCancelar(ModeloHtml $modelo) { ?>
      <div class="form-actions">
        <button type="submit" class="btn btn-primary"><?php echo $modelo->sBotaoSalvar; ?></button>
        <button type="reset" class="btn"><?php echo $modelo->sBotaoCancelar; ?></button>
      </div>
      <?php 
    }
    
    public function MultiSelect(ModeloHtml $modelo) { ?>
      <div class="control-group">
        <label class="control-label" for="<?php echo $modelo->sId; ?>"><?php echo $modelo->sRotulo; ?></label>
        <div class="controls">
          <select multiple="multiple" id="<?php echo $modelo->sId; ?>" name="<?php echo $this->DefinirNomeDoCampo($modelo); ?>" class="chzn-select span4">
            <?php
              foreach ($modelo->aIndiceRotulo as $key => $value) { ?>
              <option value="<?php echo $key; ?>" <?php echo in_array($key, $modelo->mValor) ? 'selected="selected"' : ''; ?> ><?php echo $value; ?></option>
              <?php
              }
            ?>
          </select>
          <!--<p class="help-block">Start typing to activate auto complete!</p>-->
        </div>
      </div> <?php
    }
    
    public function Select(ModeloHtml $modelo) {
      ?>
      <div class="control-group">
        <label class="control-label" for="<?php echo $modelo->sId; ?>"><?php echo $modelo->sRotulo; ?></label>
        <div class="controls">
          <select id="<?php echo $modelo->sId; ?>" name="<?php echo $this->DefinirNomeDoCampo($modelo); ?>"class="chzn-select">
            <?php
              foreach ($modelo->aIndiceRotulo as $key => $value) { ?>
                <option value="<?php echo $key; ?>" <?php echo ($key == $modelo->mValor) ? 'selected="selected"' : ''; ?> ><?php echo $value; ?></option>
              <?php
              }
            ?>
          </select>
        </div>
      </div><?php
    }


    public function checkbox(ModeloHtml $modelo) { ?>
      <div class="control-group">
        <label class="control-label" for="<?php echo $modelo->sId; ?>"><?php echo $modelo->sRotulo; ?></label>
        <div class="controls">
          <label class="uniform">
            <?php
              foreach ($modelo->aObjCheckbox as $key => $objCheckboxOption) { 
                $strMarcado = ($objCheckboxOption->mixValorHtml == $objCheckboxOption->mixValorSalvo) ? 'checked="checked"' : '';
                ?>
                <input name="<?php echo $this->DefinirNomeDoCampo($modelo); ?>" class="uniform_on" type="checkbox" id="<?php echo $objCheckboxOption->mixValorHtml.$key; ?>" value="<?php echo $objCheckboxOption->mixValorHtml; ?>" <?php echo $strMarcado; ?>> <?php echo $objCheckboxOption->strRotulo;
              }
            ?>
          </label>
        </div>
      </div><?php
      
    }
    
    public function TextAreaCkEditor(ModeloHtml $modelo) { ?>
      <div class="control-group">
        <label class="control-label" for="<?php echo $modelo->sId; ?>"><?php echo $modelo->sRotulo; ?></label>
        <div class="controls">
          <textarea id="<?php echo $this->DefinirNomeDoCampo($modelo); ?>" name="<?php echo $this->DefinirNomeDoCampo($modelo); ?>" class="input-xlarge textarea" placeholder="<?php echo $modelo->sPlaceHolder; ?>" style="width: 810px; height: 200px"><?php echo $modelo->mValor; ?></textarea>
          <script>
            CKEDITOR.replace('<?php echo $this->DefinirNomeDoCampo($modelo); ?>', {
              UserFilesPath : '<?php echo $this->sUrlBase; ?>'
            });
          </script>
        </div>
      </div>
        <?php
    }
    
    public function TextArea(ModeloHtml $modelo, $strWidth = '800px', $strHeight = '200px') { ?>
      <div class="control-group">
        <label class="control-label" for="<?php echo $modelo->sId; ?>"><?php echo $modelo->sRotulo; ?></label>
        <div class="controls">
          <textarea id="<?php echo $this->DefinirNomeDoCampo($modelo); ?>" name="<?php echo $this->DefinirNomeDoCampo($modelo); ?>" class="input-xlarge textarea" placeholder="<?php echo $modelo->sPlaceHolder; ?>" style="width: <?php echo $strWidth; ?>; height: <?php echo $strHeight; ?>"><?php echo $modelo->mValor; ?></textarea>
        </div>
      </div>        
        <?php
    }
    
    public function FileInputOnlyImage(ModeloHtml $modelo) { ?>
      <div class="control-group">
        <label class="control-label" for="<?php echo $modelo->sId; ?>"><?php echo $modelo->sRotulo; ?></label>
        <div class="controls">
          <input class="input-file uniform_on" id="<?php echo $this->DefinirNomeDoCampo($modelo); ?>" name="<?php echo $this->DefinirNomeDoCampo($modelo); ?>" type="file" accept="image/*" />
        </div>
      </div>
      <?php
    }

    public function FileInput(ModeloHtml $modelo) { ?>
      <div class="control-group">
        <label class="control-label" for="<?php echo $modelo->sId; ?>"><?php echo $modelo->sRotulo; ?></label>
        <div class="controls">
          <input class="input-file uniform_on" id="<?php echo $this->DefinirNomeDoCampo($modelo); ?>" name="<?php echo $this->DefinirNomeDoCampo($modelo); ?>" type="file" />
        </div>
      </div>
      <?php
    }

    public function Alert($modelo) {
      switch ($modelo->mAcaoResultado) {
        case '0': $this->AlertSucess($modelo);break;
        case '1': $this->AlertWarning($modelo);break;
        case '2': $this->AlertError($modelo);break;
        case '3': $this->AlertInfo($modelo);break;
        case '': break;
      }
    }

    public function AlertSucess($modelo) {?>
      <div class="alert alert-success">
        <button class="close" data-dismiss="alert">&times;</button>
        <strong>Successo!</strong><br /> <?php echo $modelo->sAcaoMsg; ?>
      </div>
      <?php      
    }
    
    public function AlertWarning($modelo) {?>
      <div class="alert">
        <button class="close" data-dismiss="alert">&times;</button>
        <strong>Aten��o!</strong><br /> <?php echo $modelo->sAcaoMsg; ?>
      </div><?php
    }
    
    public function AlertInfo($modelo) {?>
      <div class="alert alert-info">
        <button class="close" data-dismiss="alert">&times;</button>
        <strong>Informa��o!</strong> <?php echo $modelo->sAcaoMsg; ?>
      </div>
      <?php
    }
    
    public function AlertError($modelo) {?>
      <div class="alert alert-error">
        <button class="close" data-dismiss="alert">&times;</button>
        <strong>Erro!</strong><br /> <?php echo $modelo->sAcaoMsg; ?>
      </div>
      <?php      
    }
    
    public function DefinirNomeDoCampo($modelo) {
      return $modelo->sNome != '' ? $modelo->sNome : $modelo->sId;
    }
  }
?>


