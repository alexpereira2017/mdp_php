<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/


  class ModeloSisResposta extends ModeloAdmin { 
    public $Id;
    public $IdProcesso;
    public $IdAvaliador;
    public $IdAvaliado;
    public $IdQuestao;
    public $IdOpcao;
    public $Descricao;
    public $IdUsuario;
    public $DtCriacao;
    public $HrCriacao;
    public $sConteudo;
    public $aListaModelos;

    public function carregarDadosAposPost($aDados) { 
      $this->Id           = (isset($aDados['CMPSisRespostaId']))           ? $aDados['CMPSisRespostaId'] : '';
      $this->IdProcesso   = (isset($aDados['CMPSisRespostaIdProcesso']))   ? $aDados['CMPSisRespostaIdProcesso'] : '';
      $this->IdAvaliador  = (isset($aDados['CMPSisRespostaIdAvaliador']))  ? $aDados['CMPSisRespostaIdAvaliador'] : '';
      $this->IdAvaliado   = (isset($aDados['CMPSisRespostaIdAvaliado']))   ? $aDados['CMPSisRespostaIdAvaliado'] : '';
      $this->IdQuestao    = (isset($aDados['CMPSisRespostaIdQuestao']))    ? $aDados['CMPSisRespostaIdQuestao'] : '';
      $this->IdOpcao      = (isset($aDados['CMPSisRespostaIdOpcao']))      ? $aDados['CMPSisRespostaIdOpcao'] : '';
      $this->Descricao    = (isset($aDados['CMPSisRespostaDescricao']))    ? $aDados['CMPSisRespostaDescricao'] : '';
      $this->IdUsuario    = (isset($aDados['CMPSisRespostaIdUsuario']))    ? $aDados['CMPSisRespostaIdUsuario'] : '';
      $this->DtCriacao    = (isset($aDados['CMPSisRespostaDtCriacao']))    ? $aDados['CMPSisRespostaDtCriacao'] : '';
      $this->HrCriacao    = (isset($aDados['CMPSisRespostaHrCriacao']))    ? $aDados['CMPSisRespostaHrCriacao'] : '';

    }
  }