<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/excecao/ExceptionValidador.php';
  class ValidadorSisQuestaoOpcoes {

    public function validar ( ModeloSisQuestaoOpcoes $oModelo ) {
      $aValidar = array ( 10 => array('Descricao' , $oModelo->Descricao , 'varchar', true, array(400)),
                          20 => array('Valor'     , $oModelo->Valor     , 'varchar', true, array(400)),
                          30 => array('Ordem'     , $oModelo->Ordem     , 'int', true, array(2)),
                          40 => array('IdQuestao' , $oModelo->IdQuestao , 'int', true, array(8)),
                          50 => array('IdUsuario' , $oModelo->IdUsuario , 'int', true, array(8)),
                          60 => array('DtCriacao' , $oModelo->DtCriacao , 'date', true),
                          70 => array('HrCriacao' , $oModelo->HrCriacao , 'time', true),
                        );
      if ($this->oUtil->valida_Preenchimento($aValidar) !== true) {
        $this->aMsg = $this->oUtil->aMsg;
        throw new exception_validacao();
      }
    }
  }