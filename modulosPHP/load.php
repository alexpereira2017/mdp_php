<?php
  include_once 'modulosPHP/ajudantes/ClassWebTools.php';
  include_once 'modulosPHP/ajudantes/ClassConfig.php';
//  include_once 'modulosPHP/view/view.geral.php';
    include_once 'modulosPHP/controle/ControleFactory.php';

$oUtil = new wTools();
$sPgAtual = $oUtil->buscarNomePaginaAtual();
  function __autoload($sPgAtual) {
    
    $sPgAtual = ucfirst($sPgAtual);
    $sPgAtual = str_replace('Visao', '', $sPgAtual);
    $sPgAtual = str_replace('Modelo', '', $sPgAtual);
    $sPgAtual = str_replace('Negocio', '', $sPgAtual);
    $sPgAtual = str_replace('Controle', '', $sPgAtual);
    $sPgAtual = str_replace('Dao', '', $sPgAtual);

    if ((file_exists('modulosPHP/visao/Visao'.$sPgAtual.'.php'))) {
      require_once 'modulosPHP/visao/Visao'.$sPgAtual.'.php';
    }

    if ((file_exists('modulosPHP/modelo/Modelo'.$sPgAtual.'.php'))) {
      require_once 'modulosPHP/modelo/ModeloPrincipal.php';
      require_once 'modulosPHP/modelo/Modelo'.$sPgAtual.'.php';
    }

    if ((file_exists('modulosPHP/negocio/Negocio'.$sPgAtual.'.php'))) {
      require_once 'modulosPHP/negocio/Negocio'.$sPgAtual.'.php';
    }

    if ((file_exists('modulosPHP/controle/Controle'.$sPgAtual.'.php'))) {
      require_once 'modulosPHP/controle/Controle'.$sPgAtual.'.php';
    }

    if ((file_exists('modulosPHP/dao/Dao'.$sPgAtual.'.php'))) {
      require_once 'modulosPHP/dao/Dao'.$sPgAtual.'.php';
    }
    
  }
