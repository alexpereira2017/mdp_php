<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/


  class ModeloCfgParametrosValores extends ModeloAdmin { 
    public $Id;
    public $IdParametro;
    public $TxValor;
    public $TxFunc;
    public $Status;
    public $IdUsuario;
    public $DtCriacao;
    public $HrCriacao;
    public $sConteudo;
    public $aListaModelos;

    public function carregarDadosAposPost($aDados) { 
      $this->Id           = (isset($aDados['CMPCfgParametrosValoresId']))           ? $aDados['CMPCfgParametrosValoresId'] : '';
      $this->IdParametro  = (isset($aDados['CMPCfgParametrosValoresIdParametro']))  ? $aDados['CMPCfgParametrosValoresIdParametro'] : '';
      $this->TxValor      = (isset($aDados['CMPCfgParametrosValoresTxValor']))      ? $aDados['CMPCfgParametrosValoresTxValor'] : '';
      $this->TxFunc       = (isset($aDados['CMPCfgParametrosValoresTxFunc']))       ? $aDados['CMPCfgParametrosValoresTxFunc'] : '';
      $this->Status       = (isset($aDados['CMPCfgParametrosValoresStatus']))       ? $aDados['CMPCfgParametrosValoresStatus'] : '';
      $this->IdUsuario    = (isset($aDados['CMPCfgParametrosValoresIdUsuario']))    ? $aDados['CMPCfgParametrosValoresIdUsuario'] : '';
      $this->DtCriacao    = (isset($aDados['CMPCfgParametrosValoresDtCriacao']))    ? $aDados['CMPCfgParametrosValoresDtCriacao'] : '';
      $this->HrCriacao    = (isset($aDados['CMPCfgParametrosValoresHrCriacao']))    ? $aDados['CMPCfgParametrosValoresHrCriacao'] : '';

    }
  }