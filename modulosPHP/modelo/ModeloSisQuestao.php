<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/


  class ModeloSisQuestao extends ModeloAdmin { 
    public $Id;
    public $Pergunta;
    public $Influencia;
    public $Acesso;
    public $PesoPadrao;
    public $TipoResposta;
    public $Periodicidade;
    public $IdUsuario;
    public $DtCriacao;
    public $HrCriacao;
    public $sConteudo;
    public $aListaModelos;

    public function carregarDadosAposPost($aDados) { 
      $this->Id            = (isset($aDados['CMPSisQuestaoId']))            ? $aDados['CMPSisQuestaoId'] : '';
      $this->Pergunta      = (isset($aDados['CMPSisQuestaoPergunta']))      ? $aDados['CMPSisQuestaoPergunta'] : '';
      $this->Influencia    = (isset($aDados['CMPSisQuestaoInfluencia']))    ? $aDados['CMPSisQuestaoInfluencia'] : '';
      $this->Acesso        = (isset($aDados['CMPSisQuestaoAcesso']))        ? $aDados['CMPSisQuestaoAcesso'] : '';
      $this->PesoPadrao    = (isset($aDados['CMPSisQuestaoPesoPadrao']))    ? $aDados['CMPSisQuestaoPesoPadrao'] : '';
      $this->TipoResposta  = (isset($aDados['CMPSisQuestaoTipoResposta']))  ? $aDados['CMPSisQuestaoTipoResposta'] : '';
      $this->Periodicidade = (isset($aDados['CMPSisQuestaoPeriodicidade'])) ? $aDados['CMPSisQuestaoPeriodicidade'] : '';
      $this->IdUsuario     = (isset($aDados['CMPSisQuestaoIdUsuario']))     ? $aDados['CMPSisQuestaoIdUsuario'] : '';
      $this->DtCriacao     = (isset($aDados['CMPSisQuestaoDtCriacao']))     ? $aDados['CMPSisQuestaoDtCriacao'] : '';
      $this->HrCriacao     = (isset($aDados['CMPSisQuestaoHrCriacao']))     ? $aDados['CMPSisQuestaoHrCriacao'] : '';

    }
  }