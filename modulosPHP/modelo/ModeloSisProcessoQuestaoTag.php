<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/


  class ModeloSisProcessoQuestaoTag extends ModeloAdmin { 
    public $Id;
    public $PesoEspecifico;
    public $IdTag;
    public $IdProcessoQuestao;
    public $IdUsuario;
    public $DtCriacao;
    public $HrCriacao;
    public $sConteudo;
    public $aListaModelos;

    public function carregarDadosAposPost($aDados) { 
      $this->Id                  = (isset($aDados['CMPSisProcessoQuestaoTagId']))                  ? $aDados['CMPSisProcessoQuestaoTagId'] : '';
      $this->PesoEspecifico      = (isset($aDados['CMPSisProcessoQuestaoTagPesoEspecifico']))      ? $aDados['CMPSisProcessoQuestaoTagPesoEspecifico'] : '';
      $this->IdTag               = (isset($aDados['CMPSisProcessoQuestaoTagIdTag']))               ? $aDados['CMPSisProcessoQuestaoTagIdTag'] : '';
      $this->IdProcessoQuestao   = (isset($aDados['CMPSisProcessoQuestaoTagIdProcessoQuestao']))   ? $aDados['CMPSisProcessoQuestaoTagIdProcessoQuestao'] : '';
      $this->IdUsuario           = (isset($aDados['CMPSisProcessoQuestaoTagIdUsuario']))           ? $aDados['CMPSisProcessoQuestaoTagIdUsuario'] : '';
      $this->DtCriacao           = (isset($aDados['CMPSisProcessoQuestaoTagDtCriacao']))           ? $aDados['CMPSisProcessoQuestaoTagDtCriacao'] : '';
      $this->HrCriacao           = (isset($aDados['CMPSisProcessoQuestaoTagHrCriacao']))           ? $aDados['CMPSisProcessoQuestaoTagHrCriacao'] : '';

    }
  }