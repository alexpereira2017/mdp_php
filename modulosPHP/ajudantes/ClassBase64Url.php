<?php

include_once 'interfaceDecodificadores.php';
class Base64Url {

  public function Codificar($string) {
    return urlencode(base64_encode($string));
  }
  public function Decodificar($string) {
    return base64_decode(urldecode($string));
  }

}
