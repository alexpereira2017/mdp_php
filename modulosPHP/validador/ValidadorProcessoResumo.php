<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/excecao/ExceptionValidador.php';
  class ValidadorProcessoResumo {

    public function validar ( ModeloProcessoResumo $oModelo ) {
      $aValidar = array ( 10 => array('NrQuestoes'     , $oModelo->NrQuestoes     , 'bigint', true, array(21)),
                          20 => array('Nome'           , $oModelo->Nome           , 'varchar', true, array(255)),
                          30 => array('DtInicio'       , $oModelo->DtInicio       , 'date', true),
                          40 => array('DtEncerramento' , $oModelo->DtEncerramento , 'date', true),
                          50 => array('Status'         , $oModelo->Status         , 'varchar', true, array(11)),
                        );
      if ($this->oUtil->valida_Preenchimento($aValidar) !== true) {
        $this->aMsg = $this->oUtil->aMsg;
        throw new exception_validacao();
      }
    }
  }