<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/modelo/ModeloSisProcessoQuestao.php';
  class DaoSisProcessoQuestao {
      
    public    $aMsg = array();
    protected $iCdMsg;
    protected $sMsg;
    protected $sErro;
    public    $sBackpage;
    protected $oUtil;
    protected $oBd;
    protected $oModelo;
    protected $aModelos;

    public function __construct() {
      include_once 'modulosPHP/ajudantes/ClassConexao.php';
      $this->oBd   = new Conexao();
      $this->oUtil = new wTools();
    }
    public function inserir ( ModeloSisProcessoQuestao $oModelo ) {
      $sSql = "INSERT INTO peso_especifico
                           ordem
                           id_processo
                           id_questao
                           id_usuario
                           dt_criacao
                           hr_criacao
                   VALUES ('".$this->oModelo->PesoEspecifico."',
                           '".$this->oModelo->Ordem."',
                           '".$this->oModelo->IdProcesso."',
                           '".$this->oModelo->IdQuestao."',
                           '".$this->oModelo->IdUsuario."',
                           '".$this->oModelo->DtCriacao."',
                           '".$this->oModelo->HrCriacao."')";
      return $this->executar( $sSql );
    }

    public function editar ( ModeloSisProcessoQuestao $oModelo, $sFiltro ) {
      $sSql = "UPDATE sis_processo_questao
                  SET peso_especifico  = '".$oModelo->PesoEspecifico."',
                      ordem           = '".$oModelo->Ordem."',
                      id_processo      = '".$oModelo->IdProcesso."',
                      id_questao       = '".$oModelo->IdQuestao."',
                      id_usuario       = '".$oModelo->IdUsuario."',
                      dt_criacao       = '".$oModelo->DtCriacao."',
                      hr_criacao       = '".$oModelo->HrCriacao."'
                ".$sFiltro;
      return $this->executar( $sSql );
    }

    public function remover ( $sFiltro ) {
      $sSql = "DELETE FROM sis_processo_questao". $sFiltro;
      return $this->executar( $sSql );
    }

    public function listar ( $mOpcoes = '') {
      $sFiltro = $this->oUtil->tratarOpcoesDeFiltro($mOpcoes);
      $sSql = 'SELECT id
                        peso_especifico
                        ordem
                        id_processo
                        id_questao
                        id_usuario
                        dt_criacao
                        hr_criacao
                   FROM sis_processo_questao
       '.$sFiltro;

      $mResultado = $this->oBd->query($sSql);

      $this->iLinhas = $this->oBd->getNumeroLinhas();

      if (!is_array($mResultado)) {
        $this->aMsg = $this->oBd->getMsg();
        return false;
      }

      for ($i= 0; $i < $this->iLinhas; $i++) { 
        $oModelo = new ModeloSisProcessoQuestao();
        $oModelo->Id              = $mResultado[$i]['id'];
        $oModelo->PesoEspecifico  = $mResultado[$i]['peso_especifico'];
        $oModelo->Ordem           = $mResultado[$i]['ordem'];
        $oModelo->IdProcesso      = $mResultado[$i]['id_processo'];
        $oModelo->IdQuestao       = $mResultado[$i]['id_questao'];
        $oModelo->IdUsuario       = $mResultado[$i]['id_usuario'];
        $oModelo->DtCriacao       = $mResultado[$i]['dt_criacao'];
        $oModelo->HrCriacao       = $mResultado[$i]['hr_criacao'];
        $this->aModelo[] = $oModelo;
      }

      return $this->aModelo;
    }

    public function executar ( $sSql ) {
      if (!$this->oBd->execute( $sSql )) {
        $this->iCdMsg = 1;
        $this->sMsg  = 'Ocorreu um erro ao salvar o registro.';
        $bSucesso = false;
      } else {
        $this->iCdMsg = 0;
        $this->sMsg  = 'O registro foi salvo com sucesso!';
        $bSucesso = true;
      }

      $this->aMsg = array('iCdMsg' => $this->iCdMsg,
                            'sMsg' => $this->sMsg);
      return $bSucesso;
    }
  }
