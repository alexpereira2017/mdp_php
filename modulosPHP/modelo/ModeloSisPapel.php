<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/


  class ModeloSisPapel extends ModeloAdmin { 
    public $Id;
    public $Nome;
    public $IdUsuario;
    public $DtCriacao;
    public $HrCriacao;
    public $sConteudo;
    public $aListaModelos;

    public function carregarDadosAposPost($aDados) { 
      $this->Id         = (isset($aDados['CMPSisPapelId']))         ? $aDados['CMPSisPapelId'] : '';
      $this->Nome       = (isset($aDados['CMPSisPapelNome']))       ? $aDados['CMPSisPapelNome'] : '';
      $this->IdUsuario  = (isset($aDados['CMPSisPapelIdUsuario']))  ? $aDados['CMPSisPapelIdUsuario'] : '';
      $this->DtCriacao  = (isset($aDados['CMPSisPapelDtCriacao']))  ? $aDados['CMPSisPapelDtCriacao'] : '';
      $this->HrCriacao  = (isset($aDados['CMPSisPapelHrCriacao']))  ? $aDados['CMPSisPapelHrCriacao'] : '';

    }
  }