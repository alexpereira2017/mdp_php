<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/modelo/ModeloSegUsuarios.php';
  class DaoSegUsuarios {
      
    public    $aMsg = array();
    protected $iCdMsg;
    protected $sMsg;
    protected $sErro;
    public    $sBackpage;
    protected $oUtil;
    protected $oBd;
    protected $oModelo;
    protected $aModelos;

    public function __construct() {
      include_once 'modulosPHP/ajudantes/ClassConexao.php';
      $this->oBd   = new Conexao();
      $this->oUtil = new wTools();
    }
    public function inserir ( ModeloSegUsuarios $oModelo ) {
      $sSql = "INSERT INTO nome
                           email
                           senha
                           dt_cadastro
                           status
                           cd_nivel
                           token
                           senha_provisoria
                           nu_tentativas_acesso
                           nu_acessos
                           id_pessoa
                           id_usuario
                           dt_criacao
                           hr_criacao
                   VALUES ('".$this->oModelo->Nome."',
                           '".$this->oModelo->Email."',
                           '".$this->oModelo->Senha."',
                           '".$this->oModelo->DtCadastro."',
                           '".$this->oModelo->Status."',
                           '".$this->oModelo->CdNivel."',
                           '".$this->oModelo->Token."',
                           '".$this->oModelo->SenhaProvisoria."',
                           '".$this->oModelo->NuTentativasAcesso."',
                           '".$this->oModelo->NuAcessos."',
                           '".$this->oModelo->IdPessoa."',
                           '".$this->oModelo->IdUsuario."',
                           '".$this->oModelo->DtCriacao."',
                           '".$this->oModelo->HrCriacao."')";
      return $this->executar( $sSql );
    }

    public function editar ( ModeloSegUsuarios $oModelo, $sFiltro ) {
      $sSql = "UPDATE seg_usuarios
                  SET nome                 = '".$oModelo->Nome."',
                      email                = '".$oModelo->Email."',
                      senha                = '".$oModelo->Senha."',
                      dt_cadastro           = '".$oModelo->DtCadastro."',
                      status               = '".$oModelo->Status."',
                      cd_nivel              = '".$oModelo->CdNivel."',
                      token                = '".$oModelo->Token."',
                      senha_provisoria      = '".$oModelo->SenhaProvisoria."',
                      nu_tentativas_acesso   = '".$oModelo->NuTentativasAcesso."',
                      nu_acessos            = '".$oModelo->NuAcessos."',
                      id_pessoa             = '".$oModelo->IdPessoa."',
                      id_usuario            = '".$oModelo->IdUsuario."',
                      dt_criacao            = '".$oModelo->DtCriacao."',
                      hr_criacao            = '".$oModelo->HrCriacao."'
                ".$sFiltro;
      return $this->executar( $sSql );
    }

    public function remover ( $sFiltro ) {
      $sSql = "DELETE FROM seg_usuarios". $sFiltro;
      return $this->executar( $sSql );
    }

    public function listar ( $mOpcoes = '') {
      $sFiltro = $this->oUtil->tratarOpcoesDeFiltro($mOpcoes);
      $sSql = 'SELECT id
                        nome
                        email
                        senha
                        dt_cadastro
                        status
                        cd_nivel
                        token
                        senha_provisoria
                        nu_tentativas_acesso
                        nu_acessos
                        id_pessoa
                        id_usuario
                        dt_criacao
                        hr_criacao
                   FROM seg_usuarios
       '.$sFiltro;

      $mResultado = $this->oBd->query($sSql);

      $this->iLinhas = $this->oBd->getNumeroLinhas();

      if (!is_array($mResultado)) {
        $this->aMsg = $this->oBd->getMsg();
        return false;
      }

      for ($i= 0; $i < $this->iLinhas; $i++) { 
        $oModelo = new ModeloSegUsuarios();
        $oModelo->Id                   = $mResultado[$i]['id'];
        $oModelo->Nome                 = $mResultado[$i]['nome'];
        $oModelo->Email                = $mResultado[$i]['email'];
        $oModelo->Senha                = $mResultado[$i]['senha'];
        $oModelo->DtCadastro           = $mResultado[$i]['dt_cadastro'];
        $oModelo->Status               = $mResultado[$i]['status'];
        $oModelo->CdNivel              = $mResultado[$i]['cd_nivel'];
        $oModelo->Token                = $mResultado[$i]['token'];
        $oModelo->SenhaProvisoria      = $mResultado[$i]['senha_provisoria'];
        $oModelo->NuTentativasAcesso   = $mResultado[$i]['nu_tentativas_acesso'];
        $oModelo->NuAcessos            = $mResultado[$i]['nu_acessos'];
        $oModelo->IdPessoa             = $mResultado[$i]['id_pessoa'];
        $oModelo->IdUsuario            = $mResultado[$i]['id_usuario'];
        $oModelo->DtCriacao            = $mResultado[$i]['dt_criacao'];
        $oModelo->HrCriacao            = $mResultado[$i]['hr_criacao'];
        $this->aModelo[] = $oModelo;
      }

      return $this->aModelo;
    }

    public function executar ( $sSql ) {
      if (!$this->oBd->execute( $sSql )) {
        $this->iCdMsg = 1;
        $this->sMsg  = 'Ocorreu um erro ao salvar o registro.';
        $bSucesso = false;
      } else {
        $this->iCdMsg = 0;
        $this->sMsg  = 'O registro foi salvo com sucesso!';
        $bSucesso = true;
      }

      $this->aMsg = array('iCdMsg' => $this->iCdMsg,
                            'sMsg' => $this->sMsg);
      return $bSucesso;
    }
  }
