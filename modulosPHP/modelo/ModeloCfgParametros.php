<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/


  class ModeloCfgParametros extends ModeloAdmin { 
    public $Id;
    public $NmParametro;
    public $CdParametro;
    public $TxExplicativo;
    public $CdTipoUso;
    public $CdTipo;
    public $NuLimiteCadastro;
    public $Status;
    public $NuOrdem;
    public $NuImportancia;
    public $TxMascara;
    public $VlPadrao;
    public $IdUsuario;
    public $DtCriacao;
    public $HrCriacao;
    public $sConteudo;
    public $aListaModelos;

    public function carregarDadosAposPost($aDados) { 
      $this->Id                 = (isset($aDados['CMPCfgParametrosId']))                 ? $aDados['CMPCfgParametrosId'] : '';
      $this->NmParametro        = (isset($aDados['CMPCfgParametrosNmParametro']))        ? $aDados['CMPCfgParametrosNmParametro'] : '';
      $this->CdParametro        = (isset($aDados['CMPCfgParametrosCdParametro']))        ? $aDados['CMPCfgParametrosCdParametro'] : '';
      $this->TxExplicativo      = (isset($aDados['CMPCfgParametrosTxExplicativo']))      ? $aDados['CMPCfgParametrosTxExplicativo'] : '';
      $this->CdTipoUso          = (isset($aDados['CMPCfgParametrosCdTipoUso']))          ? $aDados['CMPCfgParametrosCdTipoUso'] : '';
      $this->CdTipo             = (isset($aDados['CMPCfgParametrosCdTipo']))             ? $aDados['CMPCfgParametrosCdTipo'] : '';
      $this->NuLimiteCadastro   = (isset($aDados['CMPCfgParametrosNuLimiteCadastro']))   ? $aDados['CMPCfgParametrosNuLimiteCadastro'] : '';
      $this->Status             = (isset($aDados['CMPCfgParametrosStatus']))             ? $aDados['CMPCfgParametrosStatus'] : '';
      $this->NuOrdem            = (isset($aDados['CMPCfgParametrosNuOrdem']))            ? $aDados['CMPCfgParametrosNuOrdem'] : '';
      $this->NuImportancia      = (isset($aDados['CMPCfgParametrosNuImportancia']))      ? $aDados['CMPCfgParametrosNuImportancia'] : '';
      $this->TxMascara          = (isset($aDados['CMPCfgParametrosTxMascara']))          ? $aDados['CMPCfgParametrosTxMascara'] : '';
      $this->VlPadrao           = (isset($aDados['CMPCfgParametrosVlPadrao']))           ? $aDados['CMPCfgParametrosVlPadrao'] : '';
      $this->IdUsuario          = (isset($aDados['CMPCfgParametrosIdUsuario']))          ? $aDados['CMPCfgParametrosIdUsuario'] : '';
      $this->DtCriacao          = (isset($aDados['CMPCfgParametrosDtCriacao']))          ? $aDados['CMPCfgParametrosDtCriacao'] : '';
      $this->HrCriacao          = (isset($aDados['CMPCfgParametrosHrCriacao']))          ? $aDados['CMPCfgParametrosHrCriacao'] : '';

    }
  }