<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/


  class ModeloAvaliacoes extends ModeloAdmin { 
    public $IdProcesso;
    public $Avaliador;
    public $IdAvaliador;
    public $Avaliado;
    public $IdAvaliado;
    public $Hash;
    public $TotalQuestoes;
    public $Respondidas;
    public $Status;
    public $sConteudo;
    public $aListaModelos;

    public function carregarDadosAposPost($aDados) { 
      $this->IdProcesso     = (isset($aDados['CMPAvaliacoesIdProcesso']))     ? $aDados['CMPAvaliacoesIdProcesso'] : '';
      $this->Avaliador      = (isset($aDados['CMPAvaliacoesAvaliador']))      ? $aDados['CMPAvaliacoesAvaliador'] : '';
      $this->IdAvaliador    = (isset($aDados['CMPAvaliacoesIdAvaliador']))    ? $aDados['CMPAvaliacoesIdAvaliador'] : '';
      $this->Avaliado       = (isset($aDados['CMPAvaliacoesAvaliado']))       ? $aDados['CMPAvaliacoesAvaliado'] : '';
      $this->IdAvaliado     = (isset($aDados['CMPAvaliacoesIdAvaliado']))     ? $aDados['CMPAvaliacoesIdAvaliado'] : '';
      $this->Hash           = (isset($aDados['CMPAvaliacoesHash']))           ? $aDados['CMPAvaliacoesHash'] : '';
      $this->TotalQuestoes  = (isset($aDados['CMPAvaliacoesTotalQuestoes']))  ? $aDados['CMPAvaliacoesTotalQuestoes'] : '';
      $this->Respondidas    = (isset($aDados['CMPAvaliacoesRespondidas']))    ? $aDados['CMPAvaliacoesRespondidas'] : '';
      $this->Status         = (isset($aDados['CMPAvaliacoesStatus']))         ? $aDados['CMPAvaliacoesStatus'] : '';

    }
  }