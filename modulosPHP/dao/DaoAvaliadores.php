<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/modelo/ModeloAvaliadores.php';
  class DaoAvaliadores {
      
    public    $aMsg = array();
    protected $iCdMsg;
    protected $sMsg;
    protected $sErro;
    public    $sBackpage;
    protected $oUtil;
    protected $oBd;
    protected $oModelo;
    protected $aModelos;

    public function __construct() {
      include_once 'modulosPHP/ajudantes/ClassConexao.php';
      $this->oBd   = new Conexao();
      $this->oUtil = new wTools();
    }
    public function inserir ( ModeloAvaliadores $oModelo ) {
      $sSql = "INSERT INTO id_avaliador
                           papel
                           processo
                           id_processo
                           hash
                   VALUES ('".$this->oModelo->IdAvaliador."',
                           '".$this->oModelo->Papel."',
                           '".$this->oModelo->Processo."',
                           '".$this->oModelo->IdProcesso."',
                           '".$this->oModelo->Hash."')";
      return $this->executar( $sSql );
    }

    public function editar ( ModeloAvaliadores $oModelo, $sFiltro ) {
      $sSql = "UPDATE avaliadores
                  SET id_avaliador  = '".$oModelo->IdAvaliador."',
                      papel        = '".$oModelo->Papel."',
                      processo     = '".$oModelo->Processo."',
                      id_processo   = '".$oModelo->IdProcesso."',
                      hash         = '".$oModelo->Hash."'
                ".$sFiltro;
      return $this->executar( $sSql );
    }

    public function remover ( $sFiltro ) {
      $sSql = "DELETE FROM avaliadores". $sFiltro;
      return $this->executar( $sSql );
    }

    public function listar ( $mOpcoes = '') {
      $sFiltro = $this->oUtil->tratarOpcoesDeFiltro($mOpcoes);
      $sSql = 'SELECT avaliador
                        id_avaliador
                        papel
                        processo
                        id_processo
                        hash
                   FROM avaliadores
       '.$sFiltro;

      $mResultado = $this->oBd->query($sSql);

      $this->iLinhas = $this->oBd->getNumeroLinhas();

      if (!is_array($mResultado)) {
        $this->aMsg = $this->oBd->getMsg();
        return false;
      }

      for ($i= 0; $i < $this->iLinhas; $i++) { 
        $oModelo = new ModeloAvaliadores();
        $oModelo->Avaliador    = $mResultado[$i]['avaliador'];
        $oModelo->IdAvaliador  = $mResultado[$i]['id_avaliador'];
        $oModelo->Papel        = $mResultado[$i]['papel'];
        $oModelo->Processo     = $mResultado[$i]['processo'];
        $oModelo->IdProcesso   = $mResultado[$i]['id_processo'];
        $oModelo->Hash         = $mResultado[$i]['hash'];
        $this->aModelo[] = $oModelo;
      }

      return $this->aModelo;
    }

    public function executar ( $sSql ) {
      if (!$this->oBd->execute( $sSql )) {
        $this->iCdMsg = 1;
        $this->sMsg  = 'Ocorreu um erro ao salvar o registro.';
        $bSucesso = false;
      } else {
        $this->iCdMsg = 0;
        $this->sMsg  = 'O registro foi salvo com sucesso!';
        $bSucesso = true;
      }

      $this->aMsg = array('iCdMsg' => $this->iCdMsg,
                            'sMsg' => $this->sMsg);
      return $bSucesso;
    }
  }
