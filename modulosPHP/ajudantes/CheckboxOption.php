<?php
/**
 * Objeto com as propriedades necessárias para montar um checkbox HTML
 *
 * @author Alex
 */
class CheckboxOption {
  
  public $strId = '';
  public $mixValorHtml = '';
  public $mixValorSalvo = '';
  public $strRotulo = '';

}
