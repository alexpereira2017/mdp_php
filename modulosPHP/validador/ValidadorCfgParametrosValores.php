<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/excecao/ExceptionValidador.php';
  class ValidadorCfgParametrosValores {

    public function validar ( ModeloCfgParametrosValores $oModelo ) {
      $aValidar = array ( 10 => array('IdParametro' , $oModelo->IdParametro , 'int', true, array(8)),
                          20 => array('TxValor'     , $oModelo->TxValor     , 'varchar', true, array(255)),
                          30 => array('TxFunc'      , $oModelo->TxFunc      , 'text', true),
                          40 => array('Status'      , $oModelo->Status      , 'varchar', true, array(2)),
                          50 => array('IdUsuario'   , $oModelo->IdUsuario   , 'int', true, array(8)),
                          60 => array('DtCriacao'   , $oModelo->DtCriacao   , 'date', true),
                          70 => array('HrCriacao'   , $oModelo->HrCriacao   , 'time', true),
                        );
      if ($this->oUtil->valida_Preenchimento($aValidar) !== true) {
        $this->aMsg = $this->oUtil->aMsg;
        throw new exception_validacao();
      }
    }
  }