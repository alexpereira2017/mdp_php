<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/excecao/ExceptionValidador.php';
  class ValidadorAvaliacoes {

    public function validar ( ModeloAvaliacoes $oModelo ) {
      $aValidar = array ( 10 => array('Avaliador'     , $oModelo->Avaliador     , 'varchar', true, array(120)),
                          20 => array('IdAvaliador'   , $oModelo->IdAvaliador   , 'int', true, array(8)),
                          30 => array('Avaliado'      , $oModelo->Avaliado      , 'varchar', true, array(120)),
                          40 => array('IdAvaliado'    , $oModelo->IdAvaliado    , 'int', true, array(8)),
                          50 => array('Hash'          , $oModelo->Hash          , 'varchar', true, array(32)),
                          60 => array('TotalQuestoes' , $oModelo->TotalQuestoes , 'bigint', true, array(21)),
                          70 => array('Respondidas'   , $oModelo->Respondidas   , 'bigint', true, array(21)),
                          80 => array('Status'        , $oModelo->Status        , 'varchar', true, array(12)),
                        );
      if ($this->oUtil->valida_Preenchimento($aValidar) !== true) {
        $this->aMsg = $this->oUtil->aMsg;
        throw new exception_validacao();
      }
    }
  }