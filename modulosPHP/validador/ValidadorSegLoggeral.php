<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/excecao/ExceptionValidador.php';
  class ValidadorSegLoggeral {

    public function validar ( ModeloSegLoggeral $oModelo ) {
      $aValidar = array ( 10 => array('Nome'      , $oModelo->Nome      , 'varchar', true, array(255)),
                          20 => array('Descricao' , $oModelo->Descricao , 'text', true),
                          30 => array('Codigo'    , $oModelo->Codigo    , 'varchar', true, array(40)),
                          40 => array('Acao'      , $oModelo->Acao      , 'varchar', true, array(2)),
                          50 => array('Ip'        , $oModelo->Ip        , 'varchar', true, array(30)),
                          60 => array('Trace'     , $oModelo->Trace     , 'text', true),
                          70 => array('IdUsuario' , $oModelo->IdUsuario , 'int', true, array(8)),
                          80 => array('DtCriacao' , $oModelo->DtCriacao , 'date', true),
                          90 => array('HrCriacao' , $oModelo->HrCriacao , 'time', true),
                        );
      if ($this->oUtil->valida_Preenchimento($aValidar) !== true) {
        $this->aMsg = $this->oUtil->aMsg;
        throw new exception_validacao();
      }
    }
  }