<?php


class exception_validacao extends Exception {
  public $iCodigoMensagem;
  public $sMensagem;
  
  public function __construct($iCodigoMensagem, $sMensagem) {
    $this->iCodigoMensagem = $iCodigoMensagem;
    $this->sMensagem = $sMensagem;
  }

}
