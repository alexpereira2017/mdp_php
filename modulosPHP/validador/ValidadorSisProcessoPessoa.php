<?php
/************************************************** 
* @package    
* @author     Alex <alex@lunacom.com.br>
* @date       24-09-2018
* @version    Arquitetura 2017
 **************************************************/

  include_once 'modulosPHP/excecao/ExceptionValidador.php';
  class ValidadorSisProcessoPessoa {

    public function validar ( ModeloSisProcessoPessoa $oModelo ) {
      $aValidar = array ( 10 => array('IdProcesso' , $oModelo->IdProcesso , 'int', true, array(8)),
                          20 => array('IdPessoa'   , $oModelo->IdPessoa   , 'int', true, array(8)),
                          30 => array('IdUsuario'  , $oModelo->IdUsuario  , 'int', true, array(8)),
                          40 => array('DtCriacao'  , $oModelo->DtCriacao  , 'date', true),
                          50 => array('HrCriacao'  , $oModelo->HrCriacao  , 'time', true),
                        );
      if ($this->oUtil->valida_Preenchimento($aValidar) !== true) {
        $this->aMsg = $this->oUtil->aMsg;
        throw new exception_validacao();
      }
    }
  }