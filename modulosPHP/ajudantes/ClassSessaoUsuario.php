<?php
include_once 'ClassBase64.php';

class SessaoUsuario {
  private $objSession;
  private $session_name = 'login_mim3';
  private $objBase64;
  private $objConfig;

  public function __construct( ClassSession $objSession) {
    $this->objSession = $objSession;
    $this->objBase64 = new Base64();
    $this->objConfig = new Config();
  }
  
  public function GetNomeUsuario () {
    return isset($_SESSION[$this->session_name][0]['nome']) ? $_SESSION[$this->session_name][0]['nome'] : false;
  }
  
  public function GetToken () {
    return isset($_SESSION[$this->session_name][0]['token']) ? $_SESSION[$this->session_name][0]['token'] : false;
  }

  public function ValidarSessao (DaoSegUsuarios $oDaoUsuario) {

    $strFiltro = "WHERE token = '".$this->GetToken()."'";
    $oDaoUsuario->listar($strFiltro);

    $bRet = false;
    
    if ($oDaoUsuario->iLinhas == 1) {
      $bRet = true;
    }

    return $bRet;
  }

  public function ValidarLogin(DaoSegUsuarios $oDaoUsuario, ModeloSegUsuarios $oModelo) {
    $sFiltro = "WHERE email = '".$oModelo->Email."'";
    $sFiltro .= " AND senha = md5('".Config::$SALT.$oModelo->Senha."')";
    $oUsuario = $oDaoUsuario->listar($sFiltro);
    

    if ($oDaoUsuario->iLinhas != 1) {
//      $this->objSession->Remove_session();
      throw new exception_validacao(1, 'Email ou senha n�o est�o corretos');
    }

    $this->CriarSessaoUsuario($oDaoUsuario);
  }
  
  private function CriarSessaoUsuario(DaoSegUsuarios $oDaoUsuario) {
    
    $oUsuario = $oDaoUsuario->GetResultSet(0);
    
    $strInfo = $oUsuario->Id;
    $strInfo .= '**'.$oUsuario->Nome;
    $strInfo .= '**'.date("Y-m-d H:i:s");

    $strToken = $this->objBase64->Codificar($strInfo);
    $oUsuario->Token = $strToken;
    
    $strFiltro = "WHERE id = ".$oUsuario->Id;
    $oDaoUsuario->editarToken($oUsuario, $strFiltro);

    $this->objSession->Create_session($this->session_name, true, true);
    $data = array('token' => $strToken,
                   'nome' => $oUsuario->Nome);
    $this->objSession->Push($this->session_name, $data);

    $sUrl = $this->objConfig->Buscar('sUrlHomeAdmin');
    header('location: '.$sUrl);
  }
  
}
