<?php
/**
 * Encarregada de conectar e fazer consultas ao BD
 * Esta classe utiliza ADO DB
 *
 * @author Alex Lunardelli 
 * @since  23/09/2014
 * 
 */

include 'modulosPHP/bibliotecas/adodb5/adodb.inc.php';
include 'modulosPHP/bibliotecas/adodb5/adodb-pager.inc.php';
include_once 'modulosPHP/ajudantes/ClassConfig.php';
include_once 'modulosPHP/ajudantes/ClassDatabase.php';

class Conexao {
  private static $sBanco = null;
  private $oBanco;
  private $oConfig;
  private $aMsg;
  private $iLinhas = 0;
  public  $iUltimoId;
  public $sErroDetalhe;
  
  public function __construct() {
    
    $this->definirBanco();
    
    $this->oConfig = new config();

    $oInfoConexao = $this->oConfig->getInfoBd();
    
    $this->oBanco = NewADOConnection($oInfoConexao->sBd);

    $this->oBanco->Connect($oInfoConexao->sServidor, $oInfoConexao->sUsuario, $oInfoConexao->sSenha, $oInfoConexao->sBanco);
    
    if ($this->oBanco->_errorMsg != '') {
      die('Verifique o seguinte erro antes de conectar ao banco de dados: '."\n".$this->oBanco->_errorMsg);
    }
        
  }
  
  public function abrirTransacao() {
    $this->oBanco->StartTrans();
  }
  
  public function efetivarTransacao() {
    $this->oBanco->CompleteTrans();
  }
  
  public function cancelarTransacao() {
    $this->oBanco->FailTrans(); 
  }

  public function buscarUltimoId($sTabela, $iId) {
    $this->iUltimoId = $this->oBanco->Insert_ID($sTabela, $iId);
    return true;
  }

  public function ativarDebug() {
    $this->oBanco->debug = true;
  }

  public function desativarDebug() {
    $this->oBanco->debug = false;
  }

  /**
   * L�gica para determinar qual o dom�nio/banco usar.
   * 
   * Busca o nome do servidor atual e define qual banco de dados dever� carregar
   * 
   * @author Alex Lunardelli
   */
  public static function definirBanco() {
    if (is_null(self::$sBanco)) {
      
      self::$sBanco = Config::buscarAmbiente();

    }

    return true;
  }


  public function query($sQuery) {
    $mRet = $this->oBanco->getAll($sQuery);

    if (!is_array($mRet)) {
      $this->getMsg(false);
      return false;
    }
    $this->iLinhas = count($mRet);
    $this->getMsg(true);
  
    return $mRet;
  }
  
  public function execute($sQuery) {

    $bResultado = true;
    if ($this->oBanco->Execute($sQuery) === false) {
      $this->sErroDetalhe = $this->oBanco->_connectionID->error;
      $bResultado = false;
    }

    $this->getMsg($bResultado);
    return $bResultado;
    
  }
  
  public function montarPaginacao($sQuery, $nrows, $page, $inputarr=false, $secs2cache=0) {
    
    $pager = new ADODB_Pager($this->oBanco,$sQuery, 'paginador');
    $pager->Render($rows_per_page = $nrows);
    
  }


  public function getMsg($mResultado = '') {

    if ($mResultado === '') {
      return $this->aMsg;

    } elseif (in_array($mResultado, array('iCdMsg', 'sMsg', 'sRasultado'))) {
      return $this->aMsg[$mResultado];

    } elseif ($mResultado === true) {
      $this->aMsg = array('iCdMsg' => 0,
                      'sMsg' => 'Sucesso',
                'sResultado' => 'sucesso');

    } elseif ($mResultado === false) {
      $this->aMsg = array('iCdMsg' => 2,
                            'sMsg' => $this->oBanco->ErrorNo().'('.$this->oBanco->ErrorMsg().')',
                      'sResultado' => 'erro');
    }

    return $this->aMsg;
  }
  
  public function getNumeroLinhas() {
    return $this->iLinhas;
  }
  
}
