<?php

class wTools {

  public $sErro;
  public $aParametros;
  public $CFGpath;
  public $sUrlBase;
  public $oCodificador;
  public $aMsg = array('mResultado' => '',
                        'sMsg' => '',
                        'sMsgErro' => '');
  protected $const = false;
  public $RETDB = array();
  public $aPastas = array();
  public $aArquivos = array();
  public $aDadosPaginaAtual = array();
  public $aDadosVariaveisUrlPaginaAtual = array();
  
  public $sPaginaAtualNome;
  public $aPaginaAtualParametros;

  public function __construct() {
    include_once 'ClassConfig.php';
    $oConfig = new Config();

    $this->pathImagens  = $oConfig->Buscar('sCaminhoImagens');
    $this->sUrlBase     = $oConfig->Buscar('sEnderecoServidor');
    $this->oCodificador = $oConfig->Buscar('oCodificador');
  }

  /* wTools::aspas
   *
   * Coloca em destaque um texto relevante com uma imagem de aspas
   * @date 07/10/2010
   *
   */

  public function aspas($sFrase) {
    ?>
    <p class="destaque-aspas">
      <img src="quote.gif" alt="quote" />
      <?php echo $sFrase ?>
    </p>
    <?php
  }

  /* wTools::subLinguagem
   * Recebe o texto de um editor contendo TAGs gen�ricas. Estas TAGs transforam
   * seu conte�do de forma como definida nos arrays.
   *
   * @param  $sConteudo                  - Conteudo a ser transformado
   * @return $this->ConteudoTransformado - Conteudo Transformado
   *
   * $aProcuraPor = array (abertura da tag da sub linguagem,
   *                       fechamento da tag da sub linguagem)
   *
   * $TrocaPor = array(abertura da tag em HTML,
    fechamento da tag em HTML)
   *
   * @date 09/10/2010
   *
   * TAGs CRIADAS - FUNCAO:
   *
   * <aspas> </aspas> - Adiciona o texto de uma forma formatada. Da destaque a uma observa��o ou cita��o importante no contexto do assunto.
   *
   */

  public function subLinguagem($sConteudo) {
    $aProcuraPor = array('&lt;aspas&gt;',
        '&lt;/aspas&gt;',
        '&lt;ver&gt;',
        '&lt;/ver&gt;'
    );
    $aTrocaPor = array('<div class="destaque-aspas"><img src="' . $this->pathImagens . 'quote.gif" alt="quote" />',
        '</div>',
        '<span style="color:#F00">',
        '</span>'
    );


    $sConteudo = str_replace($aProcuraPor, $aTrocaPor, $sConteudo);

    $this->ConteudoTransformado = $sConteudo;

    return $this->ConteudoTransformado;
  }

  /* wTools::novoComentario
   * Adiciona um comentario. Utilizado para paginas como noticias ou artigos.
   *
   * CSS
   *  - class="comentario-post"
   *  - class="titulo"
   *
   * @param  $sAction           - Documento que trata o formulario  
   * @param  $sNomeForm         - Nome do formulario
   * @param  $iRelacionamento   - Valor de um ID ao qual o comentario ira pertencer
   * @param  $sVarDestinoOpc    - Variavel opcional para o tratamento dos dados
   * @param  $sScriptValidacao  - Script para valida��o do formulario
   * @param  $sTitComentario    - Frase que fica no titulo que fica no topo do formulario
   * @return null
   *
   * @date 14/10/2010
   */

  public function novoComentario($sAction, $sNomeForm, $iRelacionamento, $sVarDestinoOpc, $sScriptValidacao = '', $sTitComentario = 'Deixe o seu coment�rio! ') {
    ?>
    <div class="comentario-post">
      <h3 class="titulo"><?php echo $sTitComentario; ?></h3>
      <form action="<?php echo $sAction ?>" name="<?php echo $sNomeForm ?>" method="POST" <?php echo $sScriptValidacao ? 'onSubmit="return ' . $sScriptValidacao . '"' : '' ?> >
        <table width="435" border="0">
          <tr>
            <td>
              <input type="text" name="FRMcoment_autor" value="Nome" tabindex="1" class="FRM_coment" onblur="if (this.value == '') {
                    this.value = 'Nome';
                  }" onfocus="if (this.value == 'Nome') {
                        this.value = '';
                      }" />
            </td>
          </tr>
          <tr>
            <td>
              <input type="text" name="FRMcoment_email" value="Email" tabindex="1" class="FRM_coment" onblur="if (this.value == '') {
                    this.value = 'Email';
                  }" onfocus="if (this.value == 'Email') {
                        this.value = '';
                      }" />
            </td>
          </tr>
          <tr>
            <td>
              <input type="text" name="FRMcoment_site" value="http://www.lunacom.com.br" tabindex="1" class="FRM_coment" onblur="if (this.value == '') {
                    this.value = 'http://www.lunacom.com.br';
                  }" onfocus="if (this.value == 'http://www.lunacom.com.br') {
                        this.value = '';
                      }" />
            </td>
          </tr>
          <tr>
            <td>
              <textarea name="FRM_comentario" rows="8" cols="10" tabindex="4" class="FRM_textarea" onfocus="if (this.value == 'Seu Coment�rio...') {
                    this.value = '';
                  }" onblur="if (this.value == '') {
                        this.value = 'Seu Coment�rio...';
                      }">Seu Coment�rio...</textarea>
            </td>
          </tr>
          <tr>
            <td>
              <input type="hidden" name="sAcao" value="<?php echo $sVarDestinoOpc ?>" />
              <input type="hidden" name="id_relacionamento" value="<?php echo $iRelacionamento ?>"  />
              <input type="submit" value="Enviar" />
            </td>
          </tr>
        </table>
      </form>
    </div>
    <?PHP
  }

  /* wTools::anti_sql_injection
   *
   * Coloca em destaque um texto relevante com uma imagem de aspas
   * @date 23/10/2010
   * @param  string $sStr - String a ser analisada
   * @return string $sStr - String pronta
   */

  public function anti_sql_injection($sStr) {
    if (!is_numeric($sStr)) {
      $sStr = get_magic_quotes_gpc() ? stripslashes($sStr) : $sStr;
//      $sStr = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($sStr) : mysql_escape_string($sStr);
//      $sStr = mysqli_real_escape_string($sStr);
    }
    return $sStr;
  }

  /*    IMPLEMENTAR
   *
   *  wTools::novoForm
   * Adiciona um comentario. Utilizado para paginas como noticias ou artigos.
   *
   * CSS
   *  - class="formNomeInterno"
   *  - class="titulo"
   *
   * @param  $sAction           - Documento que trata o formulario
   * @param  $sNomeForm         - Nome do formulario
   * @param  $sNomeForm         - Array contando os campos que deverao ser exibidos
   * @param  $iRelacionamento   - Valor de um ID ao qual o comentario ira pertencer
   * @param  $sVarDestinoOpc    - Variavel opcional para o tratamento dos dados
   * @param  $sScriptValidacao  - Script para valida��o do formulario
   * @param  $sTitComentario    - Frase que fica no titulo que fica no topo do formulario
   * @return null
   *
   * @date 14/10/2010
   */
  /*
    public function formNomeInterno($sAction, $sNomeForm, $aConfig = '', $iRelacionamento, $sVarDestinoOpc, $sScriptValidacao = '', $sTitForm = '') {




    ?>
    <div class="formNomeInterno">
    <h3 class="titulo"><?php echo $sTitForm ?></h3>
    <form action="<?php echo $sAction ?>" name="<?php echo $sNomeForm ?>" method="post" <?php echo $sScriptValidacao ? 'onSubmit="return '.$sScriptValidacao.'"' : '' ?> >
    <table width="435" border="0">
    <?php if(isset($aConfig['nome'])) {?>
    <tr>
    <td>
    <input type="text" name="FRMcoment_autor" value="Nome" tabindex="1" class="FRM_coment" onblur="if (this.value == ''){this.value = 'Nome';}" onfocus="if (this.value == 'Nome') {this.value = '';}" />
    </td>
    </tr>
    <?php } ?>
    <tr>
    <td>
    <input type="text" name="FRMcoment_email" value="Email" tabindex="1" class="FRM_coment" onblur="if (this.value == ''){this.value = 'Email';}" onfocus="if (this.value == 'Email') {this.value = '';}" />
    </td>
    </tr>
    <tr>
    <td>
    <input type="text" name="FRMcoment_site" value="http://www.lunacom.com.br" tabindex="1" class="FRM_coment" onblur="if (this.value == ''){this.value = 'http://www.lunacom.com.br';}" onfocus="if (this.value == 'http://www.lunacom.com.br') {this.value = '';}" />
    </td>
    </tr>
    <tr>
    <td>
    <textarea name="FRM_comentario" rows="8" cols="10" tabindex="4" class="FRM_textarea" onfocus="if (this.value == 'Seu Coment�rio...') {this.value='';}" onblur="if (this.value == ''){this.value = 'Seu Coment�rio...';}">Seu Coment�rio...</textarea>
    </td>
    </tr>
    <tr>
    <td>
    <input type="hidden" name="funcao" value="<?php echo $sVarDestinoOpc ?>" />
    <input type="hidden" name="id_relacionamento" value="<?php echo $iRelacionamento ?>"  />
    <input type="submit" value="Enviar" />
    </td>
    </tr>
    </table>
    </form>
    </div>
    <?PHP

    }

   */

  /* wTools::validarIndexPaginacao
   *
   * Evita que o usu�rio digite um numero de p�ginas superior ao que realmente
   * existe no sistema quando � utilizado o paginador
   * @date 18/12/2012
   * @param  int    $iPgAtual          - Vem geralmente da vari�vel $_GET['n'] da pagina��o.
   *                                     N�mero da pagina��o atual que esta visivel da url
   * @param  int    $iQntItensListagem - Quantidade de itens listados por p�gina
   * @param  string $sPgRetEmErro      - P�gina em que ser� direcionado em caso de erro
   * @param  string $sTbPaginacao      - Tabela paginacao
   * @return true em caso de suceeso
   */

  public function validarIndexPaginacao($iPgAtual, $iQntItensListagem, $sPgRetEmErro, $sTbPaginacao) {
    $iIndice = $iPgAtual - 1;
    $iIndice = $iIndice * $iQntItensListagem;
    $this->pegaInfoDB($sTbPaginacao, 'count(1)');

    $iNumMaxPaginas = ceil($this->RETDB[0][0] / $iQntItensListagem);


    if ($iPgAtual > $iNumMaxPaginas) {
      header('location:' . $sPgRetEmErro);
      exit;
    }
    return true;
  }

  /* wTools::resumo
   *
   * Cria um resumo de um texto passado como par�metro
   * @date 02/11/2010
   * @param  string $sParagrafo - String a ser analisada
   * @param  array  $aConfig    - Configura��es
   *                  [iQntCar] Quantidade de caracteres a cortar
   *         [bFecharParagrafo] Se encontrar tag <p> ir� fechar
   * @return string $sResumida  - String pronta
   */

  public function resumo($sParagrafo, $aConfig = array()) {

    if (!isset($aConfig['iQntCar'])) {
      $aConfig['iQntCar'] = 150;
    }

    $sParagrafo = strip_tags($sParagrafo, '<p><br><br />');
    $sResumida = substr($sParagrafo, 0, $aConfig['iQntCar']);
    $iCortada = strrpos($sResumida, " ");
    if ($iCortada > $aConfig['iQntCar']) {
      $sResumida = substr($sParagrafo, 0, $iCortada);
    }
    $sResumida . ' ...';

    if (isset($aConfig['bFecharParagrafo']) && $aConfig['bFecharParagrafo'] === true) {
      $iPosParagrafo = strrpos($sResumida, "<p>");
      if (is_numeric($iPosParagrafo)) {
        $sResumida . '</p>';
      }
    }

    return $sResumida;
  }

  /* wTools::enviaImg
   *
   * Upload de Imagens
   * @date 24/11/2010
   * @param  array   $aArquivo        - Um array com o arquivo ex.: $_FILES["img_capa"]
   * @param  string  $sLocal          - Local a salvar a imagem
   * @param  string  $iLargura        - Largura da imagem
   * @param  string  $iAltura         - Altura da imagem
   * @param  string  $bNomeAleatorio  - Coloca um nome aleatorio para a imagem
   * @param  string  $bApagarImg      - Faz upload de uma imagem para o lugar de uma outra
   * @return string  $sNomeImg        - Nome da imagem salva
   * @return string  $iSize           - Tamanho m�ximo da imagem
   */

  public function enviaImg($aArquivo, $sLocal, $iLargura, $iAltura, $bNomeAleatorio = false, $bApagarImg = false, $iSize = 1000000) {

    $modTamanho = '';
    $sNomeImg = 1;

    if ($aArquivo["name"] == '') {
      $this->sMsg = "Por favor, selecione uma figura";
      echo '
		<script type="text/javascript">
			alert("' . $this->sMsg . '");
		</script>
		';
      return $sNomeImg;
    }

    if ((($aArquivo["type"] == "image/gif") || ($aArquivo["type"] == "image/jpeg") || ($aArquivo["type"] == "image/jpg")) && ($aArquivo["size"] < $iSize)) {
      if ($aArquivo["error"] == 0) {

        $aDimensoes = getimagesize($aArquivo["tmp_name"]);

        if (($aDimensoes[0] <= $iLargura || ($aDimensoes[1] <= $iAltura))) {

          // Pega extens�o do arquivo
          preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $aArquivo["name"], $aExtensao);

          // Nome da imagem
          if ($bNomeAleatorio) {
            $sNomeImg = md5(uniqid(time())) . "." . $aExtensao[1];
          } else {
            $sNomeImg = $aArquivo["name"];
          }

          // Caminho de onde a imagem ficar�
          $imagem_dir = $sLocal . $sNomeImg;

          // Faz o upload da imagem
          move_uploaded_file($aArquivo["tmp_name"], $imagem_dir);

          //mensagem de sucesso
          $this->sMsg = "Sua foto foi enviada com sucesso!";
          //aviso($this->sMsg);
          return $sNomeImg;
        } else {
          //mensagem de erro
          //aviso ('Insira uma imagem de no m�ximo '.$iLargura.' x '.$iAltura.' pixels');
          $this->sMsg = '� necess�rio inserir uma imagem com tamanho m�ximo de ' . $iLargura . ' x ' . $iAltura . 'px';
          $this->sErro = 'Erro';
          return $sNomeImg;
        }
      } else {
        //mensagem de erro
        $this->sMsg = 'Erro no carregamento da imagem: ' . $aArquivo["error"];
        $this->sErro = 'Erro';
        return $sNomeImg;
      }
    } else {
      //mensagem de erro
      $this->sMsg = "A imagem n�o pode ser enviada com sucesso.";
      $this->sErro = 'Erro';
      return $sNomeImg;
    }
  }

  /* wTools::getSeguro
   *
   * Testa se o conte�do recebido via GET � o esperado
   * @date 14/12/2010
   * @param  string  $iId            - Id a testar
   * @return string  $iIndice        - Retorna o numero referente ao indice ou 0 caso seja malicioso.
   */

  public function getSeguro($iId) {
    // Testa se � a pagina principal ou uma sub-pagina
    if ((isset($iId)) && (is_numeric($iId))) {
      $iIndice = $iId;
    } else {
      $iIndice = 0;
    }
    return $iIndice;
  }

  
  /* wTools::subArray
   *
   * Divide um array em partes
   * @date 27/02/2011
   * @param  array     $aDados         - Array a ser dividido
   * @param  integer   $iAgrupa        - Quantidade de registros que cada indice do array ter�
   * @return array     $aArrayDividido - Resultato dividido

   */

  public function subArray($aDados, $iAgrupa) {
    $iQnt = count($aDados);
    $iRepete = ceil($iQnt / $iAgrupa);
    $iOffSet = 0;

    for ($i = 0; $i < $iRepete; $i++) {
      $aArrayDividido[] = array_slice($aDados, $iOffSet, 4);
      $iOffSet += $iAgrupa;
    }

    return $aArrayDividido;
  }

  /* wTools::montaUrlAmigavel
   *
   * Coloca em destaque um texto relevante com uma imagem de aspas
   * @date 13/12/2010
   * 
   * Atualiza��es:
   * 05/01/2013
   * Incluido prote��o anti injection
   * 
   * @param  string $sFrase - String a ser analisada
   * @param  bool   $bContraBarra - Contrabarra na string de retorno
   * @return string $sRet   - String pronta
   */

  public function montaUrlAmigavel($sFrase = '', $bContraBarra = true) {
    if ($sFrase == '') {
      return false;
    }

    $sFrase = $this->anti_sql_injection($sFrase);

    $sFrase = trim($sFrase);
    $sFrase = strtolower($sFrase);

    $sFrase = str_replace(' ', '-', $sFrase);
    $sFrase = preg_replace("/[����]/", "a", $sFrase);
    $sFrase = preg_replace("/[���]/", "e", $sFrase);
    $sFrase = preg_replace("/[��]/", "i", $sFrase);
    $sFrase = preg_replace("/[�����]/", "o", $sFrase);
    $sFrase = preg_replace("/[���]/", "u", $sFrase);
    $sFrase = preg_replace("/[.;:@#%&=?!$*,%(){}+']/", "", $sFrase);
    $sFrase = str_replace('--', '-', $sFrase);
    $sFrase = str_replace("�", "c", $sFrase);
    $sFrase = str_replace("\"", "", $sFrase);


    // Algumas vezes o link termina com -
    $sFrase = preg_replace("/-$/", "", $sFrase);
    $sFrase = $sFrase . ($bContraBarra ? '/' : '');
    $sRet = $sFrase;

    return $sRet;
  }

  /* wTools::montaSelect
   *
   * Coloca em destaque um texto relevante com uma imagem de aspas
   * @date 03/05/2011
   * @update 01/05/2015 Trocado o nome do m�todo de montaSelect para montarSelect
   * 
   * @param  string $sNome    - String a ser analisada
   * @param  array  $aValores - Array com os valores (value, nome)
   * @param  bool   $bId      - Se tiver id, repete o nome do campo
   * @param  string $sClass   - Nome da classe
   * @return true
   */

  public function montarSelect($sNome, $aValores, $sSelecionado = '', $bId = true, $sClass = '', $sJsAdicional = '', $sBranco = 'Selecione um item') {
    ?>

    <select name="<?php echo $sNome; ?>" <?php
    echo ($bId) ? 'id="' . $sNome . '"' : '';
    echo ($sClass) ? 'class="' . $sClass . '"' : '';
    echo ($sJsAdicional) ? $sJsAdicional : ''
    ?> >
      <?php if ($sBranco) { ?>
        <option <?php echo ($sSelecionado == '') ? 'selected="selected"' : ''; ?>value=""><?php echo $sBranco; ?></option>
        <?php
      }
      foreach ($aValores as $sKey => $sValue) {
        ?>
        <option <?php echo ($sSelecionado == $sKey) ? 'selected="selected"' : '' ?>value="<?php echo $sKey; ?>"><?php echo $sValue; ?></option>
        <?php
      }
      ?>
    </select>
    <?php
  }

  /* wTools::montarRadio
   *
   * Cria o HTML de um campo Radio
   * @date 01/06/2013
   * @param  string  $sNome         - String a ser analisada
   * @param  array   $aValores      - Array com os valores (value, nome)
   * @param  string  $sSelecionado  - Valor do campo que vir� selecionado
   * @param  bool    $bQuebrarLinha - Adicionar um <br /> no final de cada input
   * @param  bool    $bId           - Se tiver id, repete o nome do campo
   * @param  string  $sClass        - Nome da classe
   * @param  string  $sJsAdicional  - Chamada adicional de uma fun��o JS
   * @return true
   */

  public function montarRadio($sNome, $aValores, $sSelecionado = '', $bQuebrarLinha = false, $bId = true, $sClass = '', $sJsAdicional = '') {
    foreach ($aValores as $sKey => $sValue) {
      ?>
      <input type="radio" name="<?php echo $sNome; ?>" <?php
             echo ($bId ? 'id="' . $sNome . '-' . $sKey . '"' : '');
             echo ($sClass != '' ? 'class="' . $sClass . '"' : '');
             echo $sJsAdicional;
             ?> value="<?php echo $sKey; ?>" <?php echo ($sSelecionado == $sKey ? 'checked="checked"' : '') ?>/> <?php echo $sValue;
       echo ($bQuebrarLinha ? '<br />' : '');
             ?>
             <?php
           }
         }

         /* wTools::valida_sAcao
          *
          * Valida as possiveis acoes permitidas dentro das p�ginas de admin.
          * @date 07/05/2011
          * @param string $sAcao  - novo ou editar
          * @param string $sRedir - P�gina para redirecionar em caso de erro
          * @return $sAcao
          */

         public function valida_sAcao($sAcao, $sRedir) {
           $bRedirSeguranca = false;
           if (isset($sAcao)) {
             if ($sAcao != 'novo' && $sAcao != 'editar') {
               $bRedirSeguranca = true;
             }
           } else {
             $bRedirSeguranca = true;
           }
           if ($bRedirSeguranca) {
             header("Location: $sRedir");
             exit();
           }
           return $sAcao;
         }

         /* wTools::msgRetPost
          *
          * Monta um array com a mensagem de retorno ao usu�rio.
          * Deve ser usada quando uma p�gina recebe um formul�rio via POST contendo o
          * resultado de uma edi��o de dados realizada em outra p�gina.
          *
          * Atualizado em 30/10/2012
          * Adicionado um par�metro com uma poss�vel mensagem. Ela tem prioridade.
          *
          * @date 30/03/2012
          * @param 
          * @return mixed Array com a mensagem montada ou string vazia
          */

         public function msgRetPost($aMsg) {

           if (isset($aMsg['sResultado']) && $aMsg['sResultado'] != '') {
             return $aMsg;
           }
           if (isset($aMsg['sMsg']) && $aMsg['sMsg'] != '') {
             return $aMsg;
           }

           $mResultado = '';
           if (isset($_POST['retMsg'])) {
             $mResultado = array('iCdMsg' => $_POST['iCdMsg'],
                 'sMsg' => $_POST['sMsg'],
                 'sResultado' => $_POST['sResultado']);
           }
           return $mResultado;
         }

         /* wTools::redirFRM
          *
          * Redireciona uma p�gia utilizando Javascript e enviando dados atrav�s de formul�rio
          * @date 08/05/2011
          * @param string $sUrl     - Endere�o da p�gina de destino.
          * @param array  $aCampos  - Campos a serem enviados pelo formulario. Indice do array = nome do campo.
          * @return true
          */

         public function redirFRM($sUrl, $aCampos = array()) {
           ?>
    <form id="FRMredir" method="post" action="<?php echo $sUrl ?>">
      <input type="hidden" name="retMsg" value="retMsg" />
    <?php foreach ($aCampos as $sIndice => $sValor) { ?>
        <input type="hidden" name="<?php echo $sIndice; ?>" value="<?php echo $sValor; ?>" /> <?php
    }
    ?>        
    </form>
    <script type="text/javascript">
      document.forms["FRMredir"].submit();
    </script>
    <?php
    exit;
  }

  /* wTools::montaTwinList
   *
   * Monta uma lista com dois campos, itens disponiveis e itens j� relacionados
   * com os bot�es para incluir ou retirar valores.
   * Para o twinlist funcionar � preciso de duas tabelas, uma de cadastro com nomes de itens e outra tabela de relacionamento
   * onde ela informa os campos que j� est�o salvos.
   *
   * @date 31/03/2012
   * @param  string $sNomeTwinList - Nome da twinlist e nome do campo que dever� ser salvo no banco de dados
   * @param  int    $iIdRegEditado - Id do registro que esta sendo editado ou em caso de um novo item
   *                                 (pagina de insert) ele dever� ser 0
   * @param  array  $aDadosTbCadastro - Dados devem estar nesta ordem 0 => nome da tabela 1=> id( ser� o campo
   *                                    value do option do select) 2=> nome do campo (� o valor visivel ao usuario)
   *                                    ex.: $aDadosTbCadastro = array('tc_tags', 'id', 'nm_tag');
   * @param  array  $aDadosTbRelacionamento - Dados devem estar nesta ordem 0 => nome da tabela de relacionamento
   *                                          1 => id estrangeiro 2 => id principal
   *                                          ex.: $aDadosTbRelacionamento = array('tr_prod_tag', 'id_tag', 'id_prod');
   * @return true
   *
   *
   */

  public function montarTwinList($sNomeTwinList, $iIdRegEditado = '', $aDadosTbCadastro = '', $aDadosTbRelacionamento = '', $sFiltro = '') {

    if ($sFiltro != '') {
      $sFiltro = "\n " . $sFiltro . " \n";
    }
    ?>
    <div>
      <select size="10" multiple="multiple" id="CMPdisponiveis_<?php echo $sNomeTwinList; ?>" name="CMPdisponiveis_<?php echo $sNomeTwinList; ?>[]" style="width: 200px; float: left" class="TLdisponiveis">
        <?php
        // Tratamento de dados recebidos por POST
        if (isset($_POST['CMPdisponiveis_' . $sNomeTwinList])) {
          $sIdsFrom = implode(',', $_POST['CMPdisponiveis_' . $sNomeTwinList]);
          $sWhere = 'WHERE ' . $aDadosTbCadastro[1] . ' IN (' . $sIdsFrom . ')';

          if ($iIdRegEditado != 0) {
            $sWhere .= ' AND ' . $aDadosTbCadastro[1] . ' 
                        NOT IN (SELECT ' . $aDadosTbRelacionamento[1] . ' 
                                  FROM ' . $aDadosTbRelacionamento[0] . ' 
                                 WHERE ' . $aDadosTbRelacionamento[2] . ' = ' . $iIdRegEditado . ')';
          }


          // Caso acontece quando todos itens foram selecionados para o campo da direita
        } elseif (isset($_POST[$sNomeTwinList])) {
          $sWhere = 'WHERE 1 = 0';

          // Carregar dados no select
        } else {

          $sIdsFrom = 'SELECT ' . $aDadosTbRelacionamento[1] . '
                           FROM ' . $aDadosTbRelacionamento[0] . '
                          WHERE ' . $aDadosTbRelacionamento[2] . ' = ' . $iIdRegEditado;
          $sWhere = 'WHERE ' . $aDadosTbCadastro[1] . ' NOT IN (' . $sIdsFrom . ')';
        }
        $sOrder = ' ORDER BY ' . $aDadosTbCadastro[2];
        $aListaEsq = array();
        $this->pegaInfoDB($aDadosTbCadastro[0], array($aDadosTbCadastro[1], $aDadosTbCadastro[2]), $sWhere . $sFiltro . $sOrder);
        $aListaEsq = $this->RETDB;
        foreach ($aListaEsq as $aDados) {
          ?>
          <option value="<?php echo $aDados[0]; ?>"><?php echo $aDados[1]; ?></option>
      <?php
    }
    ?>
      </select>

      <div style="float: left; margin: 0 10px 0 10px ">
        <input type="hidden" id="CMPtwinList_<?php echo $sNomeTwinList; ?>" name="CMPtwinList_<?php echo $sNomeTwinList; ?>" value="<?php echo $sNomeTwinList; ?>" />
        <input type="button" id="bt_d_<?php echo $sNomeTwinList; ?>"  class="bt tl_d"  style="width: 40px; margin: 1px" value=">" /><br />
        <input type="button" id="bt_td_<?php echo $sNomeTwinList; ?>" class="bt tl_td" style="width: 40px; margin: 1px" value=">>" /><br />
        <input type="button" id="bt_e_<?php echo $sNomeTwinList; ?>"  class="bt tl_e"  style="width: 40px; margin: 1px" value="<" /><br />
        <input type="button" id="bt_te_<?php echo $sNomeTwinList; ?>" class="bt tl_te" style="width: 40px; margin: 1px" value="<<" />
      </div>

      <select size="10" multiple="multiple" id="<?php echo $sNomeTwinList; ?>" name="<?php echo $sNomeTwinList; ?>[]" style="width: 200px;  float: left" class="TLselecionados">
        <?php
        // Tratamento de dados recebidos por POST
        if (isset($_POST[$sNomeTwinList])) {
          $sIdsFrom = implode(',', $_POST[$sNomeTwinList]);
          $sWhere = 'WHERE ' . $aDadosTbCadastro[1] . ' IN (' . $sIdsFrom . ')';
        } else {

          // Carregar dados no select
          $sIdsFrom = 'SELECT ' . $aDadosTbRelacionamento[1] . '
                           FROM ' . $aDadosTbRelacionamento[0] . '
                          WHERE ' . $aDadosTbRelacionamento[2] . ' = ' . $iIdRegEditado;
          $sWhere = 'WHERE ' . $aDadosTbCadastro[1] . ' IN (' . $sIdsFrom . ')';
        }

        $sOrder = ' ORDER BY ' . $aDadosTbCadastro[2];
        $this->pegaInfoDB($aDadosTbCadastro[0], array($aDadosTbCadastro[1], $aDadosTbCadastro[2]), $sWhere . $sFiltro . $sOrder);
        $aListaDir = $this->RETDB;
        foreach ($aListaDir as $aDadosDir) {
          ?>
          <option value="<?php echo $aDadosDir[0]; ?>"><?php echo $aDadosDir[1]; ?></option>
      <?php
    }
    ?>
      </select>
    </div>
    <div style="clear: both;">&nbsp;</div>
    <?php
    return true;
  }

  public function montarJsTwinList($sIdFormulario) {

    echo '
        // 1) Ao enviar o formul�rio ele deve ser tratado via JS
        $(\'#' . $sIdFormulario . '\').submit(function(){
          sIdNomeTl = \'CMPctegorias\';
          //$("select[name=\'CMPdisponiveis_"+sIdNomeTl+"[]\'] option").each(function () {

          $("select[class=\'TLdisponiveis\'] option").each(function () {
            $(this).attr(\'selected\', \'selected\');
          });
          $("select[class=\'TLselecionados\'] option").each(function () {
            $(this).attr(\'selected\', \'selected\');
          });
        });';

    echo '

        // 2) Bot�o "Todos para Direita"
        $(\'.tl_td\').click(function(){
          sIdNomeTl = $(this).attr("id");
          sIdNomeTl = sIdNomeTl.replace("bt_td_", "");

          $("select[name=\'CMPdisponiveis_"+sIdNomeTl+"[]\'] option").each(function () {
            $(\'#\'+sIdNomeTl).append(\'<option value="\'+$(this).val()+\'">\'+$(this).text()+\'</option>\');
            $("select[name=\'CMPdisponiveis_"+sIdNomeTl+"[]\'] option").remove();
          });

        });

        // 3) Bot�o "Selecionados para Direita"
        $(\'.tl_d\').click(function(){
          sIdNomeTl = $(this).attr("id");
          sIdNomeTl = sIdNomeTl.replace("bt_d_", "");

          $("select[name=\'CMPdisponiveis_"+sIdNomeTl+"[]\'] option:selected").each(function () {
            $(\'#\'+sIdNomeTl).append(\'<option value="\'+$(this).val()+\'">\'+$(this).text()+\'</option>\');
            $("select[name=\'CMPdisponiveis_"+sIdNomeTl+"[]\'] option:selected").remove();
          });
        });

        // 4) Bot�o "Todos para Esquerda"
        $(\'.tl_te\').click(function(){
          sIdNomeTl = $(this).attr("id");
          sIdNomeTl = sIdNomeTl.replace("bt_te_", "");

          $("select[name=\'"+sIdNomeTl+"[]\'] option").each(function () {
            $(\'#CMPdisponiveis_\'+sIdNomeTl).append(\'<option value="\'+$(this).val()+\'">\'+$(this).text()+\'</option>\');
            $("select[name=\'"+sIdNomeTl+"[]\'] option").remove();
          });
        });

        // 5) Bot�o "Selecionados para Direita"
        $(\'.tl_e\').click(function(){
          sIdNomeTl = $(this).attr("id");
          sIdNomeTl = sIdNomeTl.replace("bt_e_", "");

          $("select[name=\'"+sIdNomeTl+"[]\'] option:selected").each(function () {
            $(\'#CMPdisponiveis_\'+sIdNomeTl).append(\'<option value="\'+$(this).val()+\'">\'+$(this).text()+\'</option>\');
            $("select[name=\'"+sIdNomeTl+"[]\'] option:selected").remove();
          });
        });
    ';
  }

  /* wTools::montaSelectDB
   *
   * Coloca em destaque um texto relevante com uma imagem de aspas
   * @date 21/05/2011
   * @param  string $sNome    - String a ser analisada
   * @param  array  $aValores - Array com os valores (value, nome)
   * @param  bool   $bId      - Se tiver id, repete o nome do campo
   * @param  string $sClass   - Nome da classe
   * @return true
   */

  public function montaSelectDB($sNome, $sTabela, $sChave, $sRotulo, $sSelecionado = '', $bId = true, $sClass = '', $sJsAdicional = '', $sBranco = 'Selecione um item', $sWhere = '') {
    ?>

    <select name="<?php echo $sNome; ?>" <?php
      echo ($bId) ? 'id="' . $sNome . '"' : '';
      echo ($sClass) ? ' class="' . $sClass . '" ' : '';
      echo ($sJsAdicional != '') ? $sJsAdicional : ''
      ?> >
      <?php if ($sBranco) { ?>
        <option <?php echo ($sSelecionado) ? '' : 'selected="selected"' ?>value=""><?php echo $sBranco; ?></option>
        <?php
      }

      $sQuery = 'SELECT ' . $sChave . '  AS chave,
                        ' . $sRotulo . ' AS rotulo
                   FROM ' . $sTabela . '
                        ' . $sWhere;
      $sResultado = mysql_query($sQuery);

      if (!$sResultado) {
        die('Erro ao utilizar o m�todo montaSelectDB: ' . mysql_error());
        return false;
      }

      while ($aResultado = mysql_fetch_array($sResultado)) {
        ?>
        <option <?php echo ($sSelecionado == $aResultado['chave'] || $sSelecionado == $aResultado['rotulo'] ) ? 'selected="selected"' : '' ?>value="<?php echo $aResultado['chave']; ?>"><?php echo $aResultado['rotulo']; ?></option> <?php
    }
    ?>
    </select>
    <?php
  }

  /* wTools::montaLink
   *
   * Recebe os dados para criar um link via PHP
   * @date 18/07/2011
   * @update 27/11/2011 - Atualizada a forma de buscar o path do link
   * @update 19/11/2017 - Adicionada funcionalidade para criptografar a URL
   * @param  string $sTexto          - String a ser analisada
   * @param  string $sLink           - Link da p�gina
   * @param  string $sParametrosUrl  - Parametros URL
   * @param  string $CFGpath         - 
   * @param  string $sClass          - Inserir uma classe especifica para o link
   * @param  bool   $bImprimir       - Escrever o link na tela
   * @return true
   */

  public function MontarLink($sTexto, $sLink, $sParametrosUrl = '', $CFGpath = '', $bImprimir = true, $sClass = '') {

    if (isset($this->sUrlBase) && $CFGpath == '') {
      $CFGpath = $this->sUrlBase . '/';
    }

    if ($sParametrosUrl != '') {
      $sParametrosUrl = '?'.$this->CodificarParametrosString($sParametrosUrl);
    }
    
    if ($bImprimir) {
      ?>
      <a href="<?php echo $CFGpath; ?><?php echo $sLink . $sParametrosUrl; ?>" <?php echo 'class="' . $sClass . '"'; ?>><?php echo $sTexto; ?></a>
      <?php
    }

    return '<a href="' . $CFGpath . $sLink . $sParametrosUrl. '"><' . $sTexto . '</a>';
  }

  /* wTools::parseValue
   *
   * Configura valores de formas a serem utilizados em locais diferentes
   * @date 19/11/2011
   * @param  string $sCampo   - Valor a ser formatado
   * @param  string $sFormato - Formato a ser configurado o valor
   * @return $sRetCampo       - Valor a ser usado para inserir no banco de dados
   */

  public function parseValue($sCampo, $sFormato) {
    $aRetornarNumero = array('moeda-bd', 'moeda-db', 'decimal-bd');
    $sRetCampo = '';

    if ($sCampo == '' || $sCampo == null) {

      // N�meros devem retornar com 0, strings vazias ('').
      if (in_array($sFormato, $aRetornarNumero)) {
        return '0.00';
      } elseif (is_string($sCampo)) {
        return '';
      }
    }
    switch ($sFormato) {
      case 'dt-bd':
      case 'dt-db':
      case 'dt-br':
        $aResult = explode('/', $sCampo);
        $sRetCampo = $aResult[2] . '-' . $aResult[1] . '-' . $aResult[0];
        break;

      case 'bd-dt':
      case 'db-dt':
        $aResult = explode('-', $sCampo);
        $sRetCampo = substr($aResult[2], 0, 2) . '/' . substr($aResult[1], 0, 2) . '/' . substr($aResult[0], 0, 4);
        break;

      case 'moeda-bd':
      case 'moeda-db':

        $iQntVirg = strpos($sCampo, ',');
        $iQntPonto = strpos($sCampo, '.');

        // Passa o n�mero para formato americano
        if ($iQntVirg > $iQntPonto) {
          $sCampo = str_replace('.', '', $sCampo);
          $sCampo = str_replace(',', '.', $sCampo);
        } else {
          $sCampo = str_replace(',', '', $sCampo);
        }

        $sRetCampo = number_format($sCampo, 2, ".", "");
        break;

      case 'db-moeda':
      case 'bd-moeda':
      case 'reais':

        $iQntVirg = strpos($sCampo, ',');
        $iQntPonto = strpos($sCampo, '.');

        // Passa o n�mero para formato americano
        if ($iQntVirg > $iQntPonto) {
          $sCampo = str_replace('.', '', $sCampo);
          $sCampo = str_replace(',', '.', $sCampo);
        } else {
          $sCampo = str_replace(',', '', $sCampo);
        }
        $sCampo = (float) $sCampo;
        $sRetCampo = number_format($sCampo, 2, ",", ".");
        //http://leocaseiro.com.br/moedas-decimais-funcao-number_format-php
        break;

      case 'decimal-bd':
        $sRetCampo = str_replace(',', '.', $sCampo);
        break;
    }




    return $sRetCampo;
  }

  /* wTools::pegarExtensao
   *
   * Retorna a extensao da string passada
   * @date 19/11/2011
   * @param  string $sNome - Campo com um nome contendo extensao  
   * @return $sExt         - Extensao do arquivo
   */

  public function pegarExtensao($sNome) {
    $aTmp = explode('.', $sNome);
    $sExt = strtolower(end($aTmp));
    return $sExt;
  }

  /* wTools::retirarExtensao
   *
   * Retorna a extensao da string passada
   * @date 19/11/2011
   * @param  string $sNome - Campo com um nome contendo extensao  
   * @return $sExt         - Extensao do arquivo
   */

  public function retirarExtensao($sNome) {
    $aTmp = explode('.', $sNome);
    $sExt = strtolower(reset($aTmp));
    return $sExt;
  }

  /* wTools::montarIN
   *
   * Busca valores salvos na tabela de par�metros
   * @date 19/11/2011
   * @param  string $mParametros - Array ou String contendo os par�metros solicitados
   * @return $sValorMontado               - Extensao do arquivo
   */

  public function montarIN($mValores) {
    if (!is_array($mValores)) {
      $aValores = explode(',', $mValores);
    } else {
      $aValores = $mValores;
    }
    $sValorMontado = '';
    foreach ($aValores as $sValor) {
      // Isto aqui deu erro ao enviar para o servidor, por isso tirei o anti injection
      $sValorMontado .= "'" . $this->anti_sql_injection($sValor) . "', ";
      //$sValorMontado .= "'".$sValor."', ";
    }
    $sValorMontado = substr($sValorMontado, 0, -2);

    return $sValorMontado;
  }

  /* wTools::resumirTexto
   *
   * Trata um conte�do deixando somente uma quantidade de texto para ser usada
   * como resumo.
   * @date 04/12/2011
   * @param  string $sParagrafo  - Texto a ser tratado
   * @param  integer $iQntLetras - Quantidade de caracteres que ser�o exibidos
   * @return string $sResumo     - Texto tratado no tamanho definido
   */

  public function resumirTexto($sParagrafo, $iQntLetras) {
    $sParagrafo = strip_tags($sParagrafo);

    $sResumo = substr($sParagrafo, 0, $iQntLetras);
    $iCorta = strrpos($sResumo, " ");
    $sResumo = substr($sParagrafo, 0, $iCorta);

    return $sResumo;
  }

  /* wTools::caixaItensRelacionados
   *
   * Monta uma caixa que apresenta uma lista de links relacionados a um assunto
   * para que o usu�rio continue navegando
   * @date  12/02/2012
   * @param string $sSql   - Script para busca de dados
   * @param string $sClass - Estilo que ser� apresentada a lista
   * @return true
   */

  public function caixaItensRelacionados($sSql, $sClass, $sDesc = 'Itens Relacionados:') {

    $this->buscarInfoDB($sSql);
    ?>
    <div class="<?php echo $sClass; ?>">
      <h3><?php echo $sDesc; ?></h3>
      <ul>
    <?php
    foreach ($this->RETDB as $aDados) {
      ?>
          <li><?php $this->montaLink($aDados[0], $aDados[1]); ?></li>
    <?php }
    ?>
      </ul>
    </div>

    <?php
    return true;
  }

  /* wTools::montarStringDados
   *
   * Transforma um array em uma string, mantendo os valores separados por:
   * [chave] valor do campo
   * @date  19/03/2012
   * @param array $aDados - No formato array('indice' => 'valor do campo')
   * @return string montada
   */

  public function montarStringDados($aDados) {
    if (!is_array($aDados)) {
      return false;
    }

    $sRet = '';
    foreach ($aDados as $sChave => $sValor) {
      $sRet .= '##C_' . $sChave . '**V_' . $sValor;
    }
    return $sRet;
  }

  /* wTools::desmontarStringDados
   *
   * Transforma uma string em um array, mantendo os valores separados por:
   * [chave] valor do campo
   * @date  19/03/2012
   * @param string $sDados - No formato ##C_campo**V_valor
   * @return array $aDados
   */

  public function desmontarStringDados($sDados) {
    $aResult = explode('##', $sDados);
    array_shift($aResult);
    $aRet = array();

    foreach ($aResult as $sChave) {
      $sChave = str_replace(array('C_', 'V_'), '', $sChave);
      $aChaveValor = explode('**', $sChave);

      $aRet[$aChaveValor[0]] = $aChaveValor[1];
    }
    return $aRet;
  }

  /* wTools::msgRetAlteracoes
   *
   * Mensagem de retorno ao usu�rio ap�s altera��es realizadas em registros
   * Atualiza��o: 14/05/2012 - $mResultado passa a aceitar array com as mensagens
   *
   * @date 07/05/2011
   *
   * @param mixed  $mCdMsg      - Tipo de resultado pode ser um codigo (0, 1, 2),
   *                              uma mensagem tipo 'sucesso' ou um array
   * @param string $sMsg        - Mensagem para apresenta��o na tela
   * @param string $sMsgErro    - Mensagem de erro caso aconte�a
   * @param bool   $bMarcaVazio - Preenche com uma div o espa�o euquando n�o h� mensagem a ser exibida de retorno ao usu�rio
   * @return true
   */

  public function msgRetAlteracoes($mResultado = '', $sMsg = '', $sMsgErro = '', $bMarcaVazio = true) {

    if (is_array($mResultado)) {
      $this->sMsg = $mResultado['sMsg'];
      $this->sErro = isset($mResultado['sErro']) ? $mResultado['sErro'] : null;
      $mResultado = $mResultado['iCdMsg'];
    }

    if (isset($this->sResultado)) {
      $mResultado = $this->sResultado;
    }
    if (!isset($mResultado)) {
      if ($bMarcaVazio) {
        ?>
        <div class="msg-vazio">&nbsp;</div> <?php
      }
      return false;
    }
    if (isset($this->sMsg)) {
      $sMsg = $this->sMsg;
    }
    if (isset($this->sErro) && $this->sErro != '') {
      $sMsgErro = $this->sErro;
    }

    switch ($mResultado) {
      case 'sucesso':
      case '0':
        ?>
        <div class="msg-ok"><?php echo $sMsg ?></div> <?php
        break;

      case 'erro':
      case '1':
        ?>
        <div class="msg-erro"><?php echo $sMsg;
        echo ($sMsgErro) ? $sMsgErro : ''
        ?></div><?php
        break;

      case 'atencao':
      case '2':
        ?>
        <div class="msg-atencao"><?php echo $sMsg;
        echo ($sMsgErro) ? ' - ' . $sMsgErro : ''
        ?></div><?php
        break;

      default:
        if ($bMarcaVazio) {
          ?>
          <div class="msg-vazio">&nbsp;</div> 
          <?php
        }
        break;
    }
    return true;
  }

  /* wTools::msgRetAlteracoes_montar
   *
   * Monta o array de mensagens de altera��es
   * 
   * @date  08/05/2012
   * @param integer $iCdMsg  - 0: Sucesso, 1:Erro , 2: Aten��o
   * @param string $sMsg     - Mensagem que � exibida na tela
   * @param string $sMsgErro - Em caso de erro, alguma mensagem pode ser informada ao usu�rio
   * @return array $aMsg
   */

  public function msgRetAlteracoes_montar($iCdMsg, $sMsg, $sResultado = '', $sMsgErro = '') {
    $aMsg = array();
    $aMsg['iCdMsg'] = $iCdMsg;
    $aMsg['sMsg'] = $sMsg;
    $aMsg['sErro'] = $sMsgErro;
    $aMsg['sResultado'] = $sResultado;
    return $aMsg;
  }

  /* wTools::tratarString
   *
   * Trabalha uma string passada como par�metro
   *
   * @date  04/06/2012
   * @param string $sString  - String a ser tratada
   * @param string $iTipo    - Tipo de tratamento
   * @return string  $sRetorno
   */

  public function tratarString($sString, $iTipo) {

    switch ($iTipo) {

      // Removendo s�mbolos de uma string (caracteres n�o alfa-num�ricos)
      case 0:
        $sRetorno = trim(preg_replace("/[^a-zA-Z0-9\s]/", "", $sString));
        break;

      // Removendo s�mbolos e n�meros
      case 1:
        $sRetorno = trim(preg_replace("/[^a-zA-Z\s]/", "", $sString));
        break;

      // Removendo letras e s�mbolos
      case 2:
        $sRetorno = trim(preg_replace("/[^0-9\s]/", "", $sString));
        break;

      // Removendo s�mbolos de uma string (caracteres n�o alfa-num�ricos, incluindo espa�o)
      case 3:
        $sRetorno = trim(preg_replace("/[^a-zA-Z0-9]/", "", $sString));
        break;
    }
    return $sRetorno;
  }

  /**
   * wTools::showNumero()
   *
   * Mostra campos num�ricos
   *
   * @param string  $sNome      Nome do campo
   * @param integer $iTamMax    N�mero m�ximo de caracteres digit�veis (maxlength)
   * @param string  $sValor     Valor inicial
   * @param integer $iDigitos   N�mero de casas decimais
   * @param float   $fValMin    Valor m�nimo admiss�vel para o campo (pode ser vazio, se n�o houver limite)
   * @param float   $fValMax    Valor m�ximo admiss�vel para o campo (pode ser vazio, se n�o houver limite)
   * @param integer $iTam       Tamanho do campo (size)
   * @param string  $sParms     Par�metros adicionais a incluir no campo (class, etc.)
   * @param string  $sMascara   M�scara de caracteres v�lidos, no formato de uma express�o regular
   * @param array   $aExtraFunc Array contendo como chave a fun��o JS na qual ser� concatenado o valor passado naquela chave
   * @param string  $sIdForm    String contendo o id do campo no formul�rio
   * @param Boolean $bMascara   Boolean informando se deve (padrao true) ou n�o (false) usar funcao JS de formatacao de dinheiro
   * @param Boolean $bEcho      Boolean informando se deve imprimir ou retornar a string do input html
   *
   * @access public
   * @author Equipe Ditech <ditech@ditech.com.br>
   */
  function showNumero($sNome, $iTamMax, $sValor = '', $iDigitos = 2, $fValMin = '', $fValMax = '', $iTam = 0, $sParms = '', $sMascara = '0-9', $aExtraFunc = '', $sIdForm = '', $bMascara = true, $bEcho = true, $bBloqueiaEnter = false) {
    if ($iTam == 0) {
      $iTam = ceil($iTamMax * 1.5);
    }
    if (!$sIdForm) {
      $sIdForm = $sNome;
    }

    $sInput = '<input size="' . $iTam . '" type="text" name="' . $sNome . '" id="' . $sIdForm . '" maxlength="' . $iTamMax . '" value="' . $sValor . '" onKeyPress="return validaTecla(this, event, \'\', \'' . ($bBloqueiaEnter ? 'true' : 'false') . '\'); ' . $aExtraFunc['onKeyPress'] . '" ' . ' onBlur="' . ($fValMin != '' || $fValMax != '' ? 'verifyRange(this, ' . (float) $fValMin . ', ' . (float) $fValMax . ');' : '') . ($bMascara ? 'mascaraDinheiro(this, ' . $iDigitos . ');' : '') . $aExtraFunc['onBlur'] . '" ' . 'onFocus="focaNumero(this);' . $aExtraFunc['onFocus'] . '"' . $sParms . '>';
    if ($bEcho == true) {
      echo $sInput;
    } else {
      return $sInput;
    }
  }

  /**
   * DitechUtil::showValor()
   *
   * Mostra campos de valor (dinheiro)
   *
   * @param string $sNome     - nome do campo
   * @param string $sId       - id do campo
   * @param integer $iTamMax  - maxlength
   * @param string $sValor    - valor inicial
   * @param integer $iDigitos - casas decimais
   * @param string $iValMin   - valor m�nimo para o campo
   * @param string $iValMax   - valor m�ximo para o campo
   * @param integer $iTam     - size
   * @param string $sParms    - par�metros adicionais
   * @param Boolean $bEcho      Boolean informando se deve imprimir ou retornar a string do input html
   * @param array   $aExtraFunc Array contendo como chave a fun��o JS na qual ser� concatenado o valor passado naquela chave
   * @return string
   * */
  function showValor($sNome, $sId, $iTamMax = 14, $sValor = '', $iDigitos = 2, $iValMin = '0', $iValMax = '9999999999999', $iTam = 18, $sParms = '', $bEcho = true, $aExtraFunc = '', $bBloqueiaEnter = false) {

    $sInput = '<input type="text" name="' . $sNome . '" id="' . $sId . '" value="' . $sValor . '" size="' . $iTam . '" maxlength="' . $iTamMax . '" style="text-align: right;"' . ' onFocus="if(this.value==\'0,00\')this.value=\'\'; focaNumero(this); ' . $aExtraFunc['onFocus'] . '" ' . ' onBlur="if(this.value==\'\')this.value=\'0,00\'; verifyRange(this, ' . $iValMin . ', ' . $iValMax . '); mascaraDinheiro(this, ' . $iDigitos . '); ' . $aExtraFunc['onBlur'] . '" ' . ' onKeyPress="return validaTecla(this, event, \'\', \'' . ($bBloqueiaEnter ? 'true' : 'false') . '\'); ' . $aExtraFunc['onKeyPress'] . '"' . ' ' . $sParms . '>';
    if ($bEcho) {
      echo $sInput;
    } else {
      return $sInput;
    }
  }

  public function criarSigla($sStr, $sTabela = null, $sCampo = null) {
    $aNomes = array();
    $aDados = explode(' ', $sStr);
    $iQnt = count($aDados);

    // Passo 1
    foreach ($aDados as $sNome) {

      // Nome escolhido tem que ter mais de 2 caracteres e guarda no array somente 3 nomes
      if (strlen($sNome) > 2 && count($aNomes) < 3) {
        $aNomes[] = trim($sNome);
      }
    }

    // Passo 2
    if (count($aNomes) < 3) {
      for ($i = count($aNomes); $i < 3; $i++) {
        $aNomes[$i] = $aNomes[0];
      }
    }

    // Passo 3
    $aNomesOk = array();
    foreach ($aNomes as $sNome) {
      $aNomesOk[] = strtoupper($this->montaUrlAmigavel($sNome, false));
    }

    // Passo 4
    if (!is_null($sTabela) && !is_null($sCampo)) {
      $this->pegaInfoDB($sTabela, 'distinct(' . $sCampo . ')');
      foreach ($this->RETDB as $aRet) {
        $aCdProjExistentes[] = $aRet[0];
      }
    }
    $aCdProjExistentes[] = '';

    // Passo 5
    $c = 1;
    $this->a = 0;
    $this->b = 0;
    $this->c = 0;
    $this->iQnt1 = strlen($aNomesOk[0]);
    $this->iQnt2 = strlen($aNomesOk[1]);
    $this->iQnt3 = strlen($aNomesOk[2]);



    do {
      $sSigla = $this->logicaSigla($aNomesOk);
      $c++;
    } while (in_array($sSigla, $aCdProjExistentes) && ($c < 15));
    $sSigla = $sSigla == '' ? 'ZER' : $sSigla;

    return $sSigla;
  }

  public function logicaSigla($aNomesOk) {

    $sSigla = $aNomesOk[0][$this->a];
    $sSigla .= $aNomesOk[1][$this->b];
    $sSigla .= $aNomesOk[2][$this->c];

    if (($this->iQnt1 > $this->a + 1)) {
      $this->a++;
    } else {
      $this->a = 0;
    }
    if (($this->iQnt2 > $this->a + 1)) {
      $this->b = $this->a + 1;
    } else {
      $this->b = 0;
    }
    if ($this->iQnt3 > $this->b + 1) {
      $this->c = $this->b + 1;
    } else {
      $this->c = 0;
    }


    if ($sSigla[0] == $sSigla[1] && $sSigla[1] == $sSigla[2]) {
      $sSigla = $this->logicaSigla($aNomesOk);
    }
    return $sSigla;
  }

  function randomStringGenerator($length = 10, $bUpper = true) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return ($bUpper) ? strtoupper($randomString) : $randomString;
  }

  public function breadCrumbs() {
    
  }

  /* wTools::plural
   *
   * Verifica a necessidade de uma palavra usar ou n�o o "s" no final
   *
   * @date  30/03/2013
   * @param string $mTeste  - Valor a ser testado
   * @return string  $sRetorno - 'S' ou ''
   */

  public function plural($mTeste, $iNecessario = 1) {
    return ($mTeste > $iNecessario) ? 's' : '';
  }

  /* wTools::BuscarNomePaginaAtual
   *
   * Define de forma autom�tica o nome da p�gina atual
   *
   * @date  08/02/2015
   * @return string  nome da p�gina atual
   */

  public function BuscarNomePaginaAtual() {

    if (!empty($this->sPaginaAtualNome)) {
      return $this->sPaginaAtualNome;
    }

    $aRet = array_filter(explode('/', $_SERVER['REQUEST_URI']));

    $aUrlBase = array_filter(explode('/', $this->sUrlBase));

    $aUrlAtualLimpo = array_values(array_diff($aRet, $aUrlBase));
        
    $this->sPaginaAtualNome = 'index';
    if (isset($aUrlAtualLimpo[0])) {
      $this->sPaginaAtualNome = $aUrlAtualLimpo[0];
    }


    for($i = 1; $i < count($aUrlAtualLimpo); $i++) {
      if (isset($aUrlAtualLimpo[$i])) {
        $this->aPaginaAtualParametros = $this->retornarParametrosPorString($aUrlAtualLimpo[$i]);
      }
    }

    return $this->sPaginaAtualNome;
  }
  
  
  /* wTools::montarNomeObjeto
   *
   * A partir do nome do arquivo/p�gina, retorna o nome do objeto que precisa
   * ser inst�nciado
   *
   * @date  18/11/2017
   * @return string  nome do objeto
   */
  public function montarNomeObjeto() {
    $sPaginaAtualNome = $this->BuscarNomePaginaAtual();
    
    $this->sPaginaAtualNomeObjeto = $sPaginaAtualNome;
    if ($sPaginaAtualNome == 'painel') {
      if (isset($this->aDadosVariaveisUrlPaginaAtual['secao'])) {
        $this->sPaginaAtualNomeObjeto = 'Admin_'.$this->aDadosVariaveisUrlPaginaAtual['secao'];
      } else {
        $this->sPaginaAtualNomeObjeto = 'Login';
      }
    }
    return $this->sPaginaAtualNomeObjeto;
  }


  /* wTools::BuscarNomePaginaAtual
   *
   * Melhoria do m�todo BuscarNomePaginaAtual, retorna mais informa��es em um
   * array
   *
   * @date  31/07/2017
   * @return string  nome da p�gina atual
   */
  public function buscarInfoPaginaAtual() {
    
    return '';
    
exit;
    
//
//    COMO ERA 
//            if (in_array('painel', $aUrlAtualLimpo)) {
//      $this->aDadosPaginaAtual[0] = $aUrlAtualLimpo[0];
//    } else {
//      $this->aDadosPaginaAtual[0] = array_pop($aUrlAtualLimpo);
//    }
//    
//    $iOffset = array_search('editar', $aUrlAtualLimpo);
//
//    if ($iOffset >= 1) {
//
//      $aVar = array_slice($aUrlAtualLimpo, $iOffset);
//      $this->aDadosVariaveisUrlPaginaAtual[0] = array_shift($aVar);
//      foreach ($aVar as $i => $value) {
//        if ($i % 2) {
//          $this->aDadosVariaveisUrlPaginaAtual[$mIndiceAnterior] = $value;
//        } else {
//          $mIndiceAnterior = $value;
//          $this->aDadosVariaveisUrlPaginaAtual[$value] = '';
//        }
//      }
//    }
//    
//    if (in_array('novo', $aUrlAtualLimpo)) {
//      $aVar = array_slice($aUrlAtualLimpo, $iOffset);
//      $this->aDadosVariaveisUrlPaginaAtual[0] = 'novo';
//    }
//
//    $this->aDadosPaginaAtual[0] = 'Admin_noticias';
//    
//    return $this->aDadosPaginaAtual[0];
  }
  
  private function retornarParametrosPorString($sParametros) {
    
    $sParametros = $this->decodificarParametrosString($sParametros);
    
    $sParametros = str_replace('?', '&', $sParametros);

    $aParametros = array_filter(explode('&', $sParametros));

    foreach ($aParametros as $value) {
      $a = explode('=', $value);
      if (count($a) > 1) {
        $this->aDadosVariaveisUrlPaginaAtual[$a[0]] = $a[1];
      } else {
        $this->aDadosVariaveisUrlPaginaAtual[] = $a[0];
      }
    }
    return $this->aDadosVariaveisUrlPaginaAtual;
  }
  
  public function CodificarParametrosString($sParametros) {
    return $this->oCodificador->Codificar($sParametros);
  }


  public function DecodificarParametrosString($sParametros) {
    return $this->oCodificador->Decodificar($sParametros);
  }
  
  public function definirPathLocal() {
    
  }

  /* wTools::buscarNomePaginaAnterior
   *
   * Define de forma autom�tica o nome da p�gina anterior
   *
   * @date  01/05/2015
   * @return string  nome da p�gina anterior
   */

  public function buscarNomePaginaAnterior($bExtensao = false) {
    $aRet = explode('/', $_SERVER['HTTP_REFERER']);
    $sPgAnterior = array_pop($aRet);
    if (!$bExtensao) {
      $aPgAtual = explode('.', $sPgAnterior);
      $sPgAnterior = strtolower(strtolower(array_shift($aPgAtual)));
    }
    return $sPgAnterior;
  }

  /* wTools::somarData
   *
   * Define de forma autom�tica o nome da p�gina atual
   *
   * @date  21/03/2015
   * @param $sData  Data de refer�ncia
   * @param $aParametros 
   * @return string  nome da p�gina atual
   */

  public function somarData($sData, $aParametros) {
    $iMeses = isset($aParametros['iMeses']) ? $aParametros['iMeses'] : 0;
    $iAno = isset($aParametros['iAno']) ? $aParametros['iAno'] : 0;
    $iDias = isset($aParametros['iDias']) ? $aParametros['iDias'] : 0;
    $aData = explode('/', $sData);
    $sNovaData = date('d/m/Y', mktime(0, 0, 0, $aData[1] + $iMeses, $aData[0] + $iDias, $aData[2] + $iAno));
    return $sNovaData;
  }

  /* wTools::buscarUltimoId
   *
   * Faz uma consulta � uma tabela no banco de dados e traz o �ltimo ID
   * @date 21/06/2015
   * @param string $sTabela  - Tabela a ser consultada
   * @return integer
   */

  public function buscarUltimoId($sTabela) {

    $sQuery = 'SELECT max(ID)
                 FROM ' . $sTabela;

    $aResult = $this->oBd->query($sQuery);

    return $aResult[0][0];
  }

  /* wTools::buscarProximoId
   *
   * Faz uma consulta � uma tabela no banco de dados e traz o pr�ximo ID
   * @date 21/06/2015
   * @param string $sTabela  - Tabela a ser consultada
   * @return integer
   */
  public function buscarProximoId($sTabela) {

    $sQuery = 'SELECT max(ID) + 1
                 FROM ' . $sTabela;

    $aResult = $this->oBd->query($sQuery);
    return $aResult[0][0];
  }
  
  /* wTools::listarPastas
   *
   * Guarda no array os diret�rios do caminho passado como par�metro
   * @date 24/08/2015
   * @param string $sCaminho Caminho dos diret�rios 
   * @return boll
   */
  public function listarPastas($sCaminho, $bSubPastas = false) {
    $this->aPastas = array();
    $hDir = opendir($sCaminho);

    while (false!==($sPasta=readdir($hDir)))  {
      
      if ($bSubPastas == false) {
        if (in_array($sPasta, array('.','..'))) {
          continue;
        }
      }
      
      if (is_dir($sCaminho.'/'.$sPasta)) {
        $this->aPastas[] = $sPasta;
      }
    }

    arsort($this->aPastas); 
    return true;
  }

  /* wTools::listarConteudoPastas
   *
   * Guarda no array os arquivos de uma pasta
   * @date 24/08/2015
   * @param string $sCaminho Caminho dos diret�rios 
   * @return bool
   */
  public function listarConteudoPastas($sPasta) {
    $this->aArquivos = array();
    $hDir = opendir($sPasta);
    while (false!==($sArquivo=readdir($hDir)))  {
      if (is_file($sPasta.'/'.$sArquivo)) {
        $this->aArquivos[] = $sArquivo;
      }
      sort($this->aArquivos); 
    }
    if (empty($this->aArquivos)) {
      return false;
    }
    return true;
  }

  /* wTools::montarTabelaHtmlPorLinhas
   *
   * Monta uma tabela Html din�micamente
   * @date 23/01/2016
   * @param $aEntrada - Dados em formato de string para serem apresentados
   *        $iLinhas  - Quantidade de linhas que a tabela ter�
   * @return bool
   */
  public function montarTabelaHtmlPorLinhas($aEntrada, $iLinhas) {
    $aDados = array();
    $iIndice = $i = 0;
    
    $iTotal = count($aEntrada);

    $this->iMaiorColuna = ceil($iTotal/$iLinhas);
    
    
    foreach ($aEntrada as $sValor) {
      
      if ($i >= $this->iMaiorColuna) {
        $iIndice++;
        $i = 0;
      }
        
      $aDados[$iIndice][] = $sValor;
      $i++;
    }
    
    $aDados = $this->prepararArrayToTable($aDados);
    
    $this->montarTabelaHtml($aDados);
  }
  
  
  /* wTools::montarTabelaHtmlPorColunas
   *
   * Monta uma tabela Html din�micamente
   * @date 23/01/2016
   * @param $aEntrada - Dados em formato de string para serem apresentados
   *        $iCols    - Quantidade de colunas que a tabela ter�
   * @return bool
   */
  public function montarTabelaHtmlPorColunas($aEntrada, $iCols) {

    $aDados = array();
    $iIndice = $i = 0;
    
    //$iTotal = count($aEntrada);

    $this->iMaiorColuna = $iCols - 1;  

    foreach ($aEntrada as $sValor) {      
      $aDados[$iIndice][] = $sValor;

      if ($i >= $this->iMaiorColuna) {
        $i = -1;
        $iIndice++;
      }
      $i++;
    }
    
    $aDados = $this->prepararArrayToTable($aDados);
    
    $this->montarTabelaHtml($aDados);

  }
  
  /* wTools::prepararArrayToTable
   *
   * Usado internamente ao ser chamado nos m�todos montarTabelaHtmlPorColunas 
   * e montarTabelaHtmlPorColunas
   * @date 23/01/2016
   * @param $aDados - Prepara os dados conforme o esperado para apresentar na tela
   * @return bool
   */
  private function prepararArrayToTable($aDados) {
    $i = 0;
    for ($iIndice = 0; $iIndice < count($aDados); $iIndice++) {
      $i = 0;
      while ($i <= $this->iMaiorColuna) {
        if (!isset($aDados[$iIndice][$i])) $aDados[$iIndice][$i] = '';
        //echo 'Linha '.$iIndice.' Coluna: '.$i." --> ".$aDados[$iIndice][$i]."<br>";
        $i++;
      }
    }
    return $aDados;
  }

  /* wTools::prepararArrayToTable
   *
   * Imprime na tela em formato de tabela HTML os dados que s�o passados pelo 
   * par�metro. 
   * O array deve estar formatado.
   * 
   * @date 23/01/2016
   * @param $aDados - Dados j� formatados com quantidade de linhas e colunas.
   * @return bool
   */
  private function montarTabelaHtml($aDados) { ?>
    <table> <?php
    for ($iIndice = 0; $iIndice < count($aDados); $iIndice++) {
      $i = 0; 
      //echo 'Row['.$iIndice.']'; ?>
      <tr> <?php
      while ($i <= $this->iMaiorColuna) {
        //echo 'Col['.$i.'] ';?>
        <td><?php echo $aDados[$iIndice][$i]?></td><?php
        $i++;
      }?>
       <tr><?php
    }?>
    </table><?php
  }
  
  
  /* wTools::substituirTagPorImagem
   *
   * Imprime na tela em formato de tabela HTML os dados que s�o passados pelo 
   * par�metro. 
   * O array deve estar formatado.
   * 
   * @date 31/01/2016
   * @param sConteudo -  String que ser� salva no banco de dados antes do parse
   *                     ser realizado
   * @param sDiretorio - Local onde as imagens est�o armazenadas
   * @return string    - Retorna o conte�do pronto para ser usado
   */
  public function substituirTagPorImagem($sConteudo, $sDiretorio) {

     
    while ($iPosInicio = stripos($sConteudo, '&lt;#')) {

      $iPosFinal  = stripos($sConteudo, '#&gt;');
      $iTamanho = $iPosFinal - $iPosInicio;
      $sNomeImagem = substr($sConteudo, ($iPosInicio + 5), ($iTamanho - 5));
      
      $sImagem = '<img src="'.$sDiretorio.$sNomeImagem.'" alt="'.$sNomeImagem.'" />';

      $sPadrao = substr($sConteudo, $iPosInicio, ($iTamanho + 5));
      $sConteudo = str_replace($sPadrao, $sImagem, $sConteudo);
              
    }
    return $sConteudo;
      
    
  }
  
  /* wTools::tratarOpcoesDeFiltro
   *   
   * @date 06/11/2017
   * @param $mOpcoes - Filtro de pesquisa para ser usado em consulta SQL
   * @param 
   * @return string    - Filtro montado
   */
  public function tratarOpcoesDeFiltro($mOpcoes) {
    $sFiltro = '';
    
    if (is_string($mOpcoes)) {
      $sFiltro = $mOpcoes;
    }elseif (is_array($mOpcoes)) {
      $sFiltro = 'WHERE ';
      $sFiltro .= implode(" AND ", $mOpcoes);
    }
    
    return $sFiltro;
  }
  
  /* wTools::RemoverSlash
   *   Remove / caso exista
   * @date 24/09/2018
   * @param $string
   * @return string
   */
  public function RemoverSlash($string) {
    $ultima = substr($string, -1);
    if ($ultima == '/') {
      $string = substr($string, 0, -1);
    }
    return $string;
    
  }
  
}
?>